#version 450

layout(binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 projection;
    vec3 ambient_color;
    vec3 light_location;
    vec3 light_color;
    float light_intensity;
} ubo;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inTangent;
layout(location = 3) in vec2 inUv;

layout(location = 0) out vec2 fragTexCoord;
layout(location = 1) out vec3 worldLocation;
layout(location = 2) out vec3 worldNormal;
layout(location = 3) out mat3 tangentBiNormalMatrix;

mat3 compute_TBN(vec3 normal, vec3 tangent)
{
    vec3 N = normalize(normal);
    vec3 T = normalize(tangent);
    vec3 B = normalize(cross(T, N));

    return mat3(T, B, N);
}

void main() {
    fragTexCoord = inUv;

    worldLocation =  (ubo.model * vec4(inPosition, 1.0)).xyz;
    worldNormal = (ubo.model * vec4(inNormal, 0.0)).xyz;
    vec3 worldTangent = (ubo.model * vec4(inTangent, 0.0)).xyz;
    tangentBiNormalMatrix = compute_TBN(worldNormal, worldTangent);
    gl_Position = ubo.projection * ubo.view * vec4(worldLocation, 1.0);
}
