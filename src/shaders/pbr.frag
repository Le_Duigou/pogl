#version 450

layout(location = 0) in vec2 fragTexCoord;
layout(location = 1) in vec3 worldLocation;
layout(location = 2) in vec3 worldNormal;
layout(location = 3) in mat3 tangentBiNormalMatrix;

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 projection;
    vec3 eye_position;
    vec3 ambient_color;
    vec3 light_location;
    vec3 light_color;
    float light_intensity;
} ubo;

layout(binding = 1) uniform sampler2D diffuseSampler;
layout(binding = 2) uniform sampler2D ormSampler;
layout(binding = 3) uniform sampler2D normalSampler;
layout(binding = 4) uniform sampler2D envSampler;


const float pi = 3.1415926535f;

float ggx_distribution(vec3 n, vec3 h, float roughness)
{
    float a = roughness * roughness;
    float a_square = a * a;

    float n_dot_h = max(dot(n, h), 0.0);
    float n_dot_h_square = n_dot_h * n_dot_h;

    float denominator = n_dot_h_square * (a_square - 1.f) + 1.f;
    denominator = pi * denominator * denominator;

    return a_square / denominator;
}

float ggx_geometry_schlick(float n_dot_v, float roughness)
{
    float r = roughness + 1.0;
    float k = (r * r) / 8.0;

    float numerator = n_dot_v;
    float denominator = n_dot_v * (1.0 - k) + k;

    return numerator / denominator;
}

float geometry_smith(vec3 n, vec3 v, vec3 l, float roughness)
{
    float n_dot_v = max(dot(n, v), 0.0);
    float n_dot_l = max(dot(n, l), 0.0);

    float ggx1 = ggx_geometry_schlick(n_dot_v, roughness);
    float ggx2 = ggx_geometry_schlick(n_dot_l, roughness);

    return ggx1 * ggx2;
}

vec3 fresnel(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(max(1.0 - cosTheta, 0.0), 5.0);
}

vec3 light_component(vec3 n, vec3 v,
                    vec3 reflection, vec3 F0,
                    vec3 base_color, float roughness, float metallic,
                    vec3 light_pos, vec3 light_color, float light_intensity)
{
    vec3 difference = light_pos - worldLocation;
    float distance = length(difference);
    vec3 l = normalize(difference);
    vec3 h = normalize(v + l);
    float attenuation = 1 / (distance * distance);
    vec3 radiance = light_color * attenuation;
    float cos_theta = max(dot(h, v), 0.0);

    float distribution = ggx_distribution(n, h, roughness);
    float geometry = geometry_smith(n, v, l, roughness);
    vec3 fresnel_coeff = clamp(fresnel(max(dot(h, v), 0.0), F0), 0, 1);

    vec3 k_specular = fresnel_coeff;
    vec3 k_diffuse = (1.0 - metallic) * (1.0 - k_specular);

    vec3 nominator = distribution * geometry * k_specular;
    float n_dot_l =  max(0.0, dot(n, l));
    float n_dot_v = max(0.0, dot(n, v));
    float denominator = 4.0 * n_dot_v * n_dot_l;

    vec3 specular = nominator / max(0.001, denominator);
    vec3 diffuse = k_diffuse * base_color / pi;

    return (diffuse + specular * reflection) * radiance * n_dot_l;
}

void main() {

    vec4 base_color_rgba = texture(diffuseSampler, fragTexCoord);
    base_color_rgba.rgb = pow(base_color_rgba.rgb, vec3(2.2));

    vec3 orm = texture(ormSampler, fragTexCoord).rgb;
    vec3 normal_map = texture(normalSampler, fragTexCoord).rgb;
    normal_map.g = 1 - normal_map.g;
    normal_map = normalize(normal_map * 2.0 - 1.0);

    vec3 n = normalize(tangentBiNormalMatrix * normal_map);
    vec3 v = normalize(ubo.eye_position - worldLocation);

    float occlusion = orm.r;
    float roughness = orm.g;
    float metallic = orm.b;

    float max_lod = textureQueryLevels(envSampler);
    float lod_bias = 3;
    float env_LOD = lod_bias + pow(roughness, 0.2) * (max_lod - lod_bias);
    vec3 reflection = textureLod(envSampler, reflect(-v, n).xy, env_LOD).rgb;
    vec3 F0 = mix(vec3(0.04), base_color_rgba.rgb, metallic); 

    vec3 color = reflection * ubo.ambient_color * base_color_rgba.rgb * occlusion;

    // For each light (currently only one)
    color +=
        light_component(
            n,
            v,
            reflection,
            F0,
            base_color_rgba.rgb,
            roughness,
            metallic,
            ubo.light_location,
            ubo.light_color,
            ubo.light_intensity
        );

    // Gamma correct & tone mapping
    color = color / (color + vec3(1.0));
    color = pow(color, vec3(1.0/2.2));

    outColor = vec4(color, 1);
}
