#include <exception>
#include <iostream>

#include "engine/program/program.hh"
#include "options/options.hh"
#include "scenes/scene_factory.hh"

int main(int argc, char* argv[])
{
    const auto options = options::Options(argc, argv);
    const auto scene_factory = scenes::get_scene_factory(options.scene_name);

    try
    {
        auto program = engine::Program("VULKAN-TEST", 1024, 1024, options);

        auto scene = scene_factory(program.renderer());

        program.run();
    }
    catch (std::exception& error)
    {
        std::cerr << "An error occurred: " << error.what() << "\n";
        return 1;
    }
    return 0;
}
