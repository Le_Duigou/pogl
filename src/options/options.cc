#include "options.hh"

#include <boost/program_options.hpp>
#include <iostream>

namespace opt = boost::program_options;

namespace options
{
    Options::Options(int argc, char* argv[])
    {
        opt::options_description desc("Allowed options");
        desc.add_options()("help", "show usage");

        desc.add_options()("no-validation-layers", opt::bool_switch(&no_validation_layers)->default_value(false),
                           "Disable validation layers");

        desc.add_options()("no-anisotropic", opt::bool_switch(&no_anisotropic_filtering)->default_value(false),
                           "Disable texture anisotropic filtering feature");

        desc.add_options()("no-multidraw-indirect", opt::bool_switch(&no_multi_draw_indirect)->default_value(false),
                           "Disable indirect multidraw feature");

        desc.add_options()("scene", opt::value<std::string>(&scene_name), "Name of the scene");

        opt::positional_options_description p;
        p.add("scene", -1);

        opt::variables_map vm;
        try
        {
            opt::store(opt::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
            opt::notify(vm);
        }
        catch (...)
        {
            std::cerr << "error: unknown option \"" << desc << "\"\n";
            std::exit(1);
        }

        if (vm.count("help"))
        {
            std::cout << desc;
            std::exit(0);
        }

        if (!vm.count("scene"))
        {
            std::cerr << "Error: no scene specified" << '\n';
            std::exit(1);
        }
    }
} // namespace options
