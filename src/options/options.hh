#pragma once

#include <string>
namespace options
{
    struct Options
    {
        Options(int argc, char* argv[]);

        std::string scene_name;

        bool no_validation_layers;

        bool no_multi_draw_indirect;
        bool no_anisotropic_filtering;
    };
} // namespace options
