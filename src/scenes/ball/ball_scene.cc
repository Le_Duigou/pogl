#include "ball_scene.hh"

#include "engine/camera/camera.hh"
#include "engine/renderer/renderer.hh"
#include "engine/window/key_pressed.hh"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>

namespace scenes
{
    constexpr std::array<const char*, 6> variations{"lava", "bronze", "forest", "concrete", "leather", "metal_panels"};
    size_t current_variation;
    bool has_texture_been_updated_recently;

    BallScene::BallScene(engine::Renderer& renderer)
    {
        set_textures(renderer, 0);

        renderer.add_mesh("../resources/ball/ball.obj");

        renderer.set_update_callback(&update);

        renderer.set_light(engine::PointLight{{0.5, -1.f, -0.2f}, {1, 1, 1}, 1});

        renderer.set_ambient_color(engine::vec3_t{1, 1, 1} * 0.5f);

        const auto camera_location = glm::vec3(0.0f, -4.0f, -2.0f);
        const auto camera_look_at = glm::vec3(0.0f, 0.0f, 0.0f);

        renderer.camera().set_location_and_lookat(camera_location, camera_look_at);
    }

    void BallScene::update(double delta, engine::Renderer& renderer, const engine::KeyPressed& keys)
    {
        rotate(delta, renderer, keys);
        update_textures(renderer, keys);
    }

    void BallScene::rotate(double delta, engine::Renderer& renderer, const engine::KeyPressed& keys)
    {
        if (keys.get_space())
            return;

        float rotation = glm::radians(20.0f * delta);

        if (keys.get_right())
        {
            if (keys.get_left())
                return;

            rotation *= -2;
        }
        else if (keys.get_left())
        {
            rotation *= 2;
        }

        renderer.set_model_transform(glm::rotate(renderer.model_transform(), rotation, {0, 0, 1}));
    }

    void BallScene::update_textures(engine::Renderer& renderer, const engine::KeyPressed& keys)
    {
        if (!keys.get_enter())
        {
            has_texture_been_updated_recently = false;
            return;
        }

        if (has_texture_been_updated_recently)
            return;

        set_textures(renderer, current_variation + 1);

        has_texture_been_updated_recently = true;
    }

    void BallScene::set_textures(engine::Renderer& renderer, size_t variation)
    {
        current_variation = variation % variations.size();

        const std::string& current_name = variations[current_variation];

        renderer.set_texture_set("main", "../resources/ball/ball_" + current_name + "_BaseColor.png",
                                 "../resources/ball/ball_" + current_name + "_OcclusionRoughnessMetallic.png",
                                 "../resources/ball/ball_" + current_name + "_Normal.png",
                                 "../resources/hdr/crosswalk_2k.hdr");
    }
} // namespace scenes
