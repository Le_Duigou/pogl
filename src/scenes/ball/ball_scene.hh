#pragma once

#include <array>
#include <cstddef>

#include "scenes/scene.hh"

namespace scenes
{
    class BallScene : public Scene
    {
    public:
        BallScene(engine::Renderer& renderer);

    private:
        static void update(double delta, engine::Renderer& renderer, const engine::KeyPressed& keys);

        static void rotate(double delta, engine::Renderer& renderer, const engine::KeyPressed& keys);
        static void update_textures(engine::Renderer& renderer, const engine::KeyPressed& keys);

        static void set_textures(engine::Renderer& renderer, size_t variation);
    };
} // namespace scenes
