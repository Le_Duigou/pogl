#include "tree_scene.hh"

#include "engine/camera/camera.hh"
#include "engine/renderer/renderer.hh"
#include "engine/window/key_pressed.hh"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>

namespace scenes
{
    TreeScene::TreeScene(engine::Renderer& renderer)
    {
        renderer.set_texture_set("main", "../resources/tree/tree_BaseColor.png",
                                 "../resources/tree/tree_OcclusionRoughnessMetallic.png",
                                 "../resources/tree/tree_Normal.png", "../resources/hdr/crosswalk_2k.hdr");

        renderer.add_mesh("../resources/tree/tree.obj");

        renderer.set_update_callback(&update);

        renderer.set_light(engine::PointLight{{-0.5f, 1.6f, -1.5f}, {1, 1, 1}, 100});

        renderer.set_ambient_color(engine::vec3_t{1, 1, 1} * 0.1f);

        const auto camera_location = glm::vec3(0.0f, 10.0f, -8.0f);
        const auto camera_look_at = glm::vec3(0.0f, 0.0f, -0.5f);

        renderer.camera().set_location_and_lookat(camera_location, camera_look_at);
    }

    void TreeScene::update(double delta, engine::Renderer& renderer, const engine::KeyPressed& keys)
    {
        rotate(delta, renderer, keys);
    }

    void TreeScene::rotate(double delta, engine::Renderer& renderer, const engine::KeyPressed& keys)
    {
        if (keys.get_space())
            return;

        float rotation = glm::radians(20.0f * delta);

        if (keys.get_right())
        {
            if (keys.get_left())
                return;

            rotation *= 2;
        }
        else if (keys.get_left())
        {
            rotation *= -2;
        }

        renderer.set_model_transform(glm::rotate(renderer.model_transform(), rotation, {0, 0, 1}));
    }

} // namespace scenes
