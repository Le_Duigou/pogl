#pragma once

#include <functional>
#include <memory>
#include <string>

#include "scene.hh"

namespace scenes
{
    using scene_factory_t = std::function<std::unique_ptr<Scene>(engine::Renderer& renderer)>;
    scene_factory_t get_scene_factory(const std::string& scene);

    std::unique_ptr<Scene> make_ball_scene(engine::Renderer& renderer);
    std::unique_ptr<Scene> make_chess_scene(engine::Renderer& renderer);
    std::unique_ptr<Scene> make_mario_scene(engine::Renderer& renderer);
    std::unique_ptr<Scene> make_suzanne_scene(engine::Renderer& renderer);
    std::unique_ptr<Scene> make_tree_scene(engine::Renderer& renderer);
} // namespace scenes
