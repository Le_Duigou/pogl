#include "chess_scene.hh"

#include <glm/gtx/dual_quaternion.hpp>
#include <glm/vector_relational.hpp>

#include "engine/camera/camera.hh"
#include "engine/renderer/renderer.hh"
#include "engine/units/units.hh"
#include "engine/window/key_pressed.hh"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>

namespace scenes
{
    ChessScene::ChessScene(engine::Renderer& renderer)
    {
        renderer.set_texture_set("main", "../resources/chess/chess_BaseColor.png",
                                 "../resources/chess/chess_OcclusionRoughnessMetallic.png",
                                 "../resources/chess/chess_Normal.png", "../resources/hdr/crosswalk_2k.hdr");

        renderer.add_mesh("../resources/chess/chess_piece.obj");
        renderer.add_mesh("../resources/chess/chess_board.obj");

        renderer.set_update_callback(&update);

        renderer.set_light(engine::PointLight{{-2, 0, -1.f}, {1, 1, 1}, 100});

        renderer.set_ambient_color(engine::vec3_t{1, 1, 1} * 0.5f);

        const auto camera_location = glm::vec3(0.0f, -3.0f, -2.5f);
        const auto camera_look_at = glm::vec3(0.0f, 0.0f, -1.f);

        renderer.camera().set_location_and_lookat(camera_location, camera_look_at);
        renderer.camera().set_z_far(1200);
    }

    void ChessScene::update(double delta, engine::Renderer& renderer, const engine::KeyPressed& keys)
    {
        rotate(delta, renderer, keys);
    }

    void ChessScene::rotate(double delta, engine::Renderer& renderer, const engine::KeyPressed& keys)
    {
        engine::vec3_t translation = engine::vec3_t(0);

        if (keys.get_left())
            translation.x -= 1;
        if (keys.get_right())
            translation.x += 1;
        if (keys.get_up())
            translation.y += 1;
        if (keys.get_down())
            translation.y -= 1;

        if (translation == engine::vec3_t(0))
            return;

        translation = glm::normalize(translation) * static_cast<float>(delta) * 4.f;

        renderer.camera().set_location_and_lookat(renderer.camera().location() + translation,
                                                  renderer.camera().lookat() + translation);
    }

} // namespace scenes
