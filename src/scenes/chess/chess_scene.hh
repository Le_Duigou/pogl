#pragma once

#include "scenes/scene.hh"

namespace scenes
{
    class ChessScene : public Scene
    {
    public:
        ChessScene(engine::Renderer& renderer);

    private:
        static void update(double delta, engine::Renderer& renderer, const engine::KeyPressed& keys);

        static void rotate(double delta, engine::Renderer& renderer, const engine::KeyPressed& keys);
    };
} // namespace scenes
