#include "scene_factory.hh"

#include <iostream>
#include <unordered_map>
#include <vector>

#include "ball/ball_scene.hh"
#include "chess/chess_scene.hh"
#include "mario/mario_scene.hh"
#include "suzanne/suzanne_scene.hh"
#include "tree/tree_scene.hh"

namespace scenes
{
    scene_factory_t get_scene_factory(const std::string& scene)
    {
        const auto map = std::unordered_multimap<std::string, scene_factory_t>{
            {"ball", &make_ball_scene},      {"balls", &make_ball_scene},  {"suzanne", &make_suzanne_scene},
            {"monkey", &make_suzanne_scene}, {"mario", &make_mario_scene}, {"chess", &make_chess_scene},
            {"tree", &make_tree_scene},
        };

        auto result = map.find(scene);

        if (result == map.end())
        {
            std::cerr << "Error: The specified scene \"" << scene << "\" does not exist.\n";
            std::cerr << "Available scenes are:\n";
            for (const auto& key_value : map)
            {
                std::cerr << "\t* " << key_value.first << "\n";
            }
            exit(1);
        }

        return result->second;
    }

    std::unique_ptr<Scene> make_ball_scene(engine::Renderer& renderer)
    {
        return std::make_unique<BallScene>(renderer);
    }

    std::unique_ptr<Scene> make_suzanne_scene(engine::Renderer& renderer)
    {
        return std::make_unique<SuzanneScene>(renderer);
    }

    std::unique_ptr<Scene> make_mario_scene(engine::Renderer& renderer)
    {
        return std::make_unique<MarioScene>(renderer);
    }

    std::unique_ptr<Scene> make_chess_scene(engine::Renderer& renderer)
    {
        return std::make_unique<ChessScene>(renderer);
    }

    std::unique_ptr<Scene> make_tree_scene(engine::Renderer& renderer)
    {
        return std::make_unique<TreeScene>(renderer);
    }

} // namespace scenes
