#include "suzanne_scene.hh"

#include "engine/camera/camera.hh"
#include "engine/renderer/renderer.hh"
#include "engine/window/key_pressed.hh"

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>

namespace scenes
{
    SuzanneScene::SuzanneScene(engine::Renderer& renderer)
    {
        renderer.set_texture_set("main", "../resources/suzanne/checker_BaseColor.png",
                                 "../resources/suzanne/checker_OcclusionRoughnessMetallic.png",
                                 "../resources/suzanne/checker_Normal.png", "../resources/hdr/crosswalk_2k.hdr");

        renderer.add_mesh("../resources/suzanne/suzanne_smooth.obj");
        renderer.add_mesh("../resources/suzanne/floor_slab.obj");

        renderer.set_update_callback(&update);

        renderer.set_light(engine::PointLight{{-2, 0, -1.f}, {1, 0, 1}, 100});

        renderer.set_ambient_color(engine::vec3_t{1, 1, 1} * 0.01f);

        const auto camera_location = glm::vec3(0.0f, -5.0f, -2.0f);
        const auto camera_look_at = glm::vec3(0.0f, 0.0f, 0.0f);

        renderer.camera().set_location_and_lookat(camera_location, camera_look_at);
    }

    void SuzanneScene::update(double delta, engine::Renderer& renderer, const engine::KeyPressed& keys)
    {
        rotate(delta, renderer, keys);
    }

    void SuzanneScene::rotate(double delta, engine::Renderer& renderer, const engine::KeyPressed& keys)
    {
        if (keys.get_space())
            return;

        float rotation = glm::radians(20.0f * delta);

        if (keys.get_right())
        {
            if (keys.get_left())
                return;

            rotation *= -2;
        }
        else if (keys.get_left())
        {
            rotation *= 2;
        }
        renderer.camera().set_location(glm::rotate(renderer.camera().location(), rotation, {0, 0, 1}));
    }

} // namespace scenes
