#pragma once

#include "engine/units/units.hh"

namespace engine
{
    struct UniformBufferObject
    {
        alignas(8) mat4_t model;
        alignas(8) mat4_t view;
        alignas(8) mat4_t projection;
        alignas(8) vec3_t eye_position;
        alignas(8) vec3_t ambient_color;
        alignas(8) vec3_t light_location;
        alignas(8) vec3_t light_color;
        alignas(8) float light_intensity;
    };
} // namespace engine
