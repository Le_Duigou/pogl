#include "image_sampler.hh"

#include <iostream>

#include "engine/devices/logical_device.hh"
#include "engine/devices/physical_device.hh"
#include "engine/error/vulkan_errors.hh"

namespace engine
{
    ImageSampler::ImageSampler(const logical_device_t& device, uint32_t mip_level)
        : device_(device)
    {
        setup(mip_level);
    }

    ImageSampler::~ImageSampler()
    {
        cleanup();
    }

    ImageSampler::sampler_t& ImageSampler::get()
    {
        return sampler_;
    }

    const ImageSampler::sampler_t& ImageSampler::get() const
    {
        return sampler_;
    }

    void ImageSampler::setup(uint32_t mip_level)
    {
        const auto properties = device_.physical_device().get_properties();
        const auto features = device_.physical_device().get_features();

        const auto create_info = VkSamplerCreateInfo{
            .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .magFilter = VK_FILTER_LINEAR,
            .minFilter = VK_FILTER_LINEAR,
            .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
            .addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT,
            .addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT,
            .addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
            .mipLodBias = 0.f,
            .anisotropyEnable = features.samplerAnisotropy,
            .maxAnisotropy = features.samplerAnisotropy ? properties.limits.maxSamplerAnisotropy : 0,
            .compareEnable = VK_FALSE,
            .compareOp = VK_COMPARE_OP_ALWAYS,
            .minLod = 0.f,
            .maxLod = static_cast<float>(mip_level),
            .borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
            .unnormalizedCoordinates = VK_FALSE,
        };

        const auto error = vkCreateSampler(device_.get(), &create_info, nullptr, &sampler_);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Unable to create sampler\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void ImageSampler::cleanup()
    {
        vkDestroySampler(device_.get(), sampler_, nullptr);
    }
} // namespace engine
