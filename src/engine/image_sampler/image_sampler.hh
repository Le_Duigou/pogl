#pragma once
#include <cstdint>
#include <vulkan/vulkan_core.h>

namespace engine
{
    class LogicalDevice;
    class ImageSampler
    {
    public:
        using sampler_t = VkSampler;
        using logical_device_t = LogicalDevice;

        ImageSampler(const logical_device_t& device, uint32_t mip_level);
        ~ImageSampler();

        sampler_t& get();
        const sampler_t& get() const;

    private:
        void setup(uint32_t mip_level);
        void cleanup();

    private:
        const logical_device_t& device_;
        sampler_t sampler_;
    };
} // namespace engine
