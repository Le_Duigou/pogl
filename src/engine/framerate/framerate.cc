#include "framerate.hh"

#include <iostream>

namespace engine
{
    void Framerate::start()
    {
        start_ = std::chrono::steady_clock::now();
        last_ = start_;
        elapsed_frames_ = 0;
        rate_ = 0;
    }

    void Framerate::stop()
    {
        elapsed_frames_ = 0;
        rate_ = 0;
    }

    Framerate::rate_t Framerate::tick()
    {
        const time_point_t now = std::chrono::steady_clock::now();
        const duration_t duration = now - start_;
        elapsed_frames_++;

        if (duration.count() > COMPUTATION_WINDOW && duration.count() > 0)
        {
            rate_ = (static_cast<rate_t>(elapsed_frames_) / duration.count());

            /*std::cout << rate_ << " FPS " << elapsed_frames_ << " frames in "
                      << duration.count() << "s\n";*/

            start_ = std::chrono::steady_clock::now();
            elapsed_frames_ = 0;
        }

        const duration_t delta = now - last_;
        last_ = now;

        return delta.count(); // s
    }

    Framerate::rate_t Framerate::get_rate() const
    {
        return rate_;
    }
} // namespace engine
