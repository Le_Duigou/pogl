#pragma once

#include <chrono>

namespace engine
{
    class Framerate
    {
    public:
        using rate_t = double;

        void start();
        void stop();
        rate_t tick();

        rate_t get_rate() const;

    private:
        using time_point_t = std::chrono::time_point<std::chrono::steady_clock>;
        using duration_t = std::chrono::duration<double>;
        using frame_t = unsigned int;

        static constexpr double COMPUTATION_WINDOW = 0.2; // s

    private:
        time_point_t start_;
        time_point_t last_;
        frame_t elapsed_frames_;
        rate_t rate_;
    };
} // namespace engine
