#include "swapchain.hh"

#include <iostream>
#include <memory>
#include <stdexcept>

#include "engine/devices/logical_device.hh"
#include "engine/devices/physical_device.hh"
#include "engine/error/vulkan_errors.hh"
#include "engine/window/window.hh"
#include "engine/window_surface/window_surface.hh"

namespace engine
{
    Swapchain::Swapchain(window_surface_t& surface, physical_device_t& physical_device,
                         logical_device_t& logical_device)
        : window_surface_(surface)
        , physical_device_(physical_device)
        , logical_device_(logical_device)
    {
        get_info();

        if (!is_supported())
            throw std::runtime_error("Physical device does not support swapchain");

        setup();
    }

    Swapchain::~Swapchain()
    {
        cleanup();
    }

    bool Swapchain::is_supported() const
    {
        return !formats_.empty() && !present_modes_.empty();
    }

    void Swapchain::setup()
    {
        setup_swapchain();
        setup_images();
        setup_image_views();
    }

    void Swapchain::setup_swapchain()

    {
        surface_format_ = get_best_surface_format();
        swap_extent_ = get_best_swap_extent();

        const auto best_image_count = get_best_image_count();
        const auto best_present_mode = get_best_present_mode();

        const auto queue_indices = physical_device().queue_family_indices();
        const auto sharing_mode = get_sharing_mode(queue_indices);
        const auto is_sharing = sharing_mode == VK_SHARING_MODE_CONCURRENT;
        const auto indices_as_array = queue_indices.as_array();
        const auto queue_family_count = is_sharing ? indices_as_array.size() : 0;
        const auto queue_family_indices = is_sharing ? indices_as_array.data() : nullptr;

        VkSwapchainCreateInfoKHR create_swapchain_info{
            .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
            .pNext = nullptr,
            .flags = 0,
            .surface = window_surface_.surface(),
            .minImageCount = best_image_count,
            .imageFormat = surface_format_.format,
            .imageColorSpace = surface_format_.colorSpace,
            .imageExtent = swap_extent_,
            .imageArrayLayers = 1,
            // VK_IMAGE_USAGE_TRANSFER_DST_BIT for compositing
            .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
            .imageSharingMode = sharing_mode,
            .queueFamilyIndexCount = static_cast<uint32_t>(queue_family_count),
            .pQueueFamilyIndices = queue_family_indices,
            .preTransform = capabilities_.currentTransform,
            .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
            .presentMode = best_present_mode,
            .clipped = VK_TRUE,
            .oldSwapchain = VK_NULL_HANDLE,
        };

        const auto error = vkCreateSwapchainKHR(logical_device().get(), &create_swapchain_info, nullptr, &swapchain_);
        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not create swapchain.\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void Swapchain::setup_images()
    {
        const auto device = logical_device().get();
        uint32_t image_count;

        vkGetSwapchainImagesKHR(device, swapchain_, &image_count, nullptr);
        images_.resize(image_count);
        vkGetSwapchainImagesKHR(device, swapchain_, &image_count, images_.data());
    }

    void Swapchain::setup_image_views()
    {
        image_views_.reserve(images_.size());

        for (size_t i = 0; i < images_.size(); i++)
        {
            image_views_.emplace_back(std::make_unique<SwapchainImageView>(images_[i], *this));
        }
    }

    void Swapchain::cleanup()
    {
        cleanup_image_views();
        cleanup_swapchain();
    }

    void Swapchain::cleanup_swapchain()
    {
        vkDestroySwapchainKHR(logical_device().get(), swapchain_, nullptr);
    }

    void Swapchain::cleanup_image_views()
    {
        image_views_.clear();
    }

    void Swapchain::get_info()
    {
        get_capabilities_info();
        get_format_info();
        get_present_mode_info();
    }

    void Swapchain::get_capabilities_info()
    {
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device().get(), window_surface_.surface(), &capabilities_);
    }

    void Swapchain::get_format_info()
    {
        uint32_t format_count;
        vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device().get(), window_surface_.surface(), &format_count,
                                             nullptr);

        if (format_count != 0)
        {
            formats_.resize(format_count);
            vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device().get(), window_surface_.surface(), &format_count,
                                                 formats_.data());
        }
        else
        {
            formats_.clear();
        }
    }

    void Swapchain::get_present_mode_info()
    {
        uint32_t presentation_mode_count;
        vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device().get(), window_surface_.surface(),
                                                  &presentation_mode_count, nullptr);

        if (presentation_mode_count != 0)
        {
            present_modes_.resize(presentation_mode_count);
            vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device().get(), window_surface_.surface(),
                                                      &presentation_mode_count, present_modes_.data());
        }
        else
        {
            present_modes_.clear();
        }
    }

    VkSurfaceFormatKHR Swapchain::get_best_surface_format()
    {
        for (const auto& format : formats_)
        {
            if (format.format == VK_FORMAT_B8G8R8A8_SRGB && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
            {
                return format;
            }
        }

        return formats_[0];
    }

    VkExtent2D Swapchain::get_best_swap_extent()
    {
        if (capabilities_.currentExtent.width != UINT32_MAX)

            return capabilities_.currentExtent;

        const uint32_t width = window_surface_.width();
        const uint32_t height = window_surface_.height();

        const VkExtent2D actualExtent = {
            std::clamp(width, capabilities_.minImageExtent.width, capabilities_.maxImageExtent.width),
            std::clamp(height, capabilities_.minImageExtent.height, capabilities_.maxImageExtent.height)};

        return actualExtent;
    }

    VkPresentModeKHR Swapchain::get_best_present_mode()
    {
        for (const auto& mode : present_modes_)
        {
            if (mode == VK_PRESENT_MODE_MAILBOX_KHR)
                return mode;
        }

        return VK_PRESENT_MODE_FIFO_KHR;
    }

    uint32_t Swapchain::get_best_image_count()
    {
        const auto image_count = capabilities_.minImageCount + 1;

        if (image_count > capabilities_.maxImageCount && capabilities_.maxImageCount > 0)
            return capabilities_.maxImageCount;

        return image_count;
    }

    VkSharingMode Swapchain::get_sharing_mode(const QueueFamilyIndices& indices)
    {
        const auto graphic_happening_on_different_queue_than_presentation =
            indices.graphics_family != indices.present_family;

        return graphic_happening_on_different_queue_than_presentation ? VK_SHARING_MODE_CONCURRENT
                                                                      : VK_SHARING_MODE_EXCLUSIVE;
    }

    Swapchain::inner_t& Swapchain::get()
    {
        return swapchain_;
    }

    const Swapchain::inner_t& Swapchain::get() const
    {
        return swapchain_;
    }

    Swapchain::physical_device_t& Swapchain::physical_device()
    {
        return physical_device_;
    }

    const Swapchain::physical_device_t& Swapchain::physical_device() const
    {
        return physical_device_;
    }

    Swapchain::logical_device_t& Swapchain::logical_device()
    {
        return logical_device_;
    }

    const Swapchain::logical_device_t& Swapchain::logical_device() const
    {
        return logical_device_;
    }

    const VkSurfaceCapabilitiesKHR& Swapchain::capabilities() const
    {
        return capabilities_;
    }

    const std::vector<VkSurfaceFormatKHR>& Swapchain::formats() const
    {
        return formats_;
    }

    const std::vector<VkPresentModeKHR>& Swapchain::present_modes() const
    {
        return present_modes_;
    }

    const VkExtent2D& Swapchain::swap_extent() const
    {
        return swap_extent_;
    }

    const VkSurfaceFormatKHR& Swapchain::surface_format() const
    {
        return surface_format_;
    }

    const Swapchain::image_view_vector_t& Swapchain::image_views() const
    {
        return image_views_;
    }

    size_t Swapchain::image_count() const
    {
        return image_views_.size();
    }
} // namespace engine
