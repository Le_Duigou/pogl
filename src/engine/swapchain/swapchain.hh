#pragma once

#include <cstddef>
#include <memory>
#include <vector>
#include <vulkan/vulkan_core.h>

#include "engine/devices/logical_device.hh"
#include "engine/image_view/swapchain_image_view.hh"

namespace engine
{
    class PhysicalDevice;
    class WindowSurface;
    struct QueueFamilyIndices;

    class Swapchain
    {
    public:
        using window_surface_t = WindowSurface;
        using physical_device_t = PhysicalDevice;
        using logical_device_t = LogicalDevice;
        using inner_t = VkSwapchainKHR;
        using image_view_t = SwapchainImageView;
        using image_view_vector_t = std::vector<std::unique_ptr<image_view_t>>;
        using image_vector_t = std::vector<VkImage>;

        Swapchain(window_surface_t& surface, physical_device_t& physical_device, logical_device_t& logical_device);
        ~Swapchain();

        bool is_supported() const;

        inner_t& get();
        const inner_t& get() const;

        physical_device_t& physical_device();
        const physical_device_t& physical_device() const;

        logical_device_t& logical_device();
        const logical_device_t& logical_device() const;

        const VkSurfaceCapabilitiesKHR& capabilities() const;

        const std::vector<VkSurfaceFormatKHR>& formats() const;

        const std::vector<VkPresentModeKHR>& present_modes() const;

        const VkExtent2D& swap_extent() const;

        const VkSurfaceFormatKHR& surface_format() const;

        const image_view_vector_t& image_views() const;

        size_t image_count() const;

    private:
        void get_info();
        void get_capabilities_info();
        void get_format_info();
        void get_present_mode_info();

        void setup();
        void setup_swapchain();
        void setup_images();
        void setup_image_views();

        void cleanup();
        void cleanup_swapchain();
        void cleanup_image_views();

        VkSurfaceFormatKHR get_best_surface_format();
        VkPresentModeKHR get_best_present_mode();
        VkExtent2D get_best_swap_extent();
        uint32_t get_best_image_count();
        VkSharingMode get_sharing_mode(const QueueFamilyIndices& indices);

    private:
        window_surface_t& window_surface_;
        physical_device_t& physical_device_;
        logical_device_t& logical_device_;

        inner_t swapchain_;

        image_vector_t images_;
        image_view_vector_t image_views_;

        VkSurfaceCapabilitiesKHR capabilities_;
        std::vector<VkSurfaceFormatKHR> formats_;
        std::vector<VkPresentModeKHR> present_modes_;
        VkExtent2D swap_extent_;
        VkSurfaceFormatKHR surface_format_;
    };

} // namespace engine
