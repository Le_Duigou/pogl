#include "renderer.hh"

#include <cstddef>
#include <memory>

#include "engine/camera/camera.hh"
#include "engine/memory_manager/memory_manager.hh"
#include "engine/mesh/mesh_manager.hh"
#include "engine/mesh/static_mesh.hh"
#include "engine/texture/texture_manager.hh"
#include "engine/uniform_buffer_object/uniform_buffer_object.hh"
#include "engine/units/units.hh"
#include "engine/vertex/vertex.hh"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace engine
{
    Renderer::Renderer(const logical_device_t& device, const command_pool_t& command_pool, memory_t& memory_manager)
        : device_(device)
        , command_pool_(&command_pool)
        , memory_manager_(memory_manager)
        , is_lighting_dirty_(false)
    {
        setup();
    }

    Renderer::~Renderer()
    {
        cleanup();
    }

    void Renderer::set_command_pool(const command_pool_t& command_pool)
    {
        command_pool_ = &command_pool;
    }

    Renderer::mesh_t& Renderer::add_mesh(const mesh_t::vertices_t& vertices, const mesh_t::indices_t& indices)
    {
        meshes().emplace_back(std::make_unique<mesh_t>(combined_mesh(), vertices, indices));

        return *meshes().back();
    }

    Renderer::mesh_t& Renderer::add_mesh(const std::string& path)
    {
        meshes().emplace_back(std::make_unique<mesh_t>(combined_mesh(), path));

        return *meshes().back();
    }

    void Renderer::update(time_delta_t delta, const KeyPressed& keys)
    {
        update_user_callback(delta, keys);

        update_vertex_buffer(delta);
        update_ubo(delta);

        end_update_frame();
    }

    void Renderer::update_user_callback(time_delta_t delta, const KeyPressed& keys)
    {
        if (update_callback_)
        {
            (*update_callback_)(delta, *this, keys);
        }
    }

    void Renderer::update_vertex_buffer(time_delta_t)
    {
        if (combined_mesh().is_dirty())
            populate_vertex_buffer();
    }

    void Renderer::update_ubo(time_delta_t)
    {
        if (camera().is_dirty() || is_lighting_dirty_ || is_ubo_dirty_)
            populate_ubo();
    }

    void Renderer::update_textures(time_delta_t)
    {
        if (memory_manager_.texture_manager().is_dirty())
        {
            memory_manager_.rebuild_descriptor_pool();
        }
    }

    void Renderer::populate_vertex_buffer()
    {
        memory_manager_.populate_vertex_buffer(*command_pool_, combined_mesh().get_all_vertices(),
                                               combined_mesh().get_all_indices(), combined_mesh().get_all_commands());
    }

    void Renderer::populate_ubo()
    {
        const auto ubo = get_current_ubo();

        for (size_t i = 0; i < memory_manager_.uniform_buffer_count(); i++)
            memory_manager_.populate_uniform_data(i, ubo);
    }

    void Renderer::end_update_frame()
    {
        camera().set_dirty(false);
        combined_mesh().set_dirty(false);
        memory_manager_.texture_manager().set_dirty(false);
        is_lighting_dirty_ = false;
        is_ubo_dirty_ = false;
    }

    void Renderer::setup()
    {
        setup_camera();
        setup_combined_mesh();
    }

    void Renderer::cleanup()
    {
        cleanup_camera();
    }

    void Renderer::setup_camera()
    {
        const auto original_camera_coordinates = glm::vec3(0.0f, -5.0f, 2.0f);
        const auto original_camera_look_at = glm::vec3(0.0f, 0.0f, 0.0f);

        camera_ =
            std::make_unique<camera_t>(original_camera_coordinates, original_camera_look_at, 50.f, 1.f, 0.1f, 1000.f);
    }

    void Renderer::setup_lighting()
    {
        light_ = {
            .location = {0, 0, 0},
            .color = {1, 1, 1},
            .intensity = 1,
        };
        ambient_color_ = {0, 0, 0};
    }

    void Renderer::setup_combined_mesh()
    {
        mesh_manager_ = std::make_unique<combined_mesh_t>();
        model_transform_ = mat4_t(1.0);
    }

    void Renderer::cleanup_camera()
    {
        camera_.reset();
    }

    void Renderer::cleanup_combined_mesh()
    {
        mesh_manager_.reset();
    }

    Renderer::ubo_t Renderer::get_current_ubo() const
    {
        return UniformBufferObject{
            .model = model_transform(),
            .view = camera().transform(),
            .projection = camera().projection_matrix(),
            .eye_position = camera().location(),
            .ambient_color = ambient_color_,
            .light_location = light_.location,
            .light_color = light_.color,
            .light_intensity = light_.intensity,
        };
    }

    void Renderer::set_camera_aspect_ratio(float aspect_ratio)
    {
        camera().set_aspect_ratio(aspect_ratio);
    }

    void Renderer::set_texture_set(const std::string& set_name, const std::string& diffuse,
                                   const std::string& occlusion_roughness_metallic, const std::string& normal,
                                   const std::string& environemnt)
    {
        memory_manager_.texture_manager().set_texture_set(set_name, *command_pool_, diffuse,
                                                          occlusion_roughness_metallic, normal, environemnt);
        memory_manager_.rebuild_descriptor_pool();
    }

    void Renderer::set_update_callback(update_callback_t callback)
    {
        update_callback_ = callback;
    }

    void Renderer::reset_update_callback()
    {
        update_callback_ = std::nullopt;
    }

    mat4_t Renderer::model_transform() const
    {
        return model_transform_;
    }

    void Renderer::set_model_transform(mat4_t transform)
    {
        model_transform_ = transform;
        is_ubo_dirty_ = true;
    }

    const Renderer::memory_t& Renderer::memory_manager() const
    {
        return memory_manager_;
    }

    Renderer::camera_t& Renderer::camera()
    {
#ifndef NDEBUG
        if (!camera_)
            throw std::runtime_error("Querying camera before initializing it");
#endif

        return *camera_;
    }

    const Renderer::camera_t& Renderer::camera() const
    {
#ifndef NDEBUG
        if (!camera_)
            throw std::runtime_error("Querying camera before initializing it");
#endif

        return *camera_;
    }

    Renderer::combined_mesh_t& Renderer::combined_mesh()
    {
#ifndef NDEBUG
        if (!mesh_manager_)
            throw std::runtime_error("Querying combined mesh before initializing it");
#endif

        return *mesh_manager_;
    }

    const Renderer::combined_mesh_t& Renderer::combined_mesh() const
    {
#ifndef NDEBUG
        if (!mesh_manager_)
            throw std::runtime_error("Querying combined mesh before initializing it");
#endif

        return *mesh_manager_;
    }

    Renderer::mesh_collection_t& Renderer::meshes()
    {
        return meshes_;
    }

    const Renderer::mesh_collection_t& Renderer::meshes() const
    {
        return meshes_;
    }

    const PointLight& Renderer::light() const
    {
        return light_;
    }

    void Renderer::set_light(const PointLight& light)
    {
        light_ = light;
        is_lighting_dirty_ = true;
    }

    vec3_t Renderer::ambient_color() const
    {
        return ambient_color_;
    }

    void Renderer::set_ambient_color(vec3_t color)
    {
        ambient_color_ = color;
        is_lighting_dirty_ = true;
    }

} // namespace engine
