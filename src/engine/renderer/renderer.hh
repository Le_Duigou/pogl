#pragma once

#include <cstddef>
#include <functional>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "engine/light/light.hh"
#include "engine/mesh/static_mesh.hh"
#include "engine/units/units.hh"

namespace engine
{
    class Camera;
    class MeshManager;
    class CommandPool;
    class LogicalDevice;
    class MemoryManager;
    class KeyPressed;
    struct UniformBufferObject;

    class Renderer
    {
    public:
        using memory_t = MemoryManager;
        using logical_device_t = LogicalDevice;
        using command_pool_t = CommandPool;
        using camera_t = Camera;
        using ubo_t = UniformBufferObject;
        using time_delta_t = double;
        using combined_mesh_t = MeshManager;
        using mesh_t = StaticMesh;
        using mesh_collection_t = std::vector<std::unique_ptr<mesh_t>>;
        using update_callback_t = std::function<void(time_delta_t, Renderer&, const KeyPressed&)>;

        Renderer(const logical_device_t& device, const command_pool_t& command_pool, memory_t& memory_manager);
        ~Renderer();

        void set_command_pool(const command_pool_t& command_pool);

        const memory_t& memory_manager() const;

        camera_t& camera();
        const camera_t& camera() const;

        mesh_collection_t& meshes();
        const mesh_collection_t& meshes() const;

        void set_camera_aspect_ratio(float aspect_ratio);
        void set_texture_set(const std::string& set_name, const std::string& diffuse,
                             const std::string& occlusion_roughness_metallic, const std::string& normal,
                             const std::string& environemnt);

        void update(time_delta_t delta, const KeyPressed& keys);

        void set_update_callback(update_callback_t callback);
        void reset_update_callback();

        mesh_t& add_mesh(const mesh_t::vertices_t& vertices, const mesh_t::indices_t& indices);
        mesh_t& add_mesh(const std::string& path);

        const PointLight& light() const;
        void set_light(const PointLight& light);

        vec3_t ambient_color() const;
        void set_ambient_color(vec3_t color);

        mat4_t model_transform() const;
        void set_model_transform(mat4_t transform);

    private:
        void setup();
        void cleanup();

        void setup_camera();
        void setup_combined_mesh();
        void setup_lighting();
        void cleanup_camera();
        void cleanup_lighting();
        void cleanup_combined_mesh();

        void update_user_callback(time_delta_t delta, const KeyPressed& keys);
        void update_vertex_buffer(time_delta_t delta);
        void update_ubo(time_delta_t delta);
        void update_textures(time_delta_t delta);

        void populate_vertex_buffer();
        void populate_ubo();

        void end_update_frame();

        ubo_t get_current_ubo() const;

    private:
        combined_mesh_t& combined_mesh();
        const combined_mesh_t& combined_mesh() const;

    private:
        const logical_device_t& device_;
        command_pool_t const* command_pool_;
        memory_t& memory_manager_;
        std::unique_ptr<camera_t> camera_;
        std::unique_ptr<combined_mesh_t> mesh_manager_;
        mesh_collection_t meshes_;
        std::optional<update_callback_t> update_callback_;
        PointLight light_;
        vec3_t ambient_color_;
        mat4_t model_transform_;
        bool is_lighting_dirty_;
        bool is_ubo_dirty_;
    };
} // namespace engine
