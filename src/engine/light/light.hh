#pragma once
#include "engine/units/units.hh"

namespace engine
{
    struct PointLight
    {
        vec3_t location;
        vec3_t color;
        float intensity;
    };
} // namespace engine
