#include "program.hh"

#include <exception>
#include <iostream>
#include <memory>

#include "engine/command_pool/command_pool.hh"
#include "engine/command_pool/command_recorder.hh"
#include "engine/devices/logical_device.hh"
#include "engine/devices/physical_device.hh"
#include "engine/engine_instance/engine_instance.hh"
#include "engine/frame_buffer/frame_buffer.hh"
#include "engine/frame_renderer/frame_renderer.hh"
#include "engine/framerate/framerate.hh"
#include "engine/image_view/swapchain_image_view.hh"
#include "engine/memory_manager/memory_manager.hh"
#include "engine/pipelines/pipeline_set.hh"
#include "engine/renderer/renderer.hh"
#include "engine/swapchain/swapchain.hh"
#include "engine/viewport/viewport.hh"
#include "engine/window/window.hh"
#include "engine/window_surface/window_surface.hh"

namespace engine
{
    Program::Program(const std::string& window_name, unsigned int width, unsigned int height,
                     const options::Options& options)
        : options_(options)
    {
        setup(window_name, width, height);
    }

    Program::~Program()
    {
        cleanup();
    }

    void Program::run()
    {
#ifndef NDEBUG
        std::cout << "Ready to start main loop.\n";
#endif

        try
        {
            main_loop();
        }
        catch (std::exception&)
        {
            // The order of call in cleanup is important, so we have to call it
            // directly before throwing
            cleanup();
            throw;
        }
    }

    void Program::setup(const std::string& window_name, unsigned int width, unsigned int height)
    {
#ifndef NDEBUG
        std::cout << "Setting up program components.\n";
#endif
        try
        {
            setup_window(window_name, width, height);
            setup_engine_instance();
            setup_surface();
            setup_devices();
            setup_swapchain();
            setup_viewport();
            setup_memory_manager();
            setup_pipeline();
            setup_frame_buffer_list();
            setup_command_pool();
            setup_renderer();
            setup_frame_renderer();
            record_pipeline();
            update_window_ratio();
        }
        catch (std::exception&)
        {
            // The order of call in cleanup is important, so we have to call it
            // directly before throwing
            cleanup();
            throw;
        }
    }

    void Program::cleanup()
    {
#ifndef NDEBUG
        std::cout << "Cleaning up program components.\n";
#endif
        cleanup_frame_renderer();
        cleanup_renderer();
        cleanup_command_pool();
        cleanup_frame_buffer_list();
        cleanup_pipeline();
        cleanup_memory_manager();
        cleanup_viewport();
        cleanup_swapchain();
        cleanup_devices();
        cleanup_surface();
        cleanup_engine_instance();
        cleanup_window();
    }

    void Program::recreate_swapchain()
    {
        await_device();
#ifndef NDEBUG
        std::cout << "Recreate swapchain.\n";
#endif
        try
        {
            // Cleanup components (stateful components like memory manager or renderer of not cleaned up)
            cleanup_frame_renderer();
            cleanup_command_pool();
            cleanup_frame_buffer_list();
            cleanup_pipeline();
            cleanup_viewport();
            cleanup_swapchain();

            // Recreate components
            setup_swapchain();
            setup_viewport();
            memory_manager().rebuild_descriptor_pool();
            setup_pipeline();
            setup_frame_buffer_list();
            setup_command_pool();
            renderer().set_command_pool(command_pool());
            setup_frame_renderer();

            record_pipeline();
            update_window_ratio();
        }
        catch (std::exception&)
        {
            // The order of call in cleanup is important, so we have to call it
            // directly before throwing
            cleanup();
            throw;
        }
    }

    void Program::record_pipeline()
    {
        const auto recorder = CommandRecorder{pipeline(), swapchain().swap_extent(), memory_manager()};
        command_pool_->record_buffers(frame_buffer_list(), memory_manager().descriptor_pool(), recorder);
    }

    void Program::update_window_ratio()
    {
        float ratio = static_cast<float>(window().width()) / window().height();
        renderer().set_camera_aspect_ratio(ratio);
    }

    void Program::main_loop()
    {
        Framerate framerate;

        framerate.start();
        while (!window_->should_close())
        {
            const auto delta = framerate.tick();

            update(delta);
            draw();

            recreate_swapchain_if_needed();
        }
        framerate.stop();

        await_device();
    }

    void Program::update(time_delta_t delta)
    {
        window().poll_events();

        renderer().update(delta, window().key_pressed_last_frame());
    }

    void Program::draw()
    {
        re_record_pipeline_if_needed();

        frame_renderer().render();
    }

    void Program::re_record_pipeline_if_needed()
    {
        if (memory_manager().is_pipeline_obsolete())
        {
            record_pipeline();
            memory_manager().reset_is_pipeline_obsolete();
        }
    }

    void Program::recreate_swapchain_if_needed()
    {
        if (frame_renderer().should_recreate_swapchain() || window().has_been_resized())
        {
            window().reset_has_been_resized();
            recreate_swapchain();
        }
    }

    void Program::await_device() const
    {
        vkDeviceWaitIdle(logical_device().get());
    }

    void Program::setup_command_pool()
    {
        command_pool_ =
            std::make_unique<command_pool_t>(physical_device(), logical_device(), swapchain().image_count());
    }

    void Program::setup_frame_renderer()
    {
        frame_renderer_ = std::make_unique<frame_renderer_t>(swapchain(), command_pool());
    }

    void Program::setup_devices()
    {
        physical_device_ = std::make_unique<physical_device_t>(engine_instance(), surface());
        logical_device_ = std::make_unique<logical_device_t>(physical_device(), options_);
    }

    void Program::setup_frame_buffer_list()
    {
        frame_buffer_list_ = std::make_unique<framebuffer_list_t>(swapchain(), pipeline_->render_pass());
    }

    void Program::setup_memory_manager()
    {
        memory_manager_ = std::make_unique<memory_manager_t>(logical_device(), swapchain().image_count());
    }

    void Program::setup_pipeline()
    {
        const auto format = swapchain().surface_format().format;
        pipeline_ = std::make_unique<pipeline_t>(logical_device(), viewport(), format, memory_manager());
    }

    void Program::setup_renderer()
    {
        renderer_ = std::make_unique<renderer_t>(logical_device(), command_pool(), memory_manager());
    }

    void Program::setup_surface()
    {
        surface_ = std::make_unique<window_surface_t>(window(), engine_instance());
    }

    void Program::setup_swapchain()
    {
        swapchain_ = std::make_unique<swapchain_t>(surface(), physical_device(), logical_device());
    }

    void Program::setup_viewport()
    {
        viewport_ = std::make_unique<viewport_t>(window().width(), window().height());
    }

    void Program::setup_engine_instance()
    {
        engine_instance_ =
            std::make_unique<instance_t>(window().name().c_str(), window().engine_extensions(), options_);
    }

    void Program::setup_window(const std::string& window_name, unsigned int width, unsigned int height)
    {
        window_ = std::make_unique<window_t>(window_name, width, height);
    }

    void Program::cleanup_command_pool()
    {
        command_pool_.reset();
    }

    void Program::cleanup_frame_renderer()
    {
        frame_renderer_.reset();
    }

    void Program::cleanup_devices()
    {
        logical_device_.reset();
        physical_device_.reset();
    }

    void Program::cleanup_frame_buffer_list()
    {
        frame_buffer_list_.reset();
    }

    void Program::cleanup_memory_manager()
    {
        memory_manager_.reset();
    }

    void Program::cleanup_pipeline()
    {
        pipeline_.reset();
    }

    void Program::cleanup_renderer()
    {
        renderer_.reset();
    }

    void Program::cleanup_surface()
    {
        surface_.reset();
    }

    void Program::cleanup_swapchain()
    {
        swapchain_.reset();
    }

    void Program::cleanup_viewport()
    {
        viewport_.reset();
    }

    void Program::cleanup_engine_instance()
    {
        engine_instance_.reset();
    }

    void Program::cleanup_window()
    {
        window_.reset();
    }

} // namespace engine
