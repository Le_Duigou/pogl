#include <memory>
#include <string>
#include <vulkan/vulkan_core.h>

#ifndef PROGRAM__H
#    define PROGRAM__H 1

namespace options
{
    struct Options;
}
namespace engine
{
    class CommandPool;
    class EngineInstance;
    class FrameBufferList;
    class FrameRenderer;
    class LogicalDevice;
    class MemoryManager;
    class PhysicalDevice;
    class PipelineSet;
    class Renderer;
    class Swapchain;
    class Viewport;
    class Window;
    class WindowSurface;

    class Program
    {
    public:
        using time_delta_t = double;

        Program(const std::string& window_name, unsigned int width, unsigned int height,
                const options::Options& options);
        ~Program();

        void run();

    private:
        void setup(const std::string& window_name, unsigned int width, unsigned int height);
        void cleanup();

        void main_loop();

        void recreate_swapchain();

        void update(time_delta_t delta);
        void draw();
        void recreate_swapchain_if_needed();
        void await_device() const;

        void setup_command_pool();
        void setup_devices();
        void setup_engine_instance();
        void setup_frame_buffer_list();
        void setup_frame_renderer();
        void setup_memory_manager();
        void setup_pipeline();
        void setup_renderer();
        void setup_surface();
        void setup_swapchain();
        void setup_viewport();
        void setup_window(const std::string& window_name, unsigned int width, unsigned int height);

        void re_record_pipeline_if_needed();
        void record_pipeline();
        void update_window_ratio();

        void cleanup_command_pool();
        void cleanup_devices();
        void cleanup_engine_instance();
        void cleanup_frame_buffer_list();
        void cleanup_frame_renderer();
        void cleanup_memory_manager();
        void cleanup_pipeline();
        void cleanup_renderer();
        void cleanup_surface();
        void cleanup_swapchain();
        void cleanup_viewport();
        void cleanup_window();

    private:
        using command_pool_t = CommandPool;
        using frame_renderer_t = FrameRenderer;
        using framebuffer_list_t = FrameBufferList;
        using instance_t = EngineInstance;
        using logical_device_t = LogicalDevice;
        using memory_manager_t = MemoryManager;
        using physical_device_t = PhysicalDevice;
        using pipeline_t = PipelineSet;
        using renderer_t = Renderer;
        using swapchain_t = Swapchain;
        using viewport_t = Viewport;
        using window_surface_t = WindowSurface;
        using window_t = Window;

    private:
        command_pool_t& command_pool();
        const command_pool_t& command_pool() const;

        frame_renderer_t& frame_renderer();
        const frame_renderer_t& frame_renderer() const;

        framebuffer_list_t& frame_buffer_list();
        const framebuffer_list_t& frame_buffer_list() const;

        instance_t& engine_instance();
        const instance_t& engine_instance() const;

        logical_device_t& logical_device();
        const logical_device_t& logical_device() const;

        memory_manager_t& memory_manager();
        const memory_manager_t& memory_manager() const;

        physical_device_t& physical_device();
        const physical_device_t& physical_device() const;

        pipeline_t& pipeline();
        const pipeline_t& pipeline() const;

        swapchain_t& swapchain();
        const swapchain_t& swapchain() const;

        viewport_t& viewport();
        const viewport_t& viewport() const;

        window_surface_t& surface();
        const window_surface_t& surface() const;

        window_t& window();
        const window_t& window() const;

    public:
        renderer_t& renderer();
        const renderer_t& renderer() const;

    private:
        std::unique_ptr<command_pool_t> command_pool_;
        std::unique_ptr<frame_renderer_t> frame_renderer_;
        std::unique_ptr<framebuffer_list_t> frame_buffer_list_;
        std::unique_ptr<instance_t> engine_instance_;
        std::unique_ptr<logical_device_t> logical_device_;
        std::unique_ptr<memory_manager_t> memory_manager_;
        std::unique_ptr<physical_device_t> physical_device_;
        std::unique_ptr<pipeline_t> pipeline_;
        std::unique_ptr<renderer_t> renderer_;
        std::unique_ptr<swapchain_t> swapchain_;
        std::unique_ptr<viewport_t> viewport_;
        std::unique_ptr<window_surface_t> surface_;
        std::unique_ptr<window_t> window_;

    private:
        const options::Options& options_;
    };
} // namespace engine

#    include "program.hxx"
#endif
