#pragma once

#ifndef PROGRAM__H
#    include "program.hh"
#endif

namespace engine
{
    inline Program::command_pool_t& Program::command_pool()
    {
#ifndef NDEBUG
        if (!command_pool_)
            throw std::runtime_error("Querying command pool before initializing it");
#endif

        return *command_pool_;
    }

    inline const Program::command_pool_t& Program::command_pool() const
    {
#ifndef NDEBUG
        if (!command_pool_)
            throw std::runtime_error("Querying command pool before initializing it");
#endif

        return *command_pool_;
    }

    inline Program::frame_renderer_t& Program::frame_renderer()
    {
#ifndef NDEBUG
        if (!frame_renderer_)
            throw std::runtime_error("Querying frame renderer before initializing it");
#endif

        return *frame_renderer_;
    }

    inline const Program::frame_renderer_t& Program::frame_renderer() const
    {
#ifndef NDEBUG
        if (!frame_renderer_)
            throw std::runtime_error("Querying frame renderer before initializing it");
#endif

        return *frame_renderer_;
    }

    inline Program::framebuffer_list_t& Program::frame_buffer_list()
    {
#ifndef NDEBUG
        if (!frame_buffer_list_)
            throw std::runtime_error("Querying frame buffer list before initializing it");
#endif

        return *frame_buffer_list_;
    }

    inline const Program::framebuffer_list_t& Program::frame_buffer_list() const
    {
#ifndef NDEBUG
        if (!frame_buffer_list_)
            throw std::runtime_error("Querying frame buffer list before initializing it");
#endif

        return *frame_buffer_list_;
    }

    inline Program::instance_t& Program::engine_instance()
    {
#ifndef NDEBUG
        if (!engine_instance_)
            throw std::runtime_error("Querying engine instance before initializing it");
#endif

        return *engine_instance_;
    }

    inline const Program::instance_t& Program::engine_instance() const
    {
#ifndef NDEBUG
        if (!engine_instance_)
            throw std::runtime_error("Querying engine instance before initializing it");
#endif

        return *engine_instance_;
    }

    inline Program::logical_device_t& Program::logical_device()
    {
#ifndef NDEBUG
        if (!physical_device_)
            throw std::runtime_error("Querying logical device but no logical device existing");
#endif

        return *logical_device_;
    }

    inline const Program::logical_device_t& Program::logical_device() const
    {
#ifndef NDEBUG
        if (!physical_device_)
            throw std::runtime_error("Querying logical device but no logical device existing");
#endif

        return *logical_device_;
    }

    inline Program::memory_manager_t& Program::memory_manager()
    {
#ifndef NDEBUG
        if (!memory_manager_)
            throw std::runtime_error("Querying memory manager before initializing it");
#endif

        return *memory_manager_;
    }

    inline const Program::memory_manager_t& Program::memory_manager() const
    {
#ifndef NDEBUG
        if (!memory_manager_)
            throw std::runtime_error("Querying memory manager before initializing it");
#endif

        return *memory_manager_;
    }

    inline Program::physical_device_t& Program::physical_device()
    {
#ifndef NDEBUG
        if (!physical_device_)
            throw std::runtime_error("Querying physical device but no physical device existing");
#endif

        return *physical_device_;
    }

    inline const Program::physical_device_t& Program::physical_device() const
    {
#ifndef NDEBUG
        if (!physical_device_)
            throw std::runtime_error("Querying physical device but no physical device existing");
#endif

        return *physical_device_;
    }

    inline Program::pipeline_t& Program::pipeline()
    {
#ifndef NDEBUG
        if (!pipeline_)
            throw std::runtime_error("Querying pipeline before initializing it");
#endif

        return *pipeline_;
    }

    inline const Program::pipeline_t& Program::pipeline() const
    {
#ifndef NDEBUG
        if (!pipeline_)
            throw std::runtime_error("Querying pipeline before initializing it");
#endif

        return *pipeline_;
    }

    inline Program::renderer_t& Program::renderer()
    {
#ifndef NDEBUG
        if (!renderer_)
            throw std::runtime_error("Querying renderer before initializing it");
#endif

        return *renderer_;
    }

    inline const Program::renderer_t& Program::renderer() const
    {
#ifndef NDEBUG
        if (!renderer_)
            throw std::runtime_error("Querying renderer before initializing it");
#endif

        return *renderer_;
    }

    inline Program::swapchain_t& Program::swapchain()
    {
#ifndef NDEBUG
        if (!swapchain_)
            throw std::runtime_error("Querying swapchain before initializing it");
#endif

        return *swapchain_;
    }

    inline const Program::swapchain_t& Program::swapchain() const
    {
#ifndef NDEBUG
        if (!swapchain_)
            throw std::runtime_error("Querying swapchain before initializing it");
#endif

        return *swapchain_;
    }

    inline Program::viewport_t& Program::viewport()
    {
#ifndef NDEBUG
        if (!surface_)
            throw std::runtime_error("Querying viewport before initializing it");
#endif
        return *viewport_;
    }

    inline const Program::viewport_t& Program::viewport() const
    {
#ifndef NDEBUG
        if (!surface_)
            throw std::runtime_error("Querying viewport before initializing it");
#endif
        return *viewport_;
    }

    inline Program::window_surface_t& Program::surface()
    {
#ifndef NDEBUG
        if (!surface_)
            throw std::runtime_error("Querying window surface before initializing it");
#endif

        return *surface_;
    }

    inline const Program::window_surface_t& Program::surface() const
    {
#ifndef NDEBUG
        if (!surface_)
            throw std::runtime_error("Querying window surface before initializing it");
#endif

        return *surface_;
    }

    inline Program::window_t& Program::window()
    {
#ifndef NDEBUG
        if (!window_)
            throw std::runtime_error("Querying window before initializing it");
#endif

        return *window_;
    }

    inline const Program::window_t& Program::window() const
    {
#ifndef NDEBUG
        if (!window_)
            throw std::runtime_error("Querying window before initializing it");
#endif

        return *window_;
    }
} // namespace engine
