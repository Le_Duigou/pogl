#include "image.hh"

#include <cstdint>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <vulkan/vulkan_core.h>

#include "engine/buffer_memory/buffer_memory.hh"
#include "engine/command_pool/command_pool.hh"
#include "engine/devices/logical_device.hh"
#include "engine/error/vulkan_errors.hh"
#include "engine/single_commands/image_layout_transition.hh"
#include "engine/single_commands/image_memcpy.hh"

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

namespace engine
{
    Image::Image(const logical_device_t& device, const command_pool_t& command_pool, const std::string& path,
                 bool use_mipmaps, format_t format)
        : device_(device)
        , command_pool_(command_pool)
        , path_(path)
        , format(format)
    {
        setup(use_mipmaps);
    }

    Image::~Image()
    {
        cleanup();
    }

    void Image::setup(bool use_mipmaps)
    {
        load_image(use_mipmaps);
    }

    void Image::cleanup()
    {
        cleanup_image();
    }

    void Image::load_image(bool use_mipmaps)
    {
        buffer_t staging_buffer = load_image_to_buffer_and_set_dimensions(width_, height_, size_);
        setup_image(width_, height_, use_mipmaps);
        copy_image_from_staging_buffer(staging_buffer, width_, height_);
    }

    Image::buffer_t Image::load_image_to_buffer_and_set_dimensions(dimension_t& width, dimension_t& height,
                                                                   VkDeviceSize& size)
    {
        int loaded_width;
        int loaded_height;
        int channel_number;

        byte_t* pixels = stbi_load(path_.c_str(), &loaded_width, &loaded_height, &channel_number, STBI_rgb_alpha);

        width = loaded_width;
        height = loaded_height;
        size = width * height * 4;

        if (!pixels)
            throw std::runtime_error("Unable to load image named " + path_);

        constexpr auto usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        constexpr auto properties = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
        auto buffer = buffer_t{device_};
        buffer.initialize(size * sizeof(byte_t), usage, properties);

        map_byte_image_to_memory(pixels, buffer, size * sizeof(byte_t));

        stbi_image_free(pixels);

        return buffer;
    }

    void Image::setup_image(dimension_t width, dimension_t height, bool use_mipmaps)
    {
        constexpr auto usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
        constexpr auto properties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

        mipmap_level_ = use_mipmaps ? std::floor(std::log2(std::max(width, height))) + 1 : 1;
        const auto settings = ImageBufferBufferSettings{
            .usage = usage,
            .format = format,
            .mip_level = mipmap_level_,
        };

        image_ = std::make_unique<image_t>(device_);
        image_->initialize({width, height, 1}, settings, properties);
        layouts_.clear();
        layouts_.resize(mipmap_level_, VK_IMAGE_LAYOUT_UNDEFINED);
    }

    void Image::cleanup_image()
    {
        image_.reset();
    }

    void Image::copy_image_from_staging_buffer(buffer_t& staging_buffer, dimension_t width, dimension_t height)
    {
        constexpr auto TRANSFER_LAYOUT = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        constexpr auto FINAL_LAYOUT = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

        ImageLayoutTransition{{command_pool_}}(*this, TRANSFER_LAYOUT);
        DeviceImageMemcpy{command_pool_}(staging_buffer.buffer(), image_->buffer(), {width, height, 1}).execute();

        if (mipmap_level_ == 1)
            ImageLayoutTransition{{command_pool_}}(*this, FINAL_LAYOUT);
        // We consider that it's the responsability of the caller (the Texture object) to transition the layout to read
        // only AFTER generating mipmaps
    }

    void Image::map_byte_image_to_memory(byte_t* bytes, const buffer_t& buffer, size_t size)
    {
        void* data;
        const auto error = vkMapMemory(device_.get(), buffer.memory(), 0, size, 0, &data);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Unable to start image memory\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }

        memcpy(data, bytes, size);
        vkUnmapMemory(device_.get(), buffer.memory());
    }

    Image::layout_t Image::layout(size_t level) const
    {
        return layouts_[level];
    }

    const Image::image_t& Image::buffer() const
    {
#ifndef NDEBUG
        if (!image_)
            throw std::runtime_error("Querying inner image buffer before initializing it");
#endif
        return *image_;
    }

    uint32_t Image::mipmap_level() const
    {
        return mipmap_level_;
    }
} // namespace engine
