#pragma once

#include <cstddef>
#include <cstdint>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <vulkan/vulkan_core.h>

#include "engine/buffer_memory/fwd.hh"

namespace engine
{
    class LogicalDevice;
    class CommandPool;
    class ImageLayoutTransition;
    class GenerateMipMaps;

    class Image
    {
    public:
        using logical_device_t = LogicalDevice;
        using image_t = ImageBuffer;
        using buffer_t = SimpleBuffer;
        using layout_t = VkImageLayout;

        using command_pool_t = CommandPool;
        using byte_t = unsigned char;
        using dimension_t = uint32_t;
        using format_t = VkFormat;

    public:
        Image(const logical_device_t& device, const command_pool_t& command_pool, const std::string& path,
              bool use_mipmaps, format_t format);
        ~Image();

        layout_t layout(size_t level) const;
        const image_t& buffer() const;

        uint32_t mipmap_level() const;

    private:
        void setup(bool use_mipmaps);
        void cleanup();

        void load_image(bool use_mipmaps);

        buffer_t load_image_to_buffer_and_set_dimensions(dimension_t& width, dimension_t& height, VkDeviceSize& size);
        void map_byte_image_to_memory(byte_t* bytes, const buffer_t& buffer, VkDeviceSize size);
        void copy_image_from_staging_buffer(buffer_t& staging_buffer, dimension_t width, dimension_t height);

        void setup_image(dimension_t width, dimension_t height, bool use_mipmaps);
        void cleanup_image();

    private:
        const logical_device_t& device_;
        const command_pool_t& command_pool_;
        std::unique_ptr<image_t> image_;

        const std::string path_;
        dimension_t width_;
        dimension_t height_;
        VkDeviceSize size_;
        std::vector<layout_t> layouts_;
        uint32_t mipmap_level_;
        const format_t format;

        friend class ImageLayoutTransition;
        friend class GenerateMipMaps;
    };
} // namespace engine
