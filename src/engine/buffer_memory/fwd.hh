#pragma once
#include <vulkan/vulkan_core.h>

namespace engine
{
    template <typename T, typename U, typename V>
    class BufferMemory;
    struct ImageBufferBufferSettings;

    using SimpleBuffer = BufferMemory<VkBuffer, uint32_t, VkBufferUsageFlags>;
    using ImageBuffer = BufferMemory<VkImage, VkExtent3D, ImageBufferBufferSettings>;
} // namespace engine
