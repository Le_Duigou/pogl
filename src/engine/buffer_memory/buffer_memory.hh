#include <cstddef>
#include <cstdint>
#include <vulkan/vulkan_core.h>

#include "engine/devices/logical_device.hh"

#ifndef BUFFER_MEMORY_H
#    define BUFFER_MEMORY_H 1

namespace engine
{
    struct ImageBufferBufferSettings
    {
        VkBufferUsageFlags usage;
        VkFormat format = VK_FORMAT_R8G8B8A8_SRGB;
        uint32_t mip_level = 1;
    };

    template <typename T, typename U, typename V>
    class BufferMemory
    {
    public:
        using buffer_t = T;
        using dimension_t = U;
        using memory_t = VkDeviceMemory;
        using logical_device_t = LogicalDevice;
        using buffer_settings_t = V;
        using memory_settings_t = VkMemoryPropertyFlags;
        using memory_requirements_t = VkMemoryRequirements;

        BufferMemory(const logical_device_t& device);
        ~BufferMemory();

        void initialize(dimension_t size, buffer_settings_t usage, memory_settings_t properties);
        void resize(dimension_t size);

        buffer_t& buffer();
        const buffer_t& buffer() const;

        memory_t& memory();
        const memory_t& memory() const;

    private:
        void setup(dimension_t size, buffer_settings_t usage, memory_settings_t properties);
        void cleanup();

        void setup_buffer(dimension_t size, buffer_settings_t usage);
        void setup_memory(memory_settings_t properties);
        void bind_buffer_to_its_memory() const;

        void cleanup_buffer();
        void cleanup_memory();

        memory_requirements_t get_memory_requirement() const;
        uint32_t get_best_memory_type(const memory_requirements_t& requirements,
                                      VkMemoryPropertyFlags properties) const;

    private:
        const logical_device_t& device_;
        buffer_t buffer_;
        memory_t memory_;
        buffer_settings_t usage_;
        memory_settings_t properties_;
        dimension_t size_;
        bool is_initialized_;
    };

} // namespace engine

#    include "buffer_memory.hxx"
#endif
