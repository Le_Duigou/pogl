#pragma once

#include <cstdint>
#include <iostream>
#include <stdexcept>

#include "engine/error/vulkan_errors.hh"
#ifndef BUFFER_MEMORY_H
#    include "buffer_memory.hh"
#endif

namespace engine
{
    template <typename T, typename U, typename V>
    inline BufferMemory<T, U, V>::BufferMemory(const logical_device_t& device)
        : device_(device)
        , is_initialized_(false)
    {}

    template <typename T, typename U, typename V>
    inline BufferMemory<T, U, V>::~BufferMemory()
    {
        cleanup();
    }

    template <typename T, typename U, typename V>
    inline void BufferMemory<T, U, V>::initialize(dimension_t size, buffer_settings_t usage,
                                                  memory_settings_t properties)
    {
#ifndef NDEBUG
        if (is_initialized_)
            throw std::runtime_error("Double buffer initialization.");
#endif

        setup(size, usage, properties);

        usage_ = usage;
        properties_ = properties;
        is_initialized_ = true;
    }

    template <typename T, typename U, typename V>
    inline void BufferMemory<T, U, V>::resize(dimension_t size)
    {
#ifndef NDEBUG
        if (!is_initialized_)
            throw std::runtime_error("Resizing buffer before initialization.");
#endif

        if (size != size_)
        {
            cleanup();
            setup(size, usage_, properties_);
        }
    }

    template <typename T, typename U, typename V>
    inline void BufferMemory<T, U, V>::setup(dimension_t size, buffer_settings_t usage, memory_settings_t properties)
    {
        setup_buffer(size, usage);
        setup_memory(properties);
        bind_buffer_to_its_memory();
    }

    template <typename T, typename U, typename V>
    inline void BufferMemory<T, U, V>::cleanup()
    {
        if (is_initialized_)
        {
            cleanup_memory();
            cleanup_buffer();
        }
    }

    template <>
    inline void BufferMemory<VkBuffer, uint32_t, VkBufferUsageFlags>::setup_buffer(dimension_t size,
                                                                                   buffer_settings_t usage)
    {
        const auto buffer_create_info = VkBufferCreateInfo{
            .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .size = size,
            .usage = usage,
            .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
            .queueFamilyIndexCount = 0,
            .pQueueFamilyIndices = 0,
        };

        const auto error = vkCreateBuffer(device_.get(), &buffer_create_info, nullptr, &buffer_);
        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not create the buffer\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }

        size_ = size;
    }

    template <>
    inline void BufferMemory<VkImage, VkExtent3D, ImageBufferBufferSettings>::setup_buffer(dimension_t size,
                                                                                           buffer_settings_t settings)
    {
        auto usage = settings.usage;

        if (settings.mip_level > 1)
            usage = usage | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;

        const auto buffer_create_info = VkImageCreateInfo{
            .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .imageType = VK_IMAGE_TYPE_2D,
            .format = settings.format,
            .extent = size,
            .mipLevels = settings.mip_level,
            .arrayLayers = 1,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .tiling = VK_IMAGE_TILING_OPTIMAL,
            .usage = usage,
            .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
            .queueFamilyIndexCount = 0,
            .pQueueFamilyIndices = nullptr,
            .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        };

        const auto error = vkCreateImage(device_.get(), &buffer_create_info, nullptr, &buffer_);
        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not create the buffer\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    template <typename T, typename U, typename V>
    inline void BufferMemory<T, U, V>::setup_memory(memory_settings_t properties)
    {
        const auto requirements = get_memory_requirement();

        const auto memory_type_index = get_best_memory_type(requirements, properties);

        const auto allocation_info = VkMemoryAllocateInfo{
            .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
            .pNext = nullptr,
            .allocationSize = requirements.size,
            .memoryTypeIndex = memory_type_index,
        };

        const auto error = vkAllocateMemory(device_.get(), &allocation_info, nullptr, &memory_);
        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not allocate memory for vertex buffer\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    template <>
    inline void BufferMemory<VkBuffer, uint32_t, VkBufferUsageFlags>::bind_buffer_to_its_memory() const
    {
        const auto error = vkBindBufferMemory(device_.get(), buffer_, memory_, 0);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not bind buffer to its memory\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    template <>
    inline void BufferMemory<VkImage, VkExtent3D, ImageBufferBufferSettings>::bind_buffer_to_its_memory() const
    {
        const auto error = vkBindImageMemory(device_.get(), buffer_, memory_, 0);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not bind buffer to its memory\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    template <>
    inline void BufferMemory<VkBuffer, uint32_t, VkBufferUsageFlags>::cleanup_buffer()
    {
        vkDestroyBuffer(device_.get(), buffer_, nullptr);
    }

    template <>
    inline void BufferMemory<VkImage, VkExtent3D, ImageBufferBufferSettings>::cleanup_buffer()
    {
        vkDestroyImage(device_.get(), buffer_, nullptr);
    }

    template <typename T, typename U, typename V>
    void BufferMemory<T, U, V>::cleanup_memory()
    {
        vkFreeMemory(device_.get(), memory_, nullptr);
    }

    template <>
    inline typename BufferMemory<VkBuffer, uint32_t, VkBufferUsageFlags>::memory_requirements_t
    BufferMemory<VkBuffer, uint32_t, VkBufferUsageFlags>::get_memory_requirement() const
    {
        memory_requirements_t requirements;
        vkGetBufferMemoryRequirements(device_.get(), buffer_, &requirements);

        return requirements;
    }

    template <>
    inline typename BufferMemory<VkImage, VkExtent3D, ImageBufferBufferSettings>::memory_requirements_t
    BufferMemory<VkImage, VkExtent3D, ImageBufferBufferSettings>::get_memory_requirement() const
    {
        memory_requirements_t requirements;
        vkGetImageMemoryRequirements(device_.get(), buffer_, &requirements);

        return requirements;
    }

    template <typename T, typename U, typename V>
    inline uint32_t BufferMemory<T, U, V>::get_best_memory_type(const memory_requirements_t& requirements,
                                                                VkMemoryPropertyFlags properties) const
    {
        VkPhysicalDeviceMemoryProperties memory_properties;
        vkGetPhysicalDeviceMemoryProperties(device_.physical_device().get(), &memory_properties);

        for (uint32_t i = 0; i < memory_properties.memoryTypeCount; i++)
        {
            const auto current_properties_flags = memory_properties.memoryTypes[i].propertyFlags;

            const bool suitable_type = requirements.memoryTypeBits & (1 << i);
            const bool properties_matching = (current_properties_flags & properties) == properties;

            if (suitable_type && properties_matching)
                return i;
        }

        throw std::runtime_error("Unable to find suitable memory type for vertex buffer");
    }

    template <typename T, typename U, typename V>
    typename BufferMemory<T, U, V>::buffer_t& BufferMemory<T, U, V>::buffer()
    {
#ifndef NDEBUG
        if (!is_initialized_)
            std::cerr << "Getting  uninitialized buffer.\n";
#endif
        return buffer_;
    }

    template <typename T, typename U, typename V>
    const typename BufferMemory<T, U, V>::buffer_t& BufferMemory<T, U, V>::buffer() const
    {
#ifndef NDEBUG
        if (!is_initialized_)
            std::cerr << "Getting  uninitialized buffer.\n";
#endif
        return buffer_;
    }

    template <typename T, typename U, typename V>
    typename BufferMemory<T, U, V>::memory_t& BufferMemory<T, U, V>::memory()
    {
#ifndef NDEBUG
        if (!is_initialized_)
            std::cerr << "Getting  uninitialized memory.\n";
#endif
        return memory_;
    }

    template <typename T, typename U, typename V>
    const typename BufferMemory<T, U, V>::memory_t& BufferMemory<T, U, V>::memory() const
    {
#ifndef NDEBUG
        if (!is_initialized_)
            std::cerr << "Getting  uninitialized memory.\n";
#endif
        return memory_;
    }
} // namespace engine
