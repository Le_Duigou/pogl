#pragma once

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>

namespace engine
{
    using vec2_t = glm::vec2;
    using vec3_t = glm::vec3;
    using mat4_t = glm::mat4;
} // namespace engine
