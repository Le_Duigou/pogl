#pragma once

#include "engine/object/object.hh"
#include "engine/units/units.hh"

namespace engine
{
    class Camera : public Object
    {
    public:
        Camera(vec3_t location, vec3_t lookat, float fov, float aspect_ratio, float z_near, float z_far);

        void set_fov(float value);
        void set_aspect_ratio(float value);
        void set_z_near(float value);
        void set_z_far(float value);
        void set_location(vec3_t value);
        void set_lookat(vec3_t value);
        void set_location_and_lookat(vec3_t location, vec3_t lookat);

        float fov() const;
        float aspect_ratio() const;
        float z_near() const;
        float z_far() const;
        vec3_t location() const;
        vec3_t lookat() const;

        const mat4_t& projection_matrix() const;
        const mat4_t& transform() const;

    private:
        void compute_projection_matrix();
        void compute_tranform();

    private:
        float fov_;
        float aspect_ratio_;
        float z_near_;
        float z_far_;
        vec3_t location_;
        vec3_t lookat_;
        mat4_t projection_matrix_;
        mat4_t transform_;
    };
} // namespace engine
