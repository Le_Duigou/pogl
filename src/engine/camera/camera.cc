#include "camera.hh"

#include <glm/ext/matrix_clip_space.hpp>
#include <glm/ext/matrix_transform.hpp>

namespace engine
{
    Camera::Camera(vec3_t location, vec3_t lookat, float fov, float aspect_ratio, float z_near, float z_far)
        : Object()
        , fov_(fov)
        , aspect_ratio_(aspect_ratio)
        , z_near_(z_near)
        , z_far_(z_far)
        , location_(location)
        , lookat_(lookat)
    {
        compute_projection_matrix();
        compute_tranform();
    }

    void Camera::set_fov(float value)
    {
        fov_ = value;
        compute_projection_matrix();
    }

    void Camera::set_aspect_ratio(float value)

    {
        aspect_ratio_ = value;
        compute_projection_matrix();
    }

    void Camera::set_z_near(float value)

    {
        z_near_ = value;
        compute_projection_matrix();
    }

    void Camera::set_z_far(float value)

    {
        z_far_ = value;
        compute_projection_matrix();
    }
    void Camera::set_location(vec3_t value)
    {
        location_ = value;
        compute_tranform();
    }

    void Camera::set_lookat(vec3_t value)
    {
        lookat_ = value;
        compute_tranform();
    }

    void Camera::set_location_and_lookat(vec3_t location, vec3_t lookat)
    {
        location_ = location;
        lookat_ = lookat;
        compute_tranform();
    }

    float Camera::fov() const
    {
        return fov_;
    }

    float Camera::aspect_ratio() const
    {
        return aspect_ratio_;
    }

    float Camera::z_near() const
    {
        return z_near_;
    }

    float Camera::z_far() const
    {
        return z_far_;
    }

    vec3_t Camera::location() const
    {
        return location_;
    }

    vec3_t Camera::lookat() const
    {
        return lookat_;
    }

    const mat4_t& Camera::projection_matrix() const
    {
        return projection_matrix_;
    }

    const mat4_t& Camera::transform() const
    {
        return transform_;
    }

    void Camera::compute_projection_matrix()
    {
        const auto new_projection_matrix = glm::perspective(glm::radians(fov_), aspect_ratio_, z_near_, z_far_);

        if (new_projection_matrix != projection_matrix_)
        {
            projection_matrix_ = new_projection_matrix;
            set_dirty(true);
        }
    }

    void Camera::compute_tranform()
    {
        const auto new_transform_matrix = glm::lookAt(location(), lookat(), {0, 0, 1});

        if (new_transform_matrix != transform_)
        {
            transform_ = new_transform_matrix;
            set_dirty(true);
        }
    }

} // namespace engine
