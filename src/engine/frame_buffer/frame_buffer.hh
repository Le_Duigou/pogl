#pragma once

#include <memory>
#include <vector>
#include <vulkan/vulkan_core.h>

#include "engine/buffer_memory/fwd.hh"
#include "engine/swapchain/swapchain.hh"

namespace engine
{
    class RenderPass;
    class TextureImageView;

    class FrameBufferList
    {
    public:
        using buffer_t = VkFramebuffer;
        using inner_t = std::vector<buffer_t>;
        using logical_device_t = LogicalDevice;
        using swapchain_t = Swapchain;
        using render_pass_t = RenderPass;
        using depth_buffer_t = ImageBuffer;
        using depth_buffer_view_t = TextureImageView;

        FrameBufferList(swapchain_t& swapchain, render_pass_t& render_pass);
        ~FrameBufferList();

        inner_t& get();
        const inner_t& get() const;

    private:
        void setup(render_pass_t& render_pass);
        void cleanup();

        void setup_depth_buffer();
        void cleanup_depth_buffer();

        void setup_buffer(size_t index, render_pass_t& render_pass);
        void cleanup_buffer(buffer_t& target);

        logical_device_t& device();
        const logical_device_t& device() const;

    private:
        swapchain_t& swapchain_;
        inner_t buffer_list_;
        std::unique_ptr<depth_buffer_t> depth_buffer_;
        std::unique_ptr<depth_buffer_view_t> depth_buffer_view_;
    };
} // namespace engine
