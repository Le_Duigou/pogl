#include "frame_buffer.hh"

#include <iostream>
#include <memory>

#include "engine/buffer_memory/buffer_memory.hh"
#include "engine/error/vulkan_errors.hh"
#include "engine/image_view/texture_image_view.hh"
#include "engine/render_pass/render_pass.hh"

namespace engine
{
    FrameBufferList::FrameBufferList(swapchain_t& swapchain, render_pass_t& render_pass)
        : swapchain_(swapchain)
    {
        setup(render_pass);
    }

    FrameBufferList::~FrameBufferList()
    {
        cleanup();
    }

    void FrameBufferList::setup(render_pass_t& render_pass)
    {
        setup_depth_buffer();
        buffer_list_.resize(swapchain_.image_views().size());
        for (size_t i = 0; i < swapchain_.image_views().size(); i++)
        {
            setup_buffer(i, render_pass);
        }
    }

    void FrameBufferList::cleanup()
    {
        for (auto& buffer : buffer_list_)
        {
            cleanup_buffer(buffer);
        }
        buffer_list_.clear();
        cleanup_depth_buffer();
    }

    void FrameBufferList::setup_depth_buffer()
    {
        const auto extent = VkExtent3D{
            swapchain_.swap_extent().width,
            swapchain_.swap_extent().height,
            1,
        };
        const auto format = swapchain_.logical_device().get_depth_format();

        constexpr auto usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
        constexpr auto properties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
        constexpr auto aspect = VK_IMAGE_ASPECT_DEPTH_BIT;

        depth_buffer_ = std::make_unique<depth_buffer_t>(swapchain_.logical_device());
        depth_buffer_->initialize(extent, {usage, format}, properties);

        depth_buffer_view_ = std::make_unique<depth_buffer_view_t>(swapchain_.logical_device(), depth_buffer_->buffer(),
                                                                   format, 1, aspect);
    }

    void FrameBufferList::cleanup_depth_buffer()
    {
        depth_buffer_view_.reset();
        depth_buffer_.reset();
    }

    void FrameBufferList::setup_buffer(size_t index, render_pass_t& render_pass)
    {
        if (index >= buffer_list_.size())
            buffer_list_.resize(index + 1);

        const auto attachments = std::array<VkImageView, 2>{
            swapchain_.image_views().at(index)->get(),
            depth_buffer_view_->get(),
        };

        const auto frame_buffer_create_info = VkFramebufferCreateInfo{
            .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .renderPass = render_pass.get(),
            .attachmentCount = attachments.size(),
            .pAttachments = attachments.data(),
            .width = swapchain_.swap_extent().width,
            .height = swapchain_.swap_extent().height,
            .layers = 1,
        };

        const auto error =
            vkCreateFramebuffer(device().get(), &frame_buffer_create_info, nullptr, &buffer_list_[index]);
        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not create frame buffer number " << index << ".\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void FrameBufferList::cleanup_buffer(buffer_t& target)
    {
        vkDestroyFramebuffer(device().get(), target, nullptr);
    }

    FrameBufferList::inner_t& FrameBufferList::get()
    {
        return buffer_list_;
    }

    const FrameBufferList::inner_t& FrameBufferList::get() const
    {
        return buffer_list_;
    }

    FrameBufferList::logical_device_t& FrameBufferList::device()
    {
        return swapchain_.logical_device();
    }

    const FrameBufferList::logical_device_t& FrameBufferList::device() const
    {
        return swapchain_.logical_device();
    }

} // namespace engine
