#pragma once

#include <cstdint>
#include <vector>
#include <vulkan/vulkan_core.h>

#include "physical_device.hh"

namespace options
{
    struct Options;
}

namespace engine
{
    class WindowSurface;

    class LogicalDevice
    {
    public:
        using inner_t = VkDevice;
        using features_t = VkPhysicalDeviceFeatures;
        using queue_t = VkQueue;
        using physical_device_t = PhysicalDevice;

        LogicalDevice() = delete;
        LogicalDevice(const physical_device_t& physical_device, const options::Options& options);

        ~LogicalDevice();

        inner_t& get();
        const inner_t& get() const;

        queue_t& graphic_queue();
        const queue_t& graphic_queue() const;

        const physical_device_t& physical_device() const;

        VkFormat get_depth_format() const;

    private:
        void create(const PhysicalDevice& physical_device, const options::Options& options);
        static features_t get_features(const options::Options& options);

        std::vector<VkDeviceQueueCreateInfo> create_queues(const QueueFamilyIndices& indices);
        VkDeviceQueueCreateInfo create_queue(uint32_t familly_index);

        void get_queues(const QueueFamilyIndices& indices);
        void get_queue(uint32_t familly_index, queue_t* target);

    private:
        const physical_device_t& physical_device_;
        inner_t device_;
        queue_t graphic_queue_;
        queue_t present_queue_;
    };
} // namespace engine
