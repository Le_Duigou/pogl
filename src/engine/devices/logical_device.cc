#include "logical_device.hh"

#include <cstdint>
#include <set>
#include <stdexcept>
#include <vector>

#include "engine/error/vulkan_errors.hh"
#include "options/options.hh"
#include "queue_family.hh"
#include "required_extentions.hh"

namespace engine
{
    LogicalDevice::LogicalDevice(const physical_device_t& physical_device, const options::Options& options)
        : physical_device_(physical_device)
    {
        create(physical_device, options);
    }

    LogicalDevice::~LogicalDevice()
    {
        vkDestroyDevice(device_, nullptr);
    }

    void LogicalDevice::create(const PhysicalDevice& physical_device, const options::Options& options)
    {
        QueueFamilyIndices indices = physical_device.queue_family_indices();
        features_t features = get_features(options);
        const auto queues_info = create_queues(indices);

        VkDeviceCreateInfo create_device_info{
            .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .queueCreateInfoCount = static_cast<uint32_t>(queues_info.size()),
            .pQueueCreateInfos = queues_info.data(),
            .enabledLayerCount = 0,
            .ppEnabledLayerNames = nullptr,
            .enabledExtensionCount = REQUIRED_DEVICE_EXTENTION_COUNT,
            .ppEnabledExtensionNames = REQUIRED_DEVICE_EXTENTION.data(),
            .pEnabledFeatures = &features,
        };

        const auto error = vkCreateDevice(physical_device.get(), &create_device_info, nullptr, &device_);

        if (error != VK_SUCCESS)
        {
            throw std::runtime_error(vulkan_error_to_string(error));
        }

        get_queues(indices);
    }

    LogicalDevice::features_t LogicalDevice::get_features(const options::Options& options)
    {
        features_t features{};

        if (!options.no_multi_draw_indirect)
            features.multiDrawIndirect = VK_TRUE; // Enable drawing multiple object in a single draw call

        if (!options.no_anisotropic_filtering)
            features.samplerAnisotropy = VK_TRUE; // Enable anisotropic texture samplers

        return features;
    }

    std::vector<VkDeviceQueueCreateInfo> LogicalDevice::create_queues(const QueueFamilyIndices& indices)
    {
        const auto familly_indices = std::set<uint32_t>{
            indices.graphics_family.value(),
            indices.present_family.value(),
        };

        auto result = std::vector<VkDeviceQueueCreateInfo>{};
        result.reserve(familly_indices.size());

        for (const auto index : familly_indices)
        {
            result.emplace_back(create_queue(index));
        }
        return result;
    }

    VkDeviceQueueCreateInfo LogicalDevice::create_queue(uint32_t familly_index)
    {
        static const float queue_priorities = 1.f;
        return VkDeviceQueueCreateInfo{
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .queueFamilyIndex = familly_index,
            .queueCount = 1,
            .pQueuePriorities = &queue_priorities,
        };
    }

    void LogicalDevice::get_queues(const QueueFamilyIndices& indices)
    {
        get_queue(indices.graphics_family.value(), &graphic_queue_);
        get_queue(indices.present_family.value(), &present_queue_);
    }

    void LogicalDevice::get_queue(uint32_t familly_index, queue_t* target)
    {
        vkGetDeviceQueue(device_, familly_index, 0, target);
    }

    const LogicalDevice::physical_device_t& LogicalDevice::physical_device() const
    {
        return physical_device_;
    }

    LogicalDevice::inner_t& LogicalDevice::get()
    {
        return device_;
    }

    const LogicalDevice::inner_t& LogicalDevice::get() const
    {
        return device_;
    }

    LogicalDevice::queue_t& LogicalDevice::graphic_queue()
    {
        return graphic_queue_;
    }

    const LogicalDevice::queue_t& LogicalDevice::graphic_queue() const
    {
        return graphic_queue_;
    }

    VkFormat LogicalDevice::get_depth_format() const
    {
        std::vector<VkFormat> candidates = {VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT,
                                            VK_FORMAT_D24_UNORM_S8_UINT};
        VkFormatProperties properties;
        constexpr auto depth_feature = VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT;

        for (const auto& candidate : candidates)
        {
            vkGetPhysicalDeviceFormatProperties(physical_device().get(), candidate, &properties);
            if ((properties.optimalTilingFeatures & depth_feature) == depth_feature)
                return candidate;
        }

        throw std::runtime_error("No supported format for depth buffer");
    }

} // namespace engine
