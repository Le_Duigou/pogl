#pragma once

#include <array>
#include <cstdint>
#include <vulkan/vulkan_core.h>

namespace engine
{
    constexpr std::array<const char*, 1> REQUIRED_DEVICE_EXTENTION = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME,
    };
    constexpr uint32_t REQUIRED_DEVICE_EXTENTION_COUNT = static_cast<uint32_t>(REQUIRED_DEVICE_EXTENTION.size());
} // namespace engine
