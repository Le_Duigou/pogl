#pragma once

#include <vulkan/vulkan_core.h>

#include "engine/engine_instance/engine_instance.hh"
#include "queue_family.hh"

namespace engine
{
    class WindowSurface;
    class PhysicalDevice
    {
    public:
        using inner_t = VkPhysicalDevice;
        using instance_t = EngineInstance;
        using queue_family_indices_t = QueueFamilyIndices;
        using properties_t = VkPhysicalDeviceProperties;
        using features_t = VkPhysicalDeviceFeatures;

        PhysicalDevice() = delete;
        PhysicalDevice(instance_t& instance, const WindowSurface& surface);

        inner_t& get();
        const inner_t& get() const;

        const queue_family_indices_t& queue_family_indices() const;

        const properties_t& get_properties() const;
        const features_t& get_features() const;

    private:
        inner_t first_suitable_device(instance_t& instance, const WindowSurface& surface) const;
        bool is_device_suitable(inner_t& device, const WindowSurface& surface) const;
        bool does_device_support_extentions(inner_t& device) const;

        void compute_properties();
        void compute_features();

    private:
        inner_t device_;
        properties_t properties_;
        features_t features_;
        queue_family_indices_t queue_family_indices_;
    };
} // namespace engine
