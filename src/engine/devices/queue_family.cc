#include "queue_family.hh"

#include <stdexcept>
#include <vector>

#include "engine/window_surface/window_surface.hh"
#include "physical_device.hh"

namespace engine
{
    QueueFamilyIndices QueueFamilyIndices::queue_of(const VkPhysicalDevice& device, const WindowSurface& surface)
    {
        uint32_t count = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(device, &count, nullptr);

        std::vector<VkQueueFamilyProperties> queue_families(count);
        vkGetPhysicalDeviceQueueFamilyProperties(device, &count, queue_families.data());

        QueueFamilyIndices result{};
        for (uint32_t i = 0; i < count; i++)
        {
            if (queue_families[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
                result.graphics_family = i;

            VkBool32 supports_present = false;
            vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface.surface(), &supports_present);

            if (supports_present)
                result.present_family = i;

            if (result.is_complete())
                return result;
        }

        return result;
    }

    bool QueueFamilyIndices::is_complete() const
    {
        return graphics_family.has_value() && present_family.has_value();
    }

    std::array<uint32_t, 2> QueueFamilyIndices::as_array() const
    {
        if (!is_complete())
            throw std::runtime_error("Export queue family indices as array but "
                                     "structure is not complete");

        return {graphics_family.value(), graphics_family.value()};
    }

} // namespace engine
