#pragma once

#include <array>
#include <cstdint>
#include <optional>
#include <vulkan/vulkan_core.h>

namespace engine
{
    class PhysicalDevice;
    class WindowSurface;

    struct QueueFamilyIndices
    {
        std::optional<uint32_t> graphics_family;
        std::optional<uint32_t> present_family;

        bool is_complete() const;

        std::array<uint32_t, 2> as_array() const;

        static QueueFamilyIndices queue_of(const VkPhysicalDevice& device, const WindowSurface& surface);
    };

} // namespace engine
