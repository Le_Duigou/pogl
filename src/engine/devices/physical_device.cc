#include "physical_device.hh"

#include <array>
#include <iostream>
#include <set>
#include <stdexcept>
#include <vector>

#include "queue_family.hh"
#include "required_extentions.hh"

namespace engine
{
    PhysicalDevice::PhysicalDevice(instance_t& instance, const WindowSurface& surface)
        : device_(first_suitable_device(instance, surface))
        , queue_family_indices_(queue_family_indices_t::queue_of(device_, surface))
    {
        compute_properties();
        compute_features();
    }

    PhysicalDevice::inner_t PhysicalDevice::first_suitable_device(instance_t& instance,
                                                                  const WindowSurface& surface) const
    {
        uint32_t device_count = 0;
        vkEnumeratePhysicalDevices(instance.get(), &device_count, nullptr);

        if (device_count == 0)
            throw std::runtime_error("No GPU with vulkan support");

        std::vector<inner_t> devices(device_count);
        vkEnumeratePhysicalDevices(instance.get(), &device_count, devices.data());

        for (auto& device : devices)
        {
            if (is_device_suitable(device, surface))
                return device;
        }

        throw std::runtime_error("No GPU with required properties");
    }

    bool PhysicalDevice::is_device_suitable(inner_t& device, const WindowSurface& surface) const
    {
        QueueFamilyIndices queue_indices = QueueFamilyIndices::queue_of(device, surface);

        return queue_indices.is_complete() && does_device_support_extentions(device);
    }

    bool PhysicalDevice::does_device_support_extentions(inner_t& device) const
    {
        uint32_t count;
        vkEnumerateDeviceExtensionProperties(device, nullptr, &count, nullptr);

        std::vector<VkExtensionProperties> availableExtensions(count);
        vkEnumerateDeviceExtensionProperties(device, nullptr, &count, availableExtensions.data());

        std::set<std::string> missing_extentions(REQUIRED_DEVICE_EXTENTION.begin(), REQUIRED_DEVICE_EXTENTION.end());

        for (const auto& extension : availableExtensions)
        {
            missing_extentions.erase(extension.extensionName);
        }

        return missing_extentions.empty();
    }

    void PhysicalDevice::compute_properties()
    {
        vkGetPhysicalDeviceProperties(device_, &properties_);
    }

    void PhysicalDevice::compute_features()
    {
        vkGetPhysicalDeviceFeatures(device_, &features_);
    }

    PhysicalDevice::inner_t& PhysicalDevice::get()
    {
        return device_;
    }

    const PhysicalDevice::inner_t& PhysicalDevice::get() const
    {
        return device_;
    }

    const PhysicalDevice::queue_family_indices_t& PhysicalDevice::queue_family_indices() const
    {
        return queue_family_indices_;
    }

    const PhysicalDevice::properties_t& PhysicalDevice::get_properties() const
    {
        return properties_;
    }

    const PhysicalDevice::features_t& PhysicalDevice::get_features() const
    {
        return features_;
    }

} // namespace engine
