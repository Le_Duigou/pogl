#include "shader.hh"

#include <iostream>

#include "engine/error/vulkan_errors.hh"
#include "engine/file_reader/file_reader.hh"

namespace engine
{
    Shader::Shader(const std::string& file_path, const logical_device_t& device)
        : path_(file_path)
        , device_(device)
        , shader_()
    {
        load_shader();
    }

    Shader::~Shader()
    {
        vkDestroyShaderModule(device_.get(), shader_, nullptr);
    }

    void Shader::load_shader()
    {
        const auto code = FileRader(path_).read_bytes();

        const auto shader_create_info = VkShaderModuleCreateInfo{
            .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .codeSize = code.size(),
            .pCode = reinterpret_cast<const uint32_t*>(code.data()),
        };

        const auto error = vkCreateShaderModule(device_.get(), &shader_create_info, nullptr, &shader_);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not create shader module " + path_ + "\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    Shader::inner_t& Shader::get()
    {
        return shader_;
    }

    const Shader::inner_t& Shader::get() const
    {
        return shader_;
    }
} // namespace engine
