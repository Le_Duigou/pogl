#pragma once

#include <string>
#include <vulkan/vulkan_core.h>

#include "engine/devices/logical_device.hh"
namespace engine
{
    class Shader
    {
    public:
        using inner_t = VkShaderModule;
        using logical_device_t = LogicalDevice;

        Shader(const std::string& file_path, const logical_device_t& device);
        ~Shader();

        inner_t& get();
        const inner_t& get() const;

    private:
        void load_shader();

    private:
        std::string path_;
        const logical_device_t& device_;
        inner_t shader_;
    };

} // namespace engine
