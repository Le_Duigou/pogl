#include "viewport.hh"

namespace engine
{
    Viewport::Viewport(uint32_t width, uint32_t height)
    {
        viewport_ = viewport_t{
            .x = 0.f,
            .y = 0.f,
            .width = static_cast<float>(width),
            .height = static_cast<float>(height),
            .minDepth = 0.f,
            .maxDepth = 1.f,
        };

        scissor_ = scissor_t{
            .offset = {0, 0},
            .extent = {width, height},
        };
    }

    Viewport::~Viewport()
    {}

    Viewport::viewport_t& Viewport::viewport()
    {
        return viewport_;
    }

    const Viewport::viewport_t& Viewport::viewport() const
    {
        return viewport_;
    }

    Viewport::scissor_t& Viewport::scissor()
    {
        return scissor_;
    }

    const Viewport::scissor_t& Viewport::scissor() const
    {
        return scissor_;
    }

    float Viewport::x() const
    {
        return viewport_.x;
    }

    float Viewport::y() const
    {
        return viewport_.y;
    }

    float Viewport::width() const
    {
        return viewport_.width;
    }

    float Viewport::height() const
    {
        return viewport_.height;
    }

    float Viewport::min_depth() const
    {
        return viewport_.minDepth;
    }

    float Viewport::max_depth() const
    {
        return viewport_.maxDepth;
    }

} // namespace engine
