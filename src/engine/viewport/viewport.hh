#pragma once

#include <vulkan/vulkan_core.h>

namespace engine
{
    class Viewport
    {
    public:
        using viewport_t = VkViewport;
        using scissor_t = VkRect2D;

        Viewport(uint32_t width, uint32_t height);
        ~Viewport();

        viewport_t& viewport();
        const viewport_t& viewport() const;

        scissor_t& scissor();
        const scissor_t& scissor() const;

        float x() const;
        float y() const;
        float width() const;
        float height() const;
        float min_depth() const;
        float max_depth() const;

    private:
        viewport_t viewport_;
        scissor_t scissor_;
    };
} // namespace engine
