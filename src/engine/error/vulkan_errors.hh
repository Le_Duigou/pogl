#pragma once

#include <string>
#include <vulkan/vulkan_core.h>

namespace engine
{
    std::string vulkan_error_to_string(VkResult result);
}
