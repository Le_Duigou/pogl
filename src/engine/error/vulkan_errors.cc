#include "vulkan_errors.hh"

namespace engine
{
    std::string vulkan_error_to_string(VkResult result)
    {
        switch (result)
        {
        case VK_SUCCESS:
            return "Success";
        case VK_NOT_READY:
            return "Not ready";
        case VK_TIMEOUT:
            return "Timeout";
        case VK_ERROR_OUT_OF_HOST_MEMORY:
            return "Out of memory on host";
        case VK_ERROR_OUT_OF_DEVICE_MEMORY:
            return "Out of memory on device";
        case VK_ERROR_INITIALIZATION_FAILED:
            return "Initialization failed";
        case VK_ERROR_LAYER_NOT_PRESENT:
            return "The required layer is not present";
        case VK_ERROR_EXTENSION_NOT_PRESENT:
            return "The required extension is not present";
        case VK_ERROR_FEATURE_NOT_PRESENT:
            return "One of the device feature is no available on device. It may be possible to disable it with a "
                   "command line argument.";
        case VK_ERROR_INCOMPATIBLE_DRIVER:
            return "The driver is incompatible";
        case VK_ERROR_DEVICE_LOST:
            return "Device lost";
        case VK_ERROR_TOO_MANY_OBJECTS:
            return "Too many objects";
        case VK_ERROR_INVALID_SHADER_NV:
            return "The shader bytecode is invalid";
        default:
            return "An error occurred";
        }
    }
} // namespace engine
