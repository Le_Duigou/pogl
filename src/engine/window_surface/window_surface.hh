#pragma once
#include <memory>
#include <vulkan/vulkan_core.h>

#include "engine/devices/logical_device.hh"
#include "engine/devices/physical_device.hh"
#include "engine/engine_instance/engine_instance.hh"
#include "engine/swapchain/swapchain.hh"

namespace engine
{
    class Window;

    class WindowSurface
    {
    public:
        using surface_t = VkSurfaceKHR;
        using instance_t = EngineInstance;
        using window_t = Window;

        WindowSurface(window_t& window, instance_t& instance);
        ~WindowSurface();

        surface_t& surface();
        const surface_t& surface() const;

        window_t& window();
        const window_t& window() const;

        unsigned int width() const;
        unsigned int height() const;

    private:
        void setup();
        void cleanup();

    private:
        window_t& window_;
        instance_t& instance_;
        surface_t surface_;
    };
} // namespace engine
