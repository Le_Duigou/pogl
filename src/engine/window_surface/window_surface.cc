#include "window_surface.hh"

#include "engine/devices/physical_device.hh"
#include "engine/error/vulkan_errors.hh"

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "engine/window/window.hh"

namespace engine
{
    WindowSurface::WindowSurface(window_t& window, instance_t& instance)
        : window_(window)
        , instance_(instance)
    {
        setup();
    }

    WindowSurface::~WindowSurface()
    {
        cleanup();
    }

    WindowSurface::surface_t& WindowSurface::surface()
    {
        return surface_;
    }

    const WindowSurface::surface_t& WindowSurface::surface() const
    {
        return surface_;
    }

    void WindowSurface::setup()
    {
        const auto error = glfwCreateWindowSurface(instance_.get(), &window_.get(), nullptr, &surface_);

        if (error != VK_SUCCESS)
        {
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void WindowSurface::cleanup()
    {
        vkDestroySurfaceKHR(instance_.get(), surface_, nullptr);
    }

    unsigned int WindowSurface::width() const
    {
        return window_.width();
    }

    unsigned int WindowSurface::height() const
    {
        return window_.height();
    }

    WindowSurface::window_t& WindowSurface::window()
    {
        return window_;
    }

    const WindowSurface::window_t& WindowSurface::window() const
    {
        return window_;
    }

} // namespace engine
