#pragma once

#include <cstddef>
#include <string>
#include <vector>

#include "engine/object/object.hh"

namespace engine
{
    class Vertex;
    class MeshManager;

    class StaticMesh : public Object
    {
    public:
        using vertex_t = Vertex;
        using index_t = uint32_t;
        using vertices_t = std::vector<vertex_t>;
        using indices_t = std::vector<index_t>;

        StaticMesh(MeshManager& manager, const std::string& path);
        StaticMesh(MeshManager& manager, const vertices_t& vertices, const indices_t& indices);

    private:
        MeshManager& manager_;
    };
} // namespace engine
