#include "mesh_manager.hh"

#include <stdexcept>
#include <vulkan/vulkan_core.h>

namespace engine
{
    MeshManager::MeshManager()
        : is_dirty_(false)
    {}

    MeshManager::~MeshManager()
    {}

    const MeshManager::vertices_t& MeshManager::get_all_vertices()
    {
        return vertices_;
    }

    const MeshManager::indices_t& MeshManager::get_all_indices()
    {
        return indices_;
    }

    const MeshManager::commands_t& MeshManager::get_all_commands()
    {
        return commands_;
    }

    void MeshManager::add_mesh(const Object& parent, const vertices_t& vertices, const indices_t& indices)
    {
#ifndef NDEBUG
        if (mesh_map_.contains(&parent))
        {
            throw std::invalid_argument("Attempting to insert a mesh with a parent object that has already been used");
        }
#endif

        auto metadata = MeshMetadata{
            .vertex_offset = vertices_.size(),
            .vertex_count = vertices.size(),
            .index_offset = indices_.size(),
            .index_count = indices.size(),
            .command = nullptr,
        };

        vertices_.insert(vertices_.end(), vertices.begin(), vertices.end());
        indices_.insert(indices_.end(), indices.begin(), indices.end());
        commands_.emplace_back(command_t{
            .indexCount = static_cast<uint32_t>(metadata.index_count),
            .instanceCount = 1,
            .firstIndex = static_cast<uint32_t>(metadata.index_offset),
            .vertexOffset = static_cast<int32_t>(metadata.vertex_offset),
            .firstInstance = 0,
        });
        metadata.command = &commands_.back();

        mesh_map_.emplace(&parent, metadata);

        set_dirty(true);
    }

    bool MeshManager::is_dirty() const
    {
        return is_dirty_;
    }

    void MeshManager::set_dirty(bool value)
    {
        is_dirty_ = value;
    }

} // namespace engine
