#include "obj_parser.hh"

#include <cmath>
#include <cstddef>
#include <exception>
#include <glm/ext/scalar_constants.hpp>
#include <glm/geometric.hpp>
#include <glm/gtc/epsilon.hpp>
#include <glm/gtx/dual_quaternion.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <iostream>
#include <optional>
#include <stdexcept>
#include <utility>
#include <vector>

#include "engine/units/units.hh"

namespace engine
{
    ObjParser::ObjParser(std::istream& in)
        : lexer_(in)

    {}

    void ObjParser::operator()()
    {
        while (lexer_.next_line())
        {
            parse_line();
        }

        compute_tangents();

#ifndef NDEBUG
        std::cout << "Object loaded. Total number of vertices: " << vertices().size() << "\n";
#endif
    }

    void ObjParser::compute_tangents()
    {
        for (size_t i = 0; i < indices_.size(); i += 3)
        {
            compute_tangent(i, i + 1, i + 2);
        }

        for (auto& vertice : vertices_)
        {
            vertice.normal = glm::normalize(vertice.normal);
            vertice.tangent = glm::normalize(tangents_[vertice.location]);
            vertice.tangent =
                glm::normalize(vertice.tangent - vertice.normal * glm::dot(vertice.normal, vertice.tangent));
        }
    }

    void ObjParser::compute_tangent(size_t i1, size_t i2, size_t i3)
    {
        auto& v1 = vertices_[indices_[i1]];
        auto& v2 = vertices_[indices_[i2]];
        auto& v3 = vertices_[indices_[i3]];

        const vec3_t edge1 = v2.location - v1.location;
        const vec3_t edge2 = v3.location - v1.location;
        const vec2_t delta_UV1 = (v2.uv - v1.uv);
        const vec2_t delta_UV2 = (v3.uv - v1.uv);

        float denom = (delta_UV1.x * delta_UV2.y - delta_UV2.x * delta_UV1.y);

        float f = 1.f / denom;
        const auto tangent = glm::normalize(vec3_t{
            f * (delta_UV2.y * edge1.x - delta_UV1.y * edge2.x),
            f * (delta_UV2.y * edge1.y - delta_UV1.y * edge2.y),
            f * (delta_UV2.y * edge1.z - delta_UV1.y * edge2.z),
        });

        tangents_[v1.location] += tangent;
        tangents_[v2.location] += tangent;
        tangents_[v3.location] += tangent;
    }

    const std::vector<Vertex>& ObjParser::vertices() const
    {
        return vertices_;
    }

    const std::vector<ObjParser::mesh_index_t>& ObjParser::indices() const
    {
        return indices_;
    }

    void ObjParser::error(const std::string& message) const
    {
        throw std::runtime_error("[" + std::to_string(lexer_.row()) + " - " + std::to_string(lexer_.column()) + "] "
                                 + message);
    }

    void ObjParser::parse_line()
    {
        switch (lexer_.line_type())
        {
        case ObjLexer::DeclarationType::Empty:
        case ObjLexer::DeclarationType::Unsupported:
            break;

        case ObjLexer::DeclarationType::VertexDeclaration:
            parse_vertex();
            break;

        case ObjLexer::DeclarationType::VertexTextureDeclaration:
            parse_texture_coordinate();
            break;

        case ObjLexer::DeclarationType::VertexNormalDeclaration:
            parse_normal();
            break;

        case ObjLexer::DeclarationType::FaceDeclaration:
            parse_face();
            break;

        case ObjLexer::DeclarationType::Unknown:
            error("Unexpected symbol: " + (*lexer_.as_string()));
            break;
        }
    }

    void ObjParser::parse_vertex()
    {
        const auto x = parse_coordinate();
        const auto y = parse_coordinate();
        const auto z = parse_coordinate();

        locations_.push_back(transform_location(vec3_t{x, y, z}));
    }

    void ObjParser::parse_normal()
    {
        const auto x = parse_coordinate();
        const auto y = parse_coordinate();
        const auto z = parse_coordinate();

        normals_.push_back(transform_normal(vec3_t{x, y, z}));
    }

    void ObjParser::parse_texture_coordinate()
    {
        const auto u = parse_coordinate();
        const auto v = parse_coordinate();

        uvs_.push_back(vec2_t{u, 1 - v});
    }

    void ObjParser::parse_face()
    {
        std::vector<obj_index_t> face_vertices = build_vertex_list_of_face();
        build_faces_from_vertex_list(face_vertices);
    }

    std::vector<ObjParser::obj_index_t> ObjParser::build_vertex_list_of_face()
    {
        std::vector<obj_index_t> face_vertices;
        std::optional<ObjParser::obj_index_triple_t> point;

        while ((point = parse_vertex_index()))
        {
            face_vertices.push_back(get_or_push_vertex_in_mesh(*point));
        }

        return face_vertices;
    }

    void ObjParser::build_faces_from_vertex_list(const std::vector<ObjParser::obj_index_t>& indices)
    {
        if (indices.size() < 3)
            return;
        for (size_t i = 2; i < indices.size(); i++)
        {
            indices_.push_back(indices[0]);
            indices_.push_back(indices[i - 1]);
            indices_.push_back(indices[i]);
        }
    }

    float ObjParser::parse_coordinate()
    {
        std::optional<float> result;
        if (!lexer_.next() || !(result = lexer_.as_float()))
            error("Expected coordinate");

        return *result;
    }

    std::optional<ObjParser::obj_index_triple_t> ObjParser::parse_vertex_index()
    {
        if (!lexer_.next())
            return std::nullopt;

        const auto triple = lexer_.as_int_triple();
        if (triple)
        {
            return {{static_cast<obj_index_t>((*triple)[0]), static_cast<obj_index_t>((*triple)[1]),
                     static_cast<obj_index_t>((*triple)[2])}};
        }

        if (lexer_.as_disconnected_int_triple() || lexer_.as_int_pair() || lexer_.as_int())
            error("Only v/vt/vn triple are supported currently");

        error("Expected vertex index");
        return std::nullopt;
    }

    ObjParser::obj_index_t ObjParser::get_or_push_vertex_in_mesh(const obj_index_triple_t& indices)
    {
        const auto location_index = get_index_within_limit(indices[0], locations_.size());
        const auto texture_index = get_index_within_limit(indices[1], uvs_.size());
        const auto normal_index = get_index_within_limit(indices[2], normals_.size());

        const auto location = locations_[location_index];
        const auto normal = normals_[normal_index];
        const auto uv = uvs_[texture_index];

        const auto as_vertex = Vertex{
            location,
            normal,
            vec3_t{0.0, 0.0, 0.0}, // Will be computed afterwise once all faces are built
            uv,
        };

        tangents_.emplace(location, vec3_t{0.f, 0.f, 0.f});

        const auto index = vertex_map_.find(as_vertex);

        if (index != vertex_map_.end())
            return index->second;

        vertices_.push_back(as_vertex);
        const obj_index_t inserted_value = vertices_.size() - 1;

        vertex_map_.insert(std::make_pair(as_vertex, inserted_value));

        return inserted_value;
    }

    ObjParser::mesh_index_t ObjParser::get_index_within_limit(ObjParser::obj_index_t index, size_t vector_size)
    {
        if (index == 0)
            error("Indexes start at 1");

        if (index < 0)
        {
            const size_t neg_index = -index;

            if (neg_index > vector_size)
                error("Out of bound negative index");

            return vector_size - neg_index;
        }
        else
        {
            const size_t pos_index = index;

            if (pos_index > vector_size)
                error("Out of bound positive index");

            return pos_index - 1;
        }
    }

    vec3_t ObjParser::transform_location(vec3_t location)
    {
        constexpr float angle = glm::radians(-90.f);
        return glm::rotate(location, angle, {1, 0, 0});
    }

    vec3_t ObjParser::transform_normal(vec3_t normal)
    {
        constexpr float angle = glm::radians(-90.f);
        return glm::rotate(normal, angle, {1, 0, 0});
        return glm::normalize(normal);
    }

} // namespace engine
