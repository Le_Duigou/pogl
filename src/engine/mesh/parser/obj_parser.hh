#pragma once

#include <array>
#include <cstddef>
#include <cstdint>
#include <unordered_map>
#include <vector>

#include "engine/units/units.hh"
#include "engine/vertex/vertex.hh"
#include "obj_lexer.hh"

namespace engine
{
    class ObjParser
    {
    public:
        using obj_index_t = int;
        using obj_index_triple_t = std::array<obj_index_t, 3>;
        using mesh_index_t = std::uint32_t;

        ObjParser(std::istream& in);
        void operator()();

        const std::vector<Vertex>& vertices() const;
        const std::vector<mesh_index_t>& indices() const;

    private:
        void error(const std::string& message) const;

        void parse_line();

        void parse_vertex();
        void parse_normal();
        void parse_texture_coordinate();
        void parse_face();

        float parse_coordinate();
        std::optional<obj_index_triple_t> parse_vertex_index();

        std::vector<obj_index_t> build_vertex_list_of_face();
        void build_faces_from_vertex_list(const std::vector<obj_index_t>& indices);

        obj_index_t get_or_push_vertex_in_mesh(const obj_index_triple_t& indices);

        mesh_index_t get_index_within_limit(obj_index_t index, size_t vector_size);

        vec3_t transform_location(vec3_t location);
        vec3_t transform_normal(vec3_t normal);

        void compute_tangents();
        void compute_tangent(size_t i1, size_t i2, size_t i3);

    private:
        ObjLexer lexer_;

        std::vector<vec3_t> locations_;
        std::vector<vec3_t> normals_;
        std::vector<vec2_t> uvs_;
        std::unordered_map<vec3_t, vec3_t> tangents_; // Todo: use both normal and location for indexing

        std::unordered_map<Vertex, size_t> vertex_map_;

        std::vector<Vertex> vertices_;
        std::vector<mesh_index_t> indices_;
    };
} // namespace engine
