#include "obj_lexer.hh"

#include <exception>
#include <iostream>
#include <optional>
#include <regex>
#include <sstream>
#include <stdexcept>
#include <string>
#include <utility>

namespace engine
{
    std::string trim_and_remove_comment(const std::string& line);
    int string_to_int(const std::string& str);
    float string_to_float(const std::string& str);
    std::vector<std::string> split(const std::string& source, const std::string& delim);

    ObjLexer::ObjLexer(std::istream& in)
        : in_(in)
        , current_line_type_(DeclarationType::Unknown)
        , column_(0)
        , row_(0)
    {}

    int ObjLexer::column() const
    {
        return column_;
    }

    int ObjLexer::row() const
    {
        return row_;
    }

    bool ObjLexer::next_line()
    {
        std::string line;
        if (!std::getline(in_, line))
            return false;
        row_++;

        current_line_ = std::stringstream(trim_and_remove_comment(line));

        std::string identifier;
        current_line_ >> identifier;

        if (identifier.empty())
            current_line_type_ = DeclarationType::Empty;
        else if (identifier == "v")
            current_line_type_ = DeclarationType::VertexDeclaration;
        else if (identifier == "vt")
            current_line_type_ = DeclarationType::VertexTextureDeclaration;
        else if (identifier == "vn")
            current_line_type_ = DeclarationType::VertexNormalDeclaration;
        else if (identifier == "f")
            current_line_type_ = DeclarationType::FaceDeclaration;
        else if (identifier == "vp" || identifier == "l" || identifier == "mtllib" || identifier == "usemtl"
                 || identifier == "s" || identifier == "o" || identifier == "g")
            current_line_type_ = DeclarationType::Unsupported;
        else
            current_line_type_ = DeclarationType::Unknown;

        return true;
    }

    bool ObjLexer::next()
    {
        if (!current_line_ || current_line_.eof())
            return false;

        current_line_ >> current_word_;

        return !current_word_.empty();
    }

    std::optional<std::string> ObjLexer::as_string() const
    {
        return current_word_.empty() ? std::nullopt : std::make_optional(current_word_);
    }

    std::optional<float> ObjLexer::as_float() const
    {
        try
        {
            return string_to_float(current_word_);
        }
        catch (const std::exception&)
        {
            return std::nullopt;
        }
    }

    std::optional<int> ObjLexer::as_int() const
    {
        try
        {
            return string_to_int(current_word_);
        }
        catch (const std::exception&)
        {
            return std::nullopt;
        }
    }

    std::optional<std::pair<int, int>> ObjLexer::as_int_pair() const
    {
        try
        {
            const auto match = split(current_word_, "/");

            if (match.size() != 2)
                return std::nullopt;

            return std::make_pair(string_to_int(match.at(0)), string_to_int(match.at(1)));
        }
        catch (const std::exception&)
        {
            return std::nullopt;
        }
    }

    std::optional<std::pair<int, int>> ObjLexer::as_disconnected_int_triple() const
    {
        try
        {
            const auto match = split(current_word_, "//");

            if (match.size() != 2)
                return std::nullopt;

            return std::make_pair(string_to_int(match[0]), string_to_int(match[1]));
        }
        catch (const std::exception&)
        {
            return std::nullopt;
        }
    }

    std::optional<std::array<int, 3>> ObjLexer::as_int_triple() const
    {
        try
        {
            const auto match = split(current_word_, "/");

            if (match.size() != 3)
            {
                std::cout << "Match nb " << match.size() << "\n";
                std::cout << current_word_ << "\n";
                return std::nullopt;
            }

            return std::make_optional(
                std::array{string_to_int(match[0]), string_to_int(match[1]), string_to_int(match[2])});
        }
        catch (const std::exception&)
        {
            return std::nullopt;
        }
    }

    ObjLexer::DeclarationType ObjLexer::line_type() const
    {
        return current_line_type_;
    }

    int string_to_int(const std::string& str)
    {
        if (!std::regex_search(str, std::regex("^ *-?[0-9]+ *$")))
            throw std::runtime_error("Invalid integer");

        return stoi(str);
    }

    float string_to_float(const std::string& str)
    {
        if (!std::regex_search(str, std::regex("^ *-?[0-9]+(\\.[0-9]*)? *$")))
            throw std::runtime_error("Invalid float");

        return stof(str);
    }

    std::string trim_and_remove_comment(const std::string& line)
    {
        const auto delimiter = line.find_first_of('#');

        auto result = (delimiter == std::string::npos) ? std::string(line) : line.substr(0, delimiter);

        result.erase(
            std::find_if(result.rbegin(), result.rend(), [](unsigned char current) { return !std::isspace(current); })
                .base(),
            result.end());

        return result;
    }

    std::vector<std::string> split(const std::string& source, const std::string& delim)
    {
        std::size_t delim_length = delim.size();
        std::size_t current, previous = 0;
        std::vector<std::string> result;

        current = source.find(delim);
        while (current != std::string::npos)
        {
            result.emplace_back(source.substr(previous, current - previous));
            previous = current + delim_length;
            current = source.find(delim, previous);
        }

        result.emplace_back(source.substr(previous, current - previous));

        return result;
    }

} // namespace engine
