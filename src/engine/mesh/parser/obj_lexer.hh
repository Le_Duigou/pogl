#pragma once

#include <array>
#include <istream>
#include <optional>
#include <sstream>
#include <utility>

namespace engine
{
    class ObjLexer
    {
    public:
        ObjLexer(std::istream& in);

        enum class DeclarationType
        {
            Unknown,

            Unsupported,

            Empty,

            VertexDeclaration,
            VertexTextureDeclaration,
            VertexNormalDeclaration,
            FaceDeclaration,
        };

        int column() const;
        int row() const;

        bool next_line();
        bool next();

        std::optional<float> as_float() const;
        std::optional<int> as_int() const;
        std::optional<std::string> as_string() const;
        std::optional<std::pair<int, int>> as_int_pair() const;
        std::optional<std::pair<int, int>> as_disconnected_int_triple() const;
        std::optional<std::array<int, 3>> as_int_triple() const;

        DeclarationType line_type() const;

    private:
        std::istream& in_;
        std::stringstream current_line_;
        DeclarationType current_line_type_;
        std::string current_word_;

        int column_;
        int row_;
    };
} // namespace engine
