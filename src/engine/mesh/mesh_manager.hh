#pragma once

#include <cstddef>
#include <cstdint>
#include <unordered_map>
#include <vector>

#include "engine/vertex/vertex.hh"

struct VkDrawIndexedIndirectCommand;

namespace engine
{
    class Object;

    class MeshManager
    {
    public:
        using vertex_t = Vertex;
        using index_t = uint32_t;
        using command_t = VkDrawIndexedIndirectCommand;
        using vertices_t = std::vector<vertex_t>;
        using indices_t = std::vector<index_t>;
        using commands_t = std::vector<command_t>;

        MeshManager();
        ~MeshManager();

        const vertices_t& get_all_vertices();
        const indices_t& get_all_indices();
        const commands_t& get_all_commands();

        void add_mesh(const Object& parent, const vertices_t& vertices, const indices_t& indices);

        bool is_dirty() const;
        void set_dirty(bool value);

    private:
        struct MeshMetadata
        {
            size_t vertex_offset;
            size_t vertex_count;
            size_t index_offset;
            size_t index_count;
            command_t* command;
        };

        using mesh_map_t = std::unordered_map<Object const*, MeshMetadata>;

        vertices_t vertices_;
        indices_t indices_;
        commands_t commands_;
        mesh_map_t mesh_map_;
        bool is_dirty_;
    };
} // namespace engine
