#include "static_mesh.hh"

#include "mesh_manager.hh"
#include "parser/obj_parser.hh"

namespace engine
{
    StaticMesh::StaticMesh(MeshManager& manager, const std::string& path)
        : manager_(manager)
    {
        if (path.empty())
            throw std::invalid_argument("Empty path provided to load function");

        constexpr auto flags = std::fstream::in;
        auto file = std::fstream(path, flags);

        if (!file)
            throw std::invalid_argument("File " + path + " not found");

        auto parser = ObjParser(file);

        parser();

        manager_.add_mesh(*this, parser.vertices(), parser.indices());
    }

    StaticMesh::StaticMesh(MeshManager& manager, const vertices_t& vertices, const indices_t& indices)
        : manager_(manager)
    {
        manager_.add_mesh(*this, vertices, indices);
    }
} // namespace engine
