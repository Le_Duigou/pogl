#pragma once

#include <cstdint>
#include <vector>
#include <vulkan/vulkan_core.h>

#include "engine/devices/logical_device.hh"

namespace engine
{
    class TextureImageView
    {
    public:
        using inner_t = VkImageView;
        using image_t = VkImage;
        using logical_device_t = LogicalDevice;
        using format_t = VkFormat;
        using aspect_t = VkImageAspectFlags;

        static constexpr aspect_t DEFAULT_ASPECT = VK_IMAGE_ASPECT_COLOR_BIT;

        TextureImageView(const logical_device_t& device, const VkImage& image, const format_t& format,
                         uint32_t mipmap_level, aspect_t aspect = DEFAULT_ASPECT);
        ~TextureImageView();

        inner_t& get();
        const inner_t& get() const;

    private:
        void setup(const format_t& format, uint32_t mipmap_level, aspect_t aspect);
        void cleanup();

    private:
        const logical_device_t& device_;
        const image_t& image_;
        inner_t image_view_;
    };
} // namespace engine
