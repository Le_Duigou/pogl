#include "texture_image_view.hh"

#include <iostream>

#include "engine/error/vulkan_errors.hh"

namespace engine
{
    TextureImageView::TextureImageView(const logical_device_t& device, const VkImage& image, const format_t& format,
                                       uint32_t mipmap_level, aspect_t aspect)
        : device_(device)
        , image_(image)
    {
        setup(format, mipmap_level, aspect);
    }

    TextureImageView::~TextureImageView()
    {
        cleanup();
    }

    TextureImageView::inner_t& TextureImageView::get()
    {
        return image_view_;
    }

    const TextureImageView::inner_t& TextureImageView::get() const
    {
        return image_view_;
    }

    void TextureImageView::setup(const format_t& format, uint32_t mipmap_level, aspect_t aspect)
    {
        const auto components = VkComponentMapping{
            .r = VK_COMPONENT_SWIZZLE_IDENTITY,
            .g = VK_COMPONENT_SWIZZLE_IDENTITY,
            .b = VK_COMPONENT_SWIZZLE_IDENTITY,
            .a = VK_COMPONENT_SWIZZLE_IDENTITY,
        };

        const auto subresource_range = VkImageSubresourceRange{
            .aspectMask = aspect,
            .baseMipLevel = 0,
            .levelCount = mipmap_level,
            .baseArrayLayer = 0,
            .layerCount = 1,
        };

        const VkImageViewCreateInfo create_image_view_info{
            .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .image = image_,
            .viewType = VK_IMAGE_VIEW_TYPE_2D,
            .format = format,
            .components = components,
            .subresourceRange = subresource_range,
        };

        const auto error = vkCreateImageView(device_.get(), &create_image_view_info, nullptr, &image_view_);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not create image view.\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void TextureImageView::cleanup()
    {
        vkDestroyImageView(device_.get(), image_view_, nullptr);
    }
} // namespace engine
