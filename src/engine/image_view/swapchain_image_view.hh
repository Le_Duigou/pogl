#pragma once

#include "texture_image_view.hh"
namespace engine
{
    class Swapchain;

    class SwapchainImageView : public TextureImageView
    {
    public:
        SwapchainImageView(const VkImage& image, const Swapchain& swapchain);
    };
} // namespace engine
