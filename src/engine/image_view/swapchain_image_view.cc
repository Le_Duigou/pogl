#include "swapchain_image_view.hh"

#include <iostream>

#include "engine/error/vulkan_errors.hh"
#include "engine/swapchain/swapchain.hh"

namespace engine
{
    SwapchainImageView::SwapchainImageView(const VkImage& image, const Swapchain& swapchain)
        : TextureImageView(swapchain.logical_device(), image, swapchain.surface_format().format, 1, DEFAULT_ASPECT)
    {}
} // namespace engine
