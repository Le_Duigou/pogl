#include "file_reader.hh"

#include <fstream>
#include <stdexcept>

namespace engine
{
    FileRader::FileRader(const std::string& path)
        : path_(path)
    {}

    std::vector<std::uint8_t> FileRader::read_bytes() const
    {
        auto file = std::ifstream(path_, std::ios::ate | std::ios::binary);

        if (!file.is_open())
            throw std::runtime_error("Unable to open file " + path_);

        size_t file_size = file.tellg();
        std::vector<std::uint8_t> byte_buffer(file_size);

        file.seekg(0);
        file.read(reinterpret_cast<char*>(byte_buffer.data()), file_size);

        return byte_buffer;
    }

} // namespace engine
