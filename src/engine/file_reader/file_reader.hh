#pragma once

#include <cstdint>
#include <string>
#include <vector>

namespace engine
{
    class FileRader
    {
    public:
        FileRader(const std::string& path);

        std::vector<std::uint8_t> read_bytes() const;

    private:
        std::string path_;
    };
} // namespace engine
