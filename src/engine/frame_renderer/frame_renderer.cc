#include "frame_renderer.hh"

#include <cstdint>
#include <iostream>
#include <vulkan/vulkan_core.h>

#include "engine/command_pool/command_pool.hh"
#include "engine/error/vulkan_errors.hh"
#include "engine/swapchain/swapchain.hh"

namespace engine
{
    FrameRenderer::FrameRenderer(const swapchain_t& swapchain, const command_pool_t& command_pool)
        : swapchain_(swapchain)
        , command_pool_(command_pool)
        , current_frame_(0)
        , should_recreate_swapchain_(false)
    {
        setup();
    }

    FrameRenderer::~FrameRenderer()
    {
        cleanup();
    }

    void FrameRenderer::render()
    {
        await_previous_frame_fence();
        next_image();
        await_current_image_fences();
        reset_fences();
        submit_graphic_queue();
        present_rendered_image();
        increment_current_fame();
    }

    void FrameRenderer::await_previous_frame_fence()
    {
        constexpr uint64_t DISABLED_TIMEOUT_VALUE = UINT64_MAX;
        const fence_t current_fence = get_current_in_flight_fence();
        vkWaitForFences(logical_device().get(), 1, &current_fence, VK_TRUE, DISABLED_TIMEOUT_VALUE);
    }

    void FrameRenderer::next_image()
    {
        constexpr uint64_t DISABLED_TIMEOUT_VALUE = UINT64_MAX;

        vkAcquireNextImageKHR(logical_device().get(), swapchain_.get(), DISABLED_TIMEOUT_VALUE,
                              get_current_image_available_semaphore(), VK_NULL_HANDLE, &current_image_);
    }

    void FrameRenderer::await_current_image_fences()
    {
        constexpr uint64_t DISABLED_TIMEOUT_VALUE = UINT64_MAX;

        fence_reference_t& current_fence = get_current_image_in_flight_fence_reference();

        if (current_fence != VK_NULL_HANDLE)
        {
            vkWaitForFences(logical_device().get(), 1, &current_fence, VK_TRUE, DISABLED_TIMEOUT_VALUE);
        }

        current_fence = get_current_in_flight_fence();
    }

    void FrameRenderer::submit_graphic_queue()
    {
        const VkSemaphore wait_semaphores[] = {
            get_current_image_available_semaphore(),
        };

        const VkSemaphore signal_semaphores[] = {
            get_current_image_rendered_semaphore(),
        };

        const VkPipelineStageFlags wait_stages[] = {
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        };

        const auto submit_info = VkSubmitInfo{
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .pNext = nullptr,
            .waitSemaphoreCount = 1,
            .pWaitSemaphores = wait_semaphores,
            .pWaitDstStageMask = wait_stages,
            .commandBufferCount = 1,
            .pCommandBuffers = &command_pool_.buffers()[current_image_],
            .signalSemaphoreCount = 1,
            .pSignalSemaphores = signal_semaphores,
        };

        constexpr uint32_t SUBMISSION_COUNT = 1;
        const auto error = vkQueueSubmit(logical_device().graphic_queue(), SUBMISSION_COUNT, &submit_info,
                                         get_current_in_flight_fence());

        if (error == VK_ERROR_OUT_OF_DATE_KHR || error == VK_SUBOPTIMAL_KHR)
        {
            should_recreate_swapchain_ = true;
        }
        else if (error != VK_SUCCESS)
        {
            std::cerr << "Could not submit draw command buffer.\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void FrameRenderer::reset_fences()
    {
        const fence_t current_fence = get_current_in_flight_fence();
        vkResetFences(logical_device().get(), 1, &current_fence);
    }

    void FrameRenderer::present_rendered_image()
    {
        const VkSemaphore wait_semaphores[] = {
            get_current_image_rendered_semaphore(),
        };

        const auto present_info = VkPresentInfoKHR{
            .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
            .pNext = nullptr,
            .waitSemaphoreCount = 1,
            .pWaitSemaphores = wait_semaphores,
            .swapchainCount = 1,
            .pSwapchains = &swapchain_.get(),
            .pImageIndices = &current_image_,
            .pResults = nullptr,
        };

        const auto error = vkQueuePresentKHR(logical_device().graphic_queue(), &present_info);

        if (error == VK_ERROR_OUT_OF_DATE_KHR || error == VK_SUBOPTIMAL_KHR)
        {
            should_recreate_swapchain_ = true;
        }
        else if (error != VK_SUCCESS)
        {
            std::cerr << "Could not present swap chain image.\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void FrameRenderer::increment_current_fame()
    {
        current_frame_ = (current_frame_ + 1) % MAX_FRAME_IN_FLIGHT_COUNT;
    }

    void FrameRenderer::setup()
    {
        setup_semaphore_collection(image_available_semaphores_);
        setup_semaphore_collection(image_rendered_semaphore_);
        setup_fence_collection(in_flight_frame_fences_);
        setup_fence_reference_collection(image_in_flight_fence_references_, swapchain_.image_views().size());
    }

    void FrameRenderer::cleanup()
    {
        cleanup_fence_reference_collection(image_in_flight_fence_references_);
        cleanup_fence_collection(in_flight_frame_fences_);
        cleanup_semaphore_collection(image_rendered_semaphore_);
        cleanup_semaphore_collection(image_available_semaphores_);
    }

    void FrameRenderer::setup_semaphore_collection(semaphore_collection_t& target)
    {
        for (auto& semaphore : target)
        {
            setup_semaphore(semaphore);
        }
    }

    void FrameRenderer::cleanup_semaphore_collection(semaphore_collection_t& target)
    {
        for (auto& semaphore : target)
        {
            cleanup_semaphore(semaphore);
        }
    }

    void FrameRenderer::setup_semaphore(semaphore_t& target)
    {
        static const auto semaphore_create_info = VkSemaphoreCreateInfo{
            .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
        };

        const auto error = vkCreateSemaphore(logical_device().get(), &semaphore_create_info, nullptr, &target);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not create semaphore.\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void FrameRenderer::cleanup_semaphore(semaphore_t& target)
    {
        vkDestroySemaphore(logical_device().get(), target, nullptr);
    }

    void FrameRenderer::setup_fence_collection(fence_collection_t& target)
    {
        for (auto& fence : target)
        {
            setup_fence(fence);
        }
    }

    void FrameRenderer::cleanup_fence_collection(fence_collection_t& target)
    {
        for (auto& fence : target)
        {
            cleanup_fence(fence);
        }
    }

    void FrameRenderer::setup_fence_reference_collection(fence_reference_collection_t& target, size_t size)
    {
        target.resize(size, VK_NULL_HANDLE);
    }

    void FrameRenderer::cleanup_fence_reference_collection(fence_reference_collection_t&)
    {}

    void FrameRenderer::setup_fence(fence_t& target)
    {
        static const auto semaphore_create_info = VkFenceCreateInfo{
            .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
            .pNext = nullptr,
            .flags = VK_FENCE_CREATE_SIGNALED_BIT,
        };

        const auto error = vkCreateFence(logical_device().get(), &semaphore_create_info, nullptr, &target);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not create fence.\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void FrameRenderer::cleanup_fence(fence_t& target)
    {
        vkDestroyFence(logical_device().get(), target, nullptr);
    }
} // namespace engine
