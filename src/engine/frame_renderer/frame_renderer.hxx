#pragma once

#ifndef FRAME_RENDERER__H
#    include "frame_renderer.hh"
#endif

#include "engine/swapchain/swapchain.hh"

namespace engine
{
    inline const FrameRenderer::logical_device_t& FrameRenderer::logical_device() const
    {
        return swapchain_.logical_device();
    }

    inline const FrameRenderer::semaphore_t& FrameRenderer::get_current_image_available_semaphore() const
    {
        return image_available_semaphores_[current_frame_];
    }

    inline const FrameRenderer::semaphore_t& FrameRenderer::get_current_image_rendered_semaphore() const
    {
        return image_rendered_semaphore_[current_frame_];
    }

    inline const FrameRenderer::fence_t& FrameRenderer::get_current_in_flight_fence() const
    {
        return in_flight_frame_fences_[current_frame_];
    }

    inline FrameRenderer::fence_reference_t& FrameRenderer::get_current_image_in_flight_fence_reference()
    {
        return image_in_flight_fence_references_[current_image_];
    }

    inline bool FrameRenderer::should_recreate_swapchain() const
    {
        return should_recreate_swapchain_;
    }

} // namespace engine
