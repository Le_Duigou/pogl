
#include <array>
#include <cstddef>
#include <cstdint>
#include <vector>
#include <vulkan/vulkan_core.h>
#ifndef FRAME_RENDERER__H
#    define FRAME_RENDERER__H 1

namespace engine
{
    class Swapchain;
    class LogicalDevice;
    class CommandPool;

    class FrameRenderer
    {
    public:
        static constexpr size_t MAX_FRAME_IN_FLIGHT_COUNT = 2;

    public:
        using swapchain_t = Swapchain;
        using semaphore_t = VkSemaphore;
        using semaphore_collection_t = std::array<semaphore_t, MAX_FRAME_IN_FLIGHT_COUNT>;
        using fence_t = VkFence;
        using fence_collection_t = std::array<fence_t, MAX_FRAME_IN_FLIGHT_COUNT>;
        using fence_reference_t = fence_t;
        using fence_reference_collection_t = std::vector<fence_reference_t>;

        using logical_device_t = LogicalDevice;
        using command_pool_t = CommandPool;
        using frame_index_t = size_t;
        using image_index_t = uint32_t;

        FrameRenderer(const swapchain_t& swapchain, const command_pool_t& command_pool);
        ~FrameRenderer();

        void render();

        const logical_device_t& logical_device() const;

        bool should_recreate_swapchain() const;

    private:
        void setup();
        void cleanup();

        void await_previous_frame_fence();
        void next_image();
        void await_current_image_fences();
        void submit_graphic_queue();
        void reset_fences();
        void present_rendered_image();
        void increment_current_fame();

        void setup_semaphore_collection(semaphore_collection_t& target);
        void cleanup_semaphore_collection(semaphore_collection_t& target);

        void setup_semaphore(semaphore_t& target);
        void cleanup_semaphore(semaphore_t& target);

        void setup_fence_collection(fence_collection_t& target);
        void cleanup_fence_collection(fence_collection_t& target);
        void setup_fence_reference_collection(fence_reference_collection_t& target, size_t size);
        void cleanup_fence_reference_collection(fence_reference_collection_t& target);

        void setup_fence(fence_t& target);
        void cleanup_fence(fence_t& target);

        const semaphore_t& get_current_image_available_semaphore() const;
        const semaphore_t& get_current_image_rendered_semaphore() const;
        const fence_t& get_current_in_flight_fence() const;
        fence_reference_t& get_current_image_in_flight_fence_reference();

    private:
        const swapchain_t& swapchain_;
        const command_pool_t& command_pool_;
        semaphore_collection_t image_available_semaphores_;
        semaphore_collection_t image_rendered_semaphore_;
        fence_collection_t in_flight_frame_fences_;
        fence_reference_collection_t image_in_flight_fence_references_;
        frame_index_t current_frame_;
        image_index_t current_image_;

        bool should_recreate_swapchain_;
    };
} // namespace engine

#    include "frame_renderer.hxx"
#endif
