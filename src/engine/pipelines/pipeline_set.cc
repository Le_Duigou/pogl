#include "pipeline_set.hh"

#include <array>
#include <iostream>
#include <memory>
#include <string>

#include "engine/descriptor_pool/descriptor_pool.hh"
#include "engine/devices/logical_device.hh"
#include "engine/error/vulkan_errors.hh"
#include "engine/memory_manager/memory_manager.hh"
#include "engine/render_pass/render_pass.hh"
#include "engine/shader/shader.hh"
#include "engine/vertex/vertex_binder.hh"
#include "engine/viewport/viewport.hh"
#include "pipeline.hh"

namespace engine
{
    PipelineSet::PipelineSet(logical_device_t& device, const Viewport& viewport, const VkFormat& format,
                             const memory_t& memory)
        : device_(device)
        , memory_(memory)
    {
        setup(viewport, format);
    }

    PipelineSet::~PipelineSet()
    {
        cleanup();
    }

    void PipelineSet::setup(const Viewport& viewport, const VkFormat& format)
    {
        setup_render_pass(format);
        setup_graphic_pipeline(viewport, format);
    }

    void PipelineSet::cleanup()
    {
        cleanup_graphic_pipeline();
        cleanup_render_pass();
    }

    void PipelineSet::setup_render_pass(const VkFormat& format)
    {
        main_render_pass_ = std::make_unique<render_pass_t>(device(), format);
    }

    void PipelineSet::cleanup_render_pass()
    {
        main_render_pass_.reset();
    }

    void PipelineSet::setup_graphic_pipeline(const Viewport& viewport, const VkFormat& format)
    {
        const auto info = PipelineWrapperCreateInfo{
            .viewport = viewport,
            .format = format,
            .vertex_shader_path = MAIN_VERT_SHADER_PATH,
            .fragment_shader_path = MAIN_FRAG_SHADER_PATH,
            .stage_name = "main",
        };

        graphic_pipeline_ = std::make_unique<pipeline_t>(device_, memory_, *main_render_pass_, info);
    }

    void PipelineSet::cleanup_graphic_pipeline()
    {
        graphic_pipeline_.reset();
    }

    const PipelineSet::logical_device_t& PipelineSet::device() const
    {
        return device_;
    }

    PipelineSet::pipeline_t& PipelineSet::graphic_pipeline()
    {
#ifndef NDEBUG
        if (!graphic_pipeline_)
            throw std::runtime_error("Querying graphic pipeline render pass before initializing it");
#endif

        return *graphic_pipeline_;
    }

    const PipelineSet::pipeline_t& PipelineSet::graphic_pipeline() const
    {
#ifndef NDEBUG
        if (!graphic_pipeline_)
            throw std::runtime_error("Querying graphic pipeline render pass before initializing it");
#endif

        return *graphic_pipeline_;
    }

    PipelineSet::render_pass_t& PipelineSet::render_pass()
    {
#ifndef NDEBUG
        if (!main_render_pass_)
            throw std::runtime_error("Querying main render pass before initializing it");
#endif

        return *main_render_pass_;
    }

    const PipelineSet::render_pass_t& PipelineSet::render_pass() const
    {
#ifndef NDEBUG
        if (!main_render_pass_)
            throw std::runtime_error("Querying main render pass before initializing it");
#endif

        return *main_render_pass_;
    }

} // namespace engine
