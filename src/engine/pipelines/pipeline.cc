#include "pipeline.hh"

#include <array>
#include <iostream>
#include <memory>
#include <string>

#include "engine/descriptor_pool/descriptor_pool.hh"
#include "engine/devices/logical_device.hh"
#include "engine/error/vulkan_errors.hh"
#include "engine/memory_manager/memory_manager.hh"
#include "engine/render_pass/render_pass.hh"
#include "engine/shader/shader.hh"
#include "engine/vertex/vertex_binder.hh"
#include "engine/viewport/viewport.hh"

namespace engine
{
    Pipeline::Pipeline(const logical_device_t& device, const memory_t& memory, const render_pass_t& main_render_pass,
                       const PipelineWrapperCreateInfo& info)
        : device_(device)
        , memory_(memory)
        , main_render_pass_(main_render_pass)
    {
        setup(info);
    }

    Pipeline::~Pipeline()
    {
        cleanup();
    }

    void Pipeline::setup(const PipelineWrapperCreateInfo& info)
    {
        setup_layout();
        setup_pipeline(info);
    }

    void Pipeline::cleanup()
    {
        cleanup_pipeline();
        cleanup_layout();
    }

    void Pipeline::setup_layout()
    {
        const auto layout_info = layout_create_info();

        const auto error = vkCreatePipelineLayout(device_.get(), &layout_info, nullptr, &layout_);
        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not create pipeline layout.\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void Pipeline::cleanup_layout()
    {
        vkDestroyPipelineLayout(device_.get(), layout_, nullptr);
    }

    void Pipeline::setup_pipeline(const PipelineWrapperCreateInfo& info)
    {
        const auto vert_shader = shader_t{info.vertex_shader_path, device_};
        const auto frag_shader = shader_t{info.fragment_shader_path, device_};

        const auto vert_stage_info = vertex_stage_create_info(vert_shader, info.stage_name);
        const auto frag_stage_info = fragment_stage_create_info(frag_shader, info.stage_name);
        const auto stages_info = std::array<VkPipelineShaderStageCreateInfo, 2>{vert_stage_info, frag_stage_info};

        const auto vertex_binder = VertexBinder();
        const auto vertex_bind = vertex_binder.get_binding_description();
        const auto vertex_attr = vertex_binder.get_attribute_description();

        const auto vertex_input_info = vertex_input_create_info(vertex_bind, vertex_attr);
        const auto input_assembly_info = input_assembly_create_info();
        const auto viewport_info = viewport_state_create_info(info.viewport);
        const auto rasterizer_info = rasterizer_create_info();
        const auto multisampling_info = multisampling_create_info();

        const auto color_blend_attachment_state = color_blend_state();
        const auto color_blend_info = color_blend_create_info(color_blend_attachment_state);

        const auto depth_stencil_state = depth_stencil_state_create_info();

        /*const auto used_dynamic_states = dynamic_states();
        const auto dynamic_states_info =
            dynamic_states_create_info(used_dynamic_states);*/

        const auto pipeline_info = VkGraphicsPipelineCreateInfo{
            .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,

            .stageCount = stages_info.size(),
            .pStages = stages_info.data(),

            .pVertexInputState = &vertex_input_info,
            .pInputAssemblyState = &input_assembly_info,
            .pTessellationState = nullptr,
            .pViewportState = &viewport_info,
            .pRasterizationState = &rasterizer_info,
            .pMultisampleState = &multisampling_info,
            .pDepthStencilState = &depth_stencil_state,
            .pColorBlendState = &color_blend_info,
            .pDynamicState = nullptr, // &dynamic_states_info,

            .layout = layout_,
            .renderPass = main_render_pass_.get(),
            .subpass = 0,

            .basePipelineHandle = VK_NULL_HANDLE,
            .basePipelineIndex = -1,
        };

        const auto error =
            vkCreateGraphicsPipelines(device_.get(), VK_NULL_HANDLE, 1, &pipeline_info, nullptr, &pipeline_);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not create graphic pipeline.\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void Pipeline::cleanup_pipeline()
    {
        vkDestroyPipeline(device_.get(), pipeline_, nullptr);
    }

    VkPipelineShaderStageCreateInfo Pipeline::vertex_stage_create_info(const shader_t& shader,
                                                                       const char* stage_name) const
    {
        return VkPipelineShaderStageCreateInfo{
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .stage = VK_SHADER_STAGE_VERTEX_BIT,
            .module = shader.get(),
            .pName = stage_name,
            .pSpecializationInfo = 0,
        };
    }

    VkPipelineShaderStageCreateInfo Pipeline::fragment_stage_create_info(const shader_t& shader,
                                                                         const char* stage_name) const
    {
        return VkPipelineShaderStageCreateInfo{
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
            .module = shader.get(),
            .pName = stage_name,
            .pSpecializationInfo = 0,
        };
    }

    VkPipelineVertexInputStateCreateInfo
    Pipeline::vertex_input_create_info(const vertex_binding_desc_t& binding,
                                       const vertex_attributes_desc_t& attributes) const
    {
        return VkPipelineVertexInputStateCreateInfo{
            .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .vertexBindingDescriptionCount = static_cast<uint32_t>(binding.size()),
            .pVertexBindingDescriptions = binding.data(),
            .vertexAttributeDescriptionCount = static_cast<uint32_t>(attributes.size()),
            .pVertexAttributeDescriptions = attributes.data(),
        };
    }

    VkPipelineInputAssemblyStateCreateInfo Pipeline::input_assembly_create_info() const
    {
        return VkPipelineInputAssemblyStateCreateInfo{
            .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
            .primitiveRestartEnable = VK_FALSE,
        };
    }

    VkPipelineRasterizationStateCreateInfo Pipeline::rasterizer_create_info() const
    {
        return {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .depthClampEnable = VK_FALSE,
            .rasterizerDiscardEnable = VK_FALSE,
            .polygonMode = VK_POLYGON_MODE_FILL,
            .cullMode = VK_CULL_MODE_BACK_BIT,
            .frontFace = VK_FRONT_FACE_CLOCKWISE,
            .depthBiasEnable = VK_FALSE,
            .depthBiasConstantFactor = 0.f,
            .depthBiasClamp = 0.f,
            .depthBiasSlopeFactor = 0.f,
            .lineWidth = 1.0f,
        };
    }

    VkPipelineViewportStateCreateInfo Pipeline::viewport_state_create_info(const Viewport& viewport) const
    {
        return VkPipelineViewportStateCreateInfo{
            .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .viewportCount = 1,
            .pViewports = &viewport.viewport(),
            .scissorCount = 1,
            .pScissors = &viewport.scissor(),
        };
    }

    VkPipelineMultisampleStateCreateInfo Pipeline::multisampling_create_info() const
    {
        return VkPipelineMultisampleStateCreateInfo{
            .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
            .sampleShadingEnable = VK_FALSE,
            .minSampleShading = 1.0f,
            .pSampleMask = nullptr,
            .alphaToCoverageEnable = VK_FALSE,
            .alphaToOneEnable = VK_FALSE,
        };
    }

    VkPipelineColorBlendStateCreateInfo
    Pipeline::color_blend_create_info(const VkPipelineColorBlendAttachmentState& state) const
    {
        return VkPipelineColorBlendStateCreateInfo{
            .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .logicOpEnable = VK_FALSE,
            .logicOp = VK_LOGIC_OP_COPY,
            .attachmentCount = 1,
            .pAttachments = &state,
            .blendConstants = {0.f, 0.f, 0.f, 0.f},
        };
    }

    VkPipelineDynamicStateCreateInfo
    Pipeline::dynamic_states_create_info(const std::vector<VkDynamicState>& states) const
    {
        return VkPipelineDynamicStateCreateInfo{
            .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .dynamicStateCount = static_cast<uint32_t>(states.size()),
            .pDynamicStates = states.data(),
        };
    }

    VkPipelineLayoutCreateInfo Pipeline::layout_create_info() const
    {
        return VkPipelineLayoutCreateInfo{
            .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .setLayoutCount = 1,
            .pSetLayouts = &memory_.descriptor_pool().layout(),
            .pushConstantRangeCount = 0,
            .pPushConstantRanges = nullptr,
        };
    }

    VkPipelineColorBlendAttachmentState Pipeline::color_blend_state() const
    {
        constexpr auto RGBA_BITS =
            VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

        return VkPipelineColorBlendAttachmentState{
            .blendEnable = VK_TRUE,
            .srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
            .dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
            .colorBlendOp = VK_BLEND_OP_ADD,
            .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
            .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
            .alphaBlendOp = VK_BLEND_OP_ADD,
            .colorWriteMask = RGBA_BITS,
        };
    }

    VkPipelineDepthStencilStateCreateInfo Pipeline::depth_stencil_state_create_info() const
    {
        return VkPipelineDepthStencilStateCreateInfo{
            .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .depthTestEnable = VK_TRUE,
            .depthWriteEnable = VK_TRUE,
            .depthCompareOp = VK_COMPARE_OP_LESS,
            .depthBoundsTestEnable = VK_FALSE,
            .stencilTestEnable = VK_FALSE,
            .front = {},
            .back = {},
            .minDepthBounds = 0.f,
            .maxDepthBounds = 1.f,
        };
    }

    std::vector<VkDynamicState> Pipeline::dynamic_states() const
    {
        return {
            VK_DYNAMIC_STATE_VIEWPORT,
            VK_DYNAMIC_STATE_LINE_WIDTH,
        };
    }

    Pipeline::pipeline_t& Pipeline::pipeline()
    {
        return pipeline_;
    }

    const Pipeline::pipeline_t& Pipeline::pipeline() const
    {
        return pipeline_;
    }

    Pipeline::layout_t& Pipeline::layout()
    {
        return layout_;
    }

    const Pipeline::layout_t& Pipeline::layout() const
    {
        return layout_;
    }

} // namespace engine
