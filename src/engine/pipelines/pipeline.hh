#pragma once

#include <memory>
#include <string>
#include <vector>
#include <vulkan/vulkan_core.h>

#include "engine/vertex/vertex_binder.hh"

namespace engine
{
    class Viewport;
    class Shader;
    class LogicalDevice;
    class RenderPass;
    class MemoryManager;

    struct PipelineWrapperCreateInfo
    {
        const Viewport& viewport;
        const VkFormat& format;
        const char* vertex_shader_path;
        const char* fragment_shader_path;
        const char* stage_name;
    };

    class Pipeline
    {
    public:
        using pipeline_t = VkPipeline;
        using layout_t = VkPipelineLayout;
        using shader_t = Shader;
        using logical_device_t = LogicalDevice;
        using render_pass_t = RenderPass;
        using vertex_binding_desc_t = VertexBinder::input_binding_desc_t;
        using vertex_attributes_desc_t = VertexBinder::input_attribute_desc_t;
        using descriptor_set_layout_t = VkDescriptorSetLayout;
        using memory_t = MemoryManager;

        Pipeline(const logical_device_t& device, const memory_t& memory, const render_pass_t& main_render_pass,
                 const PipelineWrapperCreateInfo& info);
        ~Pipeline();

        layout_t& layout();
        const layout_t& layout() const;

        pipeline_t& pipeline();
        const pipeline_t& pipeline() const;

    private:
        void setup(const PipelineWrapperCreateInfo& info);
        void cleanup();

        void setup_layout();
        void cleanup_layout();

        void setup_pipeline(const PipelineWrapperCreateInfo& info);
        void cleanup_pipeline();

        VkPipelineShaderStageCreateInfo vertex_stage_create_info(const shader_t& shader, const char* stage_name) const;
        VkPipelineShaderStageCreateInfo fragment_stage_create_info(const shader_t& shader,
                                                                   const char* stage_name) const;

        VkPipelineVertexInputStateCreateInfo vertex_input_create_info(const vertex_binding_desc_t& binding,
                                                                      const vertex_attributes_desc_t& attributes) const;
        VkPipelineInputAssemblyStateCreateInfo input_assembly_create_info() const;
        VkPipelineRasterizationStateCreateInfo rasterizer_create_info() const;
        VkPipelineViewportStateCreateInfo viewport_state_create_info(const Viewport& viewport) const;
        VkPipelineMultisampleStateCreateInfo multisampling_create_info() const;
        VkPipelineColorBlendStateCreateInfo
        color_blend_create_info(const VkPipelineColorBlendAttachmentState& state) const;
        VkPipelineDynamicStateCreateInfo dynamic_states_create_info(const std::vector<VkDynamicState>& states) const;
        VkPipelineLayoutCreateInfo layout_create_info() const;
        VkPipelineDepthStencilStateCreateInfo depth_stencil_state_create_info() const;

        VkPipelineColorBlendAttachmentState color_blend_state() const;
        std::vector<VkDynamicState> dynamic_states() const;

    private:
        const logical_device_t& device_;
        const memory_t& memory_;
        const render_pass_t& main_render_pass_;

        pipeline_t pipeline_;
        layout_t layout_;
    };
} // namespace engine
