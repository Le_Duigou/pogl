#pragma once

#include <memory>
#include <string>
#include <vector>
#include <vulkan/vulkan_core.h>

#include "engine/vertex/vertex_binder.hh"

namespace engine
{
    class Pipeline;
    class LogicalDevice;
    class RenderPass;
    class MemoryManager;
    class Viewport;

    class PipelineSet
    {
    public:
        static constexpr char* MAIN_VERT_SHADER_PATH = (char*)"shaders/pbr.vert.spv";
        static constexpr char* MAIN_FRAG_SHADER_PATH = (char*)"shaders/pbr.frag.spv";

        using pipeline_t = Pipeline;
        using logical_device_t = LogicalDevice;
        using render_pass_t = RenderPass;
        using memory_t = MemoryManager;

        PipelineSet(logical_device_t& device, const Viewport& viewport, const VkFormat& format, const memory_t& memory);
        ~PipelineSet();

        const logical_device_t& device() const;

        render_pass_t& render_pass();
        const render_pass_t& render_pass() const;

        pipeline_t& graphic_pipeline();
        const pipeline_t& graphic_pipeline() const;

    private:
        void setup(const Viewport& viewport, const VkFormat& format);
        void cleanup();

        void setup_render_pass(const VkFormat& format);
        void cleanup_render_pass();

        void setup_graphic_pipeline(const Viewport& viewport, const VkFormat& format);
        void cleanup_graphic_pipeline();

    private:
        const logical_device_t& device_;
        const memory_t& memory_;
        std::unique_ptr<render_pass_t> main_render_pass_;
        std::unique_ptr<pipeline_t> graphic_pipeline_;
    };
} // namespace engine
