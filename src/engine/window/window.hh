#pragma once

#include <memory>
#include <string>
#include <vector>
#include <vulkan/vulkan_core.h>

class GLFWwindow;

namespace engine
{
    class KeyPressed;
    class Window
    {
    public:
        using instance_t = VkInstance;
        using inner_t = GLFWwindow;

        Window(const std::string& window_name, unsigned int width, unsigned int height);
        ~Window();

        const std::string& name() const;
        unsigned int width() const;
        unsigned int height() const;

        std::pair<const char**, uint32_t> engine_extensions();

        bool should_close();
        void poll_events();

        inner_t& get();
        const inner_t& get() const;

        bool has_been_resized() const;
        void reset_has_been_resized();

        const KeyPressed& key_pressed_last_frame() const;

    private:
        void setup();
        void cleanup();

        static void external_resize_callback(GLFWwindow* glfw_window, int width, int height);
        void resize_callback(unsigned int width, unsigned int height);

        static void external_key_pressed_callback(GLFWwindow* glfw_window, int key, int scancode, int action, int mods);
        void key_pressed_callback(int key, int scancode, int action, int mods);

    private:
        std::string window_name_;
        unsigned int width_;
        unsigned int height_;
        bool has_been_resized_;
        std::unique_ptr<KeyPressed> key_pressed_;

        inner_t* window_;
    };
} // namespace engine
