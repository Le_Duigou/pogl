#pragma once

#include <set>
#include <sys/types.h>

namespace engine
{
    class KeyPressed
    {
    public:
        using key_t = int;

        void reset();
        void mark_key_pressed(key_t key);
        void mark_key_released(key_t key);

        bool get_space() const;
        bool get_apostrophe() const;
        bool get_comma() const;
        bool get_minus() const;
        bool get_period() const;
        bool get_slash() const;
        bool get_0() const;
        bool get_1() const;
        bool get_2() const;
        bool get_3() const;
        bool get_4() const;
        bool get_5() const;
        bool get_6() const;
        bool get_7() const;
        bool get_8() const;
        bool get_9() const;
        bool get_semicolon() const;
        bool get_equal() const;
        bool get_a() const;
        bool get_b() const;
        bool get_c() const;
        bool get_d() const;
        bool get_e() const;
        bool get_f() const;
        bool get_g() const;
        bool get_h() const;
        bool get_i() const;
        bool get_j() const;
        bool get_k() const;
        bool get_l() const;
        bool get_m() const;
        bool get_n() const;
        bool get_o() const;
        bool get_p() const;
        bool get_q() const;
        bool get_r() const;
        bool get_s() const;
        bool get_t() const;
        bool get_u() const;
        bool get_v() const;
        bool get_w() const;
        bool get_x() const;
        bool get_y() const;
        bool get_z() const;
        bool get_left_bracket() const;
        bool get_backslash() const;
        bool get_right_bracket() const;
        bool get_grave_accent() const;
        bool get_world_1() const;
        bool get_world_2() const;
        bool get_escape() const;
        bool get_enter() const;
        bool get_tab() const;
        bool get_backspace() const;
        bool get_insert() const;
        bool get_delete() const;
        bool get_right() const;
        bool get_left() const;
        bool get_down() const;
        bool get_up() const;
        bool get_page_up() const;
        bool get_page_down() const;
        bool get_home() const;
        bool get_end() const;
        bool get_caps_lock() const;
        bool get_scroll_lock() const;
        bool get_num_lock() const;
        bool get_print_screen() const;
        bool get_pause() const;
        bool get_f1() const;
        bool get_f2() const;
        bool get_f3() const;
        bool get_f4() const;
        bool get_f5() const;
        bool get_f6() const;
        bool get_f7() const;
        bool get_f8() const;
        bool get_f9() const;
        bool get_f10() const;
        bool get_f11() const;
        bool get_f12() const;
        bool get_f13() const;
        bool get_f14() const;
        bool get_f15() const;
        bool get_f16() const;
        bool get_f17() const;
        bool get_f18() const;
        bool get_f19() const;
        bool get_f20() const;
        bool get_f21() const;
        bool get_f22() const;
        bool get_f23() const;
        bool get_f24() const;
        bool get_f25() const;
        bool get_kp_0() const;
        bool get_kp_1() const;
        bool get_kp_2() const;
        bool get_kp_3() const;
        bool get_kp_4() const;
        bool get_kp_5() const;
        bool get_kp_6() const;
        bool get_kp_7() const;
        bool get_kp_8() const;
        bool get_kp_9() const;
        bool get_kp_decimal() const;
        bool get_kp_divide() const;
        bool get_kp_multiply() const;
        bool get_kp_subtract() const;
        bool get_kp_add() const;
        bool get_kp_enter() const;
        bool get_kp_equal() const;
        bool get_left_shift() const;
        bool get_left_control() const;
        bool get_left_alt() const;
        bool get_left_super() const;
        bool get_right_shift() const;
        bool get_right_control() const;
        bool get_right_alt() const;
        bool get_right_super() const;
        bool get_menu() const;

    private:
        bool get_key(key_t key) const;

        std::set<key_t> key_pressed_;
    };
} // namespace engine
