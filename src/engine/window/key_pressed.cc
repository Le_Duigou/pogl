#include "key_pressed.hh"

#include <GLFW/glfw3.h>

namespace engine
{
    void KeyPressed::reset()
    {
        key_pressed_.clear();
    }

    void KeyPressed::mark_key_pressed(key_t key)
    {
        key_pressed_.emplace(key);
    }

    void KeyPressed::mark_key_released(key_t key)
    {
        key_pressed_.erase(key);
    }

    bool KeyPressed::get_space() const
    {
        return get_key(GLFW_KEY_SPACE);
    }
    bool KeyPressed::get_apostrophe() const
    {
        return get_key(GLFW_KEY_APOSTROPHE);
    }
    bool KeyPressed::get_comma() const
    {
        return get_key(GLFW_KEY_COMMA);
    }
    bool KeyPressed::get_minus() const
    {
        return get_key(GLFW_KEY_MINUS);
    }
    bool KeyPressed::get_period() const
    {
        return get_key(GLFW_KEY_PERIOD);
    }
    bool KeyPressed::get_slash() const
    {
        return get_key(GLFW_KEY_SLASH);
    }
    bool KeyPressed::get_0() const
    {
        return get_key(GLFW_KEY_0);
    }
    bool KeyPressed::get_1() const
    {
        return get_key(GLFW_KEY_1);
    }
    bool KeyPressed::get_2() const
    {
        return get_key(GLFW_KEY_2);
    }
    bool KeyPressed::get_3() const
    {
        return get_key(GLFW_KEY_3);
    }
    bool KeyPressed::get_4() const
    {
        return get_key(GLFW_KEY_4);
    }
    bool KeyPressed::get_5() const
    {
        return get_key(GLFW_KEY_5);
    }
    bool KeyPressed::get_6() const
    {
        return get_key(GLFW_KEY_6);
    }
    bool KeyPressed::get_7() const
    {
        return get_key(GLFW_KEY_7);
    }
    bool KeyPressed::get_8() const
    {
        return get_key(GLFW_KEY_8);
    }
    bool KeyPressed::get_9() const
    {
        return get_key(GLFW_KEY_9);
    }
    bool KeyPressed::get_semicolon() const
    {
        return get_key(GLFW_KEY_SEMICOLON);
    }
    bool KeyPressed::get_equal() const
    {
        return get_key(GLFW_KEY_EQUAL);
    }
    bool KeyPressed::get_a() const
    {
        return get_key(GLFW_KEY_A);
    }
    bool KeyPressed::get_b() const
    {
        return get_key(GLFW_KEY_B);
    }
    bool KeyPressed::get_c() const
    {
        return get_key(GLFW_KEY_C);
    }
    bool KeyPressed::get_d() const
    {
        return get_key(GLFW_KEY_D);
    }
    bool KeyPressed::get_e() const
    {
        return get_key(GLFW_KEY_E);
    }
    bool KeyPressed::get_f() const
    {
        return get_key(GLFW_KEY_F);
    }
    bool KeyPressed::get_g() const
    {
        return get_key(GLFW_KEY_G);
    }
    bool KeyPressed::get_h() const
    {
        return get_key(GLFW_KEY_H);
    }
    bool KeyPressed::get_i() const
    {
        return get_key(GLFW_KEY_I);
    }
    bool KeyPressed::get_j() const
    {
        return get_key(GLFW_KEY_J);
    }
    bool KeyPressed::get_k() const
    {
        return get_key(GLFW_KEY_K);
    }
    bool KeyPressed::get_l() const
    {
        return get_key(GLFW_KEY_L);
    }
    bool KeyPressed::get_m() const
    {
        return get_key(GLFW_KEY_M);
    }
    bool KeyPressed::get_n() const
    {
        return get_key(GLFW_KEY_N);
    }
    bool KeyPressed::get_o() const
    {
        return get_key(GLFW_KEY_O);
    }
    bool KeyPressed::get_p() const
    {
        return get_key(GLFW_KEY_P);
    }
    bool KeyPressed::get_q() const
    {
        return get_key(GLFW_KEY_Q);
    }
    bool KeyPressed::get_r() const
    {
        return get_key(GLFW_KEY_R);
    }
    bool KeyPressed::get_s() const
    {
        return get_key(GLFW_KEY_S);
    }
    bool KeyPressed::get_t() const
    {
        return get_key(GLFW_KEY_T);
    }
    bool KeyPressed::get_u() const
    {
        return get_key(GLFW_KEY_U);
    }
    bool KeyPressed::get_v() const
    {
        return get_key(GLFW_KEY_V);
    }
    bool KeyPressed::get_w() const
    {
        return get_key(GLFW_KEY_W);
    }
    bool KeyPressed::get_x() const
    {
        return get_key(GLFW_KEY_X);
    }
    bool KeyPressed::get_y() const
    {
        return get_key(GLFW_KEY_Y);
    }
    bool KeyPressed::get_z() const
    {
        return get_key(GLFW_KEY_Z);
    }
    bool KeyPressed::get_left_bracket() const
    {
        return get_key(GLFW_KEY_LEFT_BRACKET);
    }
    bool KeyPressed::get_backslash() const
    {
        return get_key(GLFW_KEY_BACKSLASH);
    }
    bool KeyPressed::get_right_bracket() const
    {
        return get_key(GLFW_KEY_RIGHT_BRACKET);
    }
    bool KeyPressed::get_grave_accent() const
    {
        return get_key(GLFW_KEY_GRAVE_ACCENT);
    }
    bool KeyPressed::get_world_1() const
    {
        return get_key(GLFW_KEY_WORLD_1);
    }
    bool KeyPressed::get_world_2() const
    {
        return get_key(GLFW_KEY_WORLD_2);
    }
    bool KeyPressed::get_escape() const
    {
        return get_key(GLFW_KEY_ESCAPE);
    }
    bool KeyPressed::get_enter() const
    {
        return get_key(GLFW_KEY_ENTER);
    }
    bool KeyPressed::get_tab() const
    {
        return get_key(GLFW_KEY_TAB);
    }
    bool KeyPressed::get_backspace() const
    {
        return get_key(GLFW_KEY_BACKSPACE);
    }
    bool KeyPressed::get_insert() const
    {
        return get_key(GLFW_KEY_INSERT);
    }
    bool KeyPressed::get_delete() const
    {
        return get_key(GLFW_KEY_DELETE);
    }
    bool KeyPressed::get_right() const
    {
        return get_key(GLFW_KEY_RIGHT);
    }
    bool KeyPressed::get_left() const
    {
        return get_key(GLFW_KEY_LEFT);
    }
    bool KeyPressed::get_down() const
    {
        return get_key(GLFW_KEY_DOWN);
    }
    bool KeyPressed::get_up() const
    {
        return get_key(GLFW_KEY_UP);
    }
    bool KeyPressed::get_page_up() const
    {
        return get_key(GLFW_KEY_PAGE_UP);
    }
    bool KeyPressed::get_page_down() const
    {
        return get_key(GLFW_KEY_PAGE_DOWN);
    }
    bool KeyPressed::get_home() const
    {
        return get_key(GLFW_KEY_HOME);
    }
    bool KeyPressed::get_end() const
    {
        return get_key(GLFW_KEY_END);
    }
    bool KeyPressed::get_caps_lock() const
    {
        return get_key(GLFW_KEY_CAPS_LOCK);
    }
    bool KeyPressed::get_scroll_lock() const
    {
        return get_key(GLFW_KEY_SCROLL_LOCK);
    }
    bool KeyPressed::get_num_lock() const
    {
        return get_key(GLFW_KEY_NUM_LOCK);
    }
    bool KeyPressed::get_print_screen() const
    {
        return get_key(GLFW_KEY_PRINT_SCREEN);
    }
    bool KeyPressed::get_pause() const
    {
        return get_key(GLFW_KEY_PAUSE);
    }
    bool KeyPressed::get_f1() const
    {
        return get_key(GLFW_KEY_F1);
    }
    bool KeyPressed::get_f2() const
    {
        return get_key(GLFW_KEY_F2);
    }
    bool KeyPressed::get_f3() const
    {
        return get_key(GLFW_KEY_F3);
    }
    bool KeyPressed::get_f4() const
    {
        return get_key(GLFW_KEY_F4);
    }
    bool KeyPressed::get_f5() const
    {
        return get_key(GLFW_KEY_F5);
    }
    bool KeyPressed::get_f6() const
    {
        return get_key(GLFW_KEY_F6);
    }
    bool KeyPressed::get_f7() const
    {
        return get_key(GLFW_KEY_F7);
    }
    bool KeyPressed::get_f8() const
    {
        return get_key(GLFW_KEY_F8);
    }
    bool KeyPressed::get_f9() const
    {
        return get_key(GLFW_KEY_F9);
    }
    bool KeyPressed::get_f10() const
    {
        return get_key(GLFW_KEY_F10);
    }
    bool KeyPressed::get_f11() const
    {
        return get_key(GLFW_KEY_F11);
    }
    bool KeyPressed::get_f12() const
    {
        return get_key(GLFW_KEY_F12);
    }
    bool KeyPressed::get_f13() const
    {
        return get_key(GLFW_KEY_F13);
    }
    bool KeyPressed::get_f14() const
    {
        return get_key(GLFW_KEY_F14);
    }
    bool KeyPressed::get_f15() const
    {
        return get_key(GLFW_KEY_F15);
    }
    bool KeyPressed::get_f16() const
    {
        return get_key(GLFW_KEY_F16);
    }
    bool KeyPressed::get_f17() const
    {
        return get_key(GLFW_KEY_F17);
    }
    bool KeyPressed::get_f18() const
    {
        return get_key(GLFW_KEY_F18);
    }
    bool KeyPressed::get_f19() const
    {
        return get_key(GLFW_KEY_F19);
    }
    bool KeyPressed::get_f20() const
    {
        return get_key(GLFW_KEY_F20);
    }
    bool KeyPressed::get_f21() const
    {
        return get_key(GLFW_KEY_F21);
    }
    bool KeyPressed::get_f22() const
    {
        return get_key(GLFW_KEY_F22);
    }
    bool KeyPressed::get_f23() const
    {
        return get_key(GLFW_KEY_F23);
    }
    bool KeyPressed::get_f24() const
    {
        return get_key(GLFW_KEY_F24);
    }
    bool KeyPressed::get_f25() const
    {
        return get_key(GLFW_KEY_F25);
    }
    bool KeyPressed::get_kp_0() const
    {
        return get_key(GLFW_KEY_KP_0);
    }
    bool KeyPressed::get_kp_1() const
    {
        return get_key(GLFW_KEY_KP_1);
    }
    bool KeyPressed::get_kp_2() const
    {
        return get_key(GLFW_KEY_KP_2);
    }
    bool KeyPressed::get_kp_3() const
    {
        return get_key(GLFW_KEY_KP_3);
    }
    bool KeyPressed::get_kp_4() const
    {
        return get_key(GLFW_KEY_KP_4);
    }
    bool KeyPressed::get_kp_5() const
    {
        return get_key(GLFW_KEY_KP_5);
    }
    bool KeyPressed::get_kp_6() const
    {
        return get_key(GLFW_KEY_KP_6);
    }
    bool KeyPressed::get_kp_7() const
    {
        return get_key(GLFW_KEY_KP_7);
    }
    bool KeyPressed::get_kp_8() const
    {
        return get_key(GLFW_KEY_KP_8);
    }
    bool KeyPressed::get_kp_9() const
    {
        return get_key(GLFW_KEY_KP_9);
    }
    bool KeyPressed::get_kp_decimal() const
    {
        return get_key(GLFW_KEY_KP_DECIMAL);
    }
    bool KeyPressed::get_kp_divide() const
    {
        return get_key(GLFW_KEY_KP_DIVIDE);
    }
    bool KeyPressed::get_kp_multiply() const
    {
        return get_key(GLFW_KEY_KP_MULTIPLY);
    }
    bool KeyPressed::get_kp_subtract() const
    {
        return get_key(GLFW_KEY_KP_SUBTRACT);
    }
    bool KeyPressed::get_kp_add() const
    {
        return get_key(GLFW_KEY_KP_ADD);
    }
    bool KeyPressed::get_kp_enter() const
    {
        return get_key(GLFW_KEY_KP_ENTER);
    }
    bool KeyPressed::get_kp_equal() const
    {
        return get_key(GLFW_KEY_KP_EQUAL);
    }
    bool KeyPressed::get_left_shift() const
    {
        return get_key(GLFW_KEY_LEFT_SHIFT);
    }
    bool KeyPressed::get_left_control() const
    {
        return get_key(GLFW_KEY_LEFT_CONTROL);
    }
    bool KeyPressed::get_left_alt() const
    {
        return get_key(GLFW_KEY_LEFT_ALT);
    }
    bool KeyPressed::get_left_super() const
    {
        return get_key(GLFW_KEY_LEFT_SUPER);
    }
    bool KeyPressed::get_right_shift() const
    {
        return get_key(GLFW_KEY_RIGHT_SHIFT);
    }
    bool KeyPressed::get_right_control() const
    {
        return get_key(GLFW_KEY_RIGHT_CONTROL);
    }
    bool KeyPressed::get_right_alt() const
    {
        return get_key(GLFW_KEY_RIGHT_ALT);
    }
    bool KeyPressed::get_right_super() const
    {
        return get_key(GLFW_KEY_RIGHT_SUPER);
    }
    bool KeyPressed::get_menu() const
    {
        return get_key(GLFW_KEY_MENU);
    }

    bool KeyPressed::get_key(key_t key) const
    {
        return key_pressed_.contains(key);
    };
} // namespace engine
