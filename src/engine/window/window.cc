
#include <cstdint>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <utility>
#include <vector>
#include <vulkan/vulkan_core.h>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "engine/error/vulkan_errors.hh"
#include "key_pressed.hh"
#include "window.hh"

namespace engine
{
    Window::Window(const std::string& window_name, unsigned int width, unsigned int height)
        : window_name_(window_name)
        , width_(width)
        , height_(height)
        , has_been_resized_(false)
        , key_pressed_(std::make_unique<KeyPressed>())
        , window_(nullptr)
    {
        if (width_ < 1 || height_ < 1)
            throw std::invalid_argument("Invalid window dimension.");

        setup();
    }

    Window::~Window()
    {
        cleanup();
    }

    bool Window::should_close()
    {
        return glfwWindowShouldClose(window_);
    }

    void Window::poll_events()
    {
        glfwPollEvents();
    }

    std::pair<const char**, uint32_t> Window::engine_extensions()
    {
        uint32_t glfwExtensionCount = 0;
        const char** glfwExtensions;
        glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

        return {glfwExtensions, glfwExtensionCount};
    }

    void Window::setup()
    {
        glfwInit();

        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

        window_ = glfwCreateWindow(width_, height_, window_name_.c_str(), nullptr, nullptr);
        glfwSetWindowUserPointer(window_, this);

        glfwSetFramebufferSizeCallback(window_, &external_resize_callback);
        glfwSetKeyCallback(window_, &external_key_pressed_callback);

        if (!window_)
            throw std::runtime_error("Unable to open the window");
    }

    void Window::cleanup()
    {
        glfwDestroyWindow(window_);
        glfwTerminate();
    }

    void Window::external_resize_callback(GLFWwindow* glfw_window, int width, int height)
    {
        Window* window = static_cast<Window*>(glfwGetWindowUserPointer(glfw_window));
        window->resize_callback(width, height);
    }

    void Window::resize_callback(unsigned int width, unsigned int height)
    {
        if (width != width_ || height != height_)
        {
            width_ = width;
            height_ = height;
            has_been_resized_ = true;
        }
    }

    void Window::external_key_pressed_callback(GLFWwindow* glfw_window, int key, int scancode, int action, int mods)
    {
        Window* window = static_cast<Window*>(glfwGetWindowUserPointer(glfw_window));
        window->key_pressed_callback(key, scancode, action, mods);
    }

    void Window::key_pressed_callback(int key, int, int action, int)
    {
        if (action == GLFW_PRESS)
            key_pressed_->mark_key_pressed(key);
        else if (action == GLFW_RELEASE)
            key_pressed_->mark_key_released(key);
    }

    bool Window::has_been_resized() const
    {
        return has_been_resized_;
    }

    void Window::reset_has_been_resized()
    {
        has_been_resized_ = false;
    }

    const std::string& Window::name() const
    {
        return window_name_;
    }

    unsigned int Window::width() const
    {
        return width_;
    }

    unsigned int Window::height() const
    {
        return height_;
    }

    Window::inner_t& Window::get()
    {
#ifndef NDEBUG
        if (!window_)
            throw std::runtime_error("Querying inner GLFW window before initializing it");
#endif

        return *window_;
    }

    const Window::inner_t& Window::get() const
    {
#ifndef NDEBUG
        if (!window_)
            throw std::runtime_error("Querying inner GLFW window before initializing it");
#endif

        return *window_;
    }

    const KeyPressed& Window::key_pressed_last_frame() const
    {
#ifndef NDEBUG
        if (!key_pressed_)
            throw std::runtime_error("Querying key press structure before initializing it");
#endif

        return *key_pressed_;
    }

} // namespace engine
