#include "render_pass.hh"

#include <array>
#include <iostream>
#include <stdexcept>

#include "engine/error/vulkan_errors.hh"

namespace engine
{
    RenderPass::RenderPass(const logical_device_t& device, const VkFormat& format)
        : device_(device)
    {
        setup(format);
    }

    RenderPass::~RenderPass()
    {
        cleanup();
    }

    void RenderPass::setup(const VkFormat& format)
    {
        const auto color_attachment_reference = get_color_attachment_reference();
        const auto depth_attachment_reference = get_depth_attachment_reference();

        const auto attachments = std::array<VkAttachmentDescription, 2>{
            get_color_attachment_description(format),
            get_depth_attachment_description(),
        };

        const auto subpasses = get_subpasses(color_attachment_reference, depth_attachment_reference);
        const auto dependency = get_subpass_dependency();

        VkRenderPassCreateInfo render_pass_create_info{
            .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .attachmentCount = attachments.size(),
            .pAttachments = attachments.data(),
            .subpassCount = static_cast<uint32_t>(subpasses.size()),
            .pSubpasses = subpasses.data(),
            .dependencyCount = 0,
            .pDependencies = &dependency,
        };

        const auto error = vkCreateRenderPass(device_.get(), &render_pass_create_info, nullptr, &pass_);
        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not create render pass.\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void RenderPass::cleanup()
    {
        vkDestroyRenderPass(device_.get(), pass_, nullptr);
    }

    VkAttachmentDescription RenderPass::get_color_attachment_description(const VkFormat& format) const
    {
        return VkAttachmentDescription{
            .flags = 0,
            .format = format,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
        };
    }

    VkAttachmentReference RenderPass::get_color_attachment_reference() const
    {
        return VkAttachmentReference{
            .attachment = 0,
            .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        };
    }
    VkAttachmentDescription RenderPass::get_depth_attachment_description() const
    {
        return VkAttachmentDescription{
            .flags = 0,
            .format = device_.get_depth_format(),
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        };
    }

    VkAttachmentReference RenderPass::get_depth_attachment_reference() const
    {
        return VkAttachmentReference{
            .attachment = 1,
            .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        };
    }

    std::vector<VkSubpassDescription> RenderPass::get_subpasses(const VkAttachmentReference& color_attachment_ref,
                                                                const VkAttachmentReference& depth_attachment_ref) const
    {
        return {
            VkSubpassDescription{
                .flags = 0,
                .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
                .inputAttachmentCount = 0,
                .pInputAttachments = nullptr,
                .colorAttachmentCount = 1,
                .pColorAttachments = &color_attachment_ref,
                .pResolveAttachments = 0,
                .pDepthStencilAttachment = &depth_attachment_ref,
                .preserveAttachmentCount = 0,
                .pPreserveAttachments = 0,
            },
        };
    }

    VkSubpassDependency RenderPass::get_subpass_dependency() const
    {
        return VkSubpassDependency{
            .srcSubpass = VK_SUBPASS_EXTERNAL,
            .dstSubpass = 0,
            .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
            .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
            .srcAccessMask = 0,
            .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
            .dependencyFlags = 0,
        };
    }

    RenderPass::inner_t& RenderPass::get()
    {
        return pass_;
    }

    const RenderPass::inner_t& RenderPass::get() const
    {
        return pass_;
    }

    const RenderPass::logical_device_t& RenderPass::device() const
    {
        return device_;
    }

} // namespace engine
