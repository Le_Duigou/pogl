#pragma once

#include <vector>
#include <vulkan/vulkan_core.h>

#include "engine/devices/logical_device.hh"

namespace engine
{
    class RenderPass
    {
    public:
        using inner_t = VkRenderPass;
        using logical_device_t = LogicalDevice;

        RenderPass(const logical_device_t& device, const VkFormat& format);
        ~RenderPass();

        const logical_device_t& device() const;

        inner_t& get();
        const inner_t& get() const;

    private:
        void setup(const VkFormat& format);
        void cleanup();

        VkAttachmentDescription get_color_attachment_description(const VkFormat& format) const;
        VkAttachmentReference get_color_attachment_reference() const;
        VkAttachmentDescription get_depth_attachment_description() const;
        VkAttachmentReference get_depth_attachment_reference() const;

        std::vector<VkSubpassDescription> get_subpasses(const VkAttachmentReference& color_attachment,
                                                        const VkAttachmentReference& depth_attachment) const;
        VkSubpassDependency get_subpass_dependency() const;

    private:
        const logical_device_t& device_;
        inner_t pass_;
    };

} // namespace engine
