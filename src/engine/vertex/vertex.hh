#pragma once

#include <fstream>

#include "engine/units/units.hh"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>

namespace engine
{
    struct Vertex
    {
        alignas(8) vec3_t location;
        alignas(8) vec3_t normal;
        alignas(8) vec3_t tangent;
        alignas(8) vec2_t uv;

        bool operator==(const Vertex& other) const;
    };

    std::ostream& operator<<(std::ostream& out, const Vertex& vertex);

} // namespace engine

namespace std
{
    template <>
    struct hash<engine::Vertex>
    {
        size_t operator()(engine::Vertex const& vertex) const
        {
            return ((hash<glm::vec3>()(vertex.location) ^ (hash<glm::vec3>()(vertex.normal) << 1)) >> 1)
                ^ (hash<glm::vec2>()(vertex.uv) << 1);
        }
    };
} // namespace std
