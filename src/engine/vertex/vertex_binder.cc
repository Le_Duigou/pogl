#include "vertex_binder.hh"

namespace engine
{
    VertexBinder::input_binding_desc_t VertexBinder::get_binding_description() const
    {
        return {
            VkVertexInputBindingDescription{
                .binding = 0,
                .stride = sizeof(Vertex),
                // VK_VERTEX_INPUT_RATE_INSTANCE for instantiation
                .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
            },
        };
    }

    VertexBinder::input_attribute_desc_t VertexBinder::get_attribute_description() const
    {
        return {
            VkVertexInputAttributeDescription{
                .location = 0,
                .binding = 0,
                .format = VK_FORMAT_R32G32B32_SFLOAT,
                .offset = offsetof(Vertex, location),
            },
            VkVertexInputAttributeDescription{
                .location = 1,
                .binding = 0,
                .format = VK_FORMAT_R32G32B32_SFLOAT,
                .offset = offsetof(Vertex, normal),
            },
            VkVertexInputAttributeDescription{
                .location = 2,
                .binding = 0,
                .format = VK_FORMAT_R32G32B32_SFLOAT,
                .offset = offsetof(Vertex, tangent),
            },
            VkVertexInputAttributeDescription{
                .location = 3,
                .binding = 0,
                .format = VK_FORMAT_R32G32_SFLOAT,
                .offset = offsetof(Vertex, uv),
            },
        };
    }
} // namespace engine
