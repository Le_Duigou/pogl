#pragma once

#include <array>
#include <vulkan/vulkan_core.h>

#include "vertex.hh"

namespace engine
{
    class VertexBinder
    {
    public:
        using input_binding_desc_t = std::array<VkVertexInputBindingDescription, 1>;
        using input_attribute_desc_t = std::array<VkVertexInputAttributeDescription, 4>;

        input_binding_desc_t get_binding_description() const;
        input_attribute_desc_t get_attribute_description() const;
    };
} // namespace engine
