#include "vertex.hh"

namespace engine
{
    std::ostream& operator<<(std::ostream& out, const Vertex& vertex)
    {
        out << "vertex{";

        out << " .location = {" << vertex.location.x << ", " << vertex.location.y << "}, ";
        out << " .color = {" << vertex.normal.r << ", " << vertex.normal.g << ", " << vertex.normal.b << "} ";
        out << " .uv = {" << vertex.uv.x << ", " << vertex.uv.y << "} ";

        out << "}";

        return out;
    }

    bool Vertex::operator==(const Vertex& other) const
    {
        return location == other.location && normal == other.normal && uv == other.uv;
    }

} // namespace engine
