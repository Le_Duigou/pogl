#include "engine_instance.hh"

#include <array>
#include <cstring>
#include <iostream>
#include <stdexcept>

#include "engine/error/vulkan_errors.hh"
#include "options/options.hh"

namespace engine
{
    const std::array<const char*, 1> REQUESTED_VALIDATION_LAYERS = {
        "VK_LAYER_KHRONOS_validation",
    };

    EngineInstance::EngineInstance(const char* application_name, std::pair<const char**, uint32_t> extensions,
                                   const options::Options& options)
    {
        const VkApplicationInfo app_info{
            .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
            .pNext = nullptr,
            .pApplicationName = application_name,
            .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
            .pEngineName = "No Engine",
            .engineVersion = VK_MAKE_VERSION(1, 0, 0),
            .apiVersion = VK_API_VERSION_1_0,
        };

        const auto layers = get_validation_layer_names();

        const VkInstanceCreateInfo create_info{
            .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .pApplicationInfo = &app_info,
            .enabledLayerCount = options.no_validation_layers ? 0 : static_cast<uint32_t>(layers.size()),
            .ppEnabledLayerNames = layers.data(),
            .enabledExtensionCount = extensions.second,
            .ppEnabledExtensionNames = extensions.first,
        };

        const auto error = vkCreateInstance(&create_info, nullptr, &instance_);

        if (error != VK_SUCCESS)
        {
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    std::vector<const char*> EngineInstance::get_validation_layer_names() const
    {
#ifdef NDEBUG
        return {};
#else
        std::vector<const char*> final_layer_list;
        final_layer_list.reserve(REQUESTED_VALIDATION_LAYERS.size());

        const auto available_layers = get_available_validation_layers();

        for (const auto& layer : REQUESTED_VALIDATION_LAYERS)
        {
            bool is_layer_available = false;
            for (const auto& available_layer_property : available_layers)
            {
                if (strcmp(layer, available_layer_property.layerName) == 0)
                {
                    is_layer_available = true;
                    break;
                }
            }

            if (!is_layer_available)
            {
                std::cerr << "Validation layer \"" << layer
                          << "\" is requested but not available on this "
                             "system. It will be ignored.";
            }
            else
            {
                final_layer_list.emplace_back(layer);
            }
        }

        return final_layer_list;
#endif
    }

    std::vector<VkLayerProperties> EngineInstance::get_available_validation_layers() const
    {
        uint32_t layer_count;
        vkEnumerateInstanceLayerProperties(&layer_count, nullptr);

        std::vector<VkLayerProperties> available_layers(layer_count);
        vkEnumerateInstanceLayerProperties(&layer_count, available_layers.data());

        return available_layers;
    }

    EngineInstance::~EngineInstance()
    {
        vkDestroyInstance(instance_, nullptr);
    }

    EngineInstance::inner_t& EngineInstance::get()
    {
        return instance_;
    }

    const EngineInstance::inner_t& EngineInstance::get() const
    {
        return instance_;
    }
} // namespace engine
