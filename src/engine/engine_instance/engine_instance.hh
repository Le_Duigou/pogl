#pragma once

#include <vector>
#include <vulkan/vulkan_core.h>

namespace options
{
    struct Options;
}

namespace engine
{
    class EngineInstance
    {
    public:
        using inner_t = VkInstance;

        EngineInstance(const char* application_name, std::pair<const char**, uint32_t> extensions,
                       const options::Options& options);
        ~EngineInstance();

        inner_t& get();
        const inner_t& get() const;

    private:
        std::vector<const char*> get_validation_layer_names() const;
        std::vector<VkLayerProperties> get_available_validation_layers() const;

    private:
        inner_t instance_;
    };
} // namespace engine
