#pragma once

#include <cstddef>
#include <cstdint>
#include <memory>
#include <vector>
#include <vulkan/vulkan_core.h>

#include "engine/buffer_memory/fwd.hh"

namespace engine
{
    class CommandPool;
    class DescriptorPool;
    class LogicalDevice;
    class UniformBufferObject;
    class TextureManager;
    class Vertex;

    class MemoryManager
    {
    public:
        using logical_device_t = LogicalDevice;
        using memory_requirements_t = VkMemoryRequirements;
        using buffer_t = VkBuffer;
        using memory_t = VkDeviceMemory;
        using command_pool_t = CommandPool;
        using descriptor_pool_t = DescriptorPool;
        using memory_buffer_t = SimpleBuffer;
        using texture_manager_t = TextureManager;

        using vertex_t = Vertex;
        using vertices_t = std::vector<vertex_t>;

        using index_t = uint32_t;
        using indices_t = std::vector<index_t>;

        using command_t = VkDrawIndexedIndirectCommand;
        using commands_t = std::vector<command_t>;

        using ubo_t = UniformBufferObject;

        MemoryManager(const logical_device_t& device, size_t image_count);
        ~MemoryManager();

        buffer_t& vertices();
        const buffer_t& vertices() const;
        buffer_t& indices();
        const buffer_t& indices() const;
        buffer_t& uniform_buffer_object(size_t index);
        const buffer_t& uniform_buffer_object(size_t index) const;
        buffer_t& draw_commands();
        const buffer_t& draw_commands() const;

        texture_manager_t& texture_manager();
        const texture_manager_t& texture_manager() const;

        void populate_vertex_buffer(const CommandPool& command_pool, const vertices_t& vertices,
                                    const indices_t& indices, const commands_t& commands);
        void populate_uniform_data(uint32_t current_image, const ubo_t& ubo);

        descriptor_pool_t& descriptor_pool();
        const descriptor_pool_t& descriptor_pool() const;

        size_t vertex_count() const;
        size_t index_count() const;
        size_t draw_command_count() const;
        size_t uniform_buffer_count() const;
        size_t uniform_buffer_size() const;

        bool is_pipeline_obsolete() const;
        void reset_is_pipeline_obsolete();

        void rebuild_descriptor_pool();

    private:
        void setup(size_t image_count);
        void cleanup();

        void setup_texture_manager();
        void cleanup_texture_manager();

        void setup_vertex_buffer();
        void cleanup_vertex_buffer();

        void setup_index_buffer();
        void cleanup_index_buffer();

        void setup_draw_command_buffer();
        void cleanup_draw_command_buffer();

        void setup_uniform_buffers(size_t image_count);
        void cleanup_uniform_buffers();

        void setup_descriptor_pool(size_t image_count);
        void cleanup_descriptor_pool();

        void resize_vertex_buffers(size_t vertice_number, size_t indice_number, size_t draw_command_count);

        memory_buffer_t create_staging_buffer(size_t size);

        void populate_vertices(const CommandPool& command_pool, const vertices_t& vertices);
        void populate_indices(const CommandPool& command_pool, const indices_t& indices);
        void populate_commands(const CommandPool& command_pool, const commands_t& commands);

        void map_vertices_to_memory(const vertices_t& vertices, const memory_t& memory);
        void map_indices_to_memory(const indices_t& indices, const memory_t& memory);
        void map_commands_to_memory(const commands_t& commands, const memory_t& memory);
        void map_ubo_to_memory(const ubo_t& ubo, const memory_t& memory);

        VkDeviceSize vertex_buffer_memory_size(size_t count) const;
        VkDeviceSize index_buffer_memory_size(size_t count) const;
        VkDeviceSize draw_command_buffer_memory_size(size_t count) const;
        VkDeviceSize uniform_buffer_memory_size(size_t count) const;

    private:
        const logical_device_t& device_;
        std::unique_ptr<texture_manager_t> texture_manager_;

        // TODO: An optimization could be to use a single buffer with offset instead of two
        std::unique_ptr<memory_buffer_t> vertex_buffer_;
        std::unique_ptr<memory_buffer_t> index_buffer_;
        std::unique_ptr<memory_buffer_t> draw_command_buffer_;

        std::vector<std::unique_ptr<memory_buffer_t>> uniform_buffers_;

        std::unique_ptr<descriptor_pool_t> descriptor_pool_;

        size_t vertex_count_;
        size_t index_count_;
        size_t draw_command_count_;
        size_t image_count_;
        size_t ubo_size_;

        bool is_pipeline_obsolete_;
    };
} // namespace engine
