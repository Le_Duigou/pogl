#include "memory_manager.hh"

#include <cstddef>
#include <cstring>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <vulkan/vulkan_core.h>

#include "engine/buffer_memory/buffer_memory.hh"
#include "engine/command_pool/command_pool.hh"
#include "engine/descriptor_pool/descriptor_pool.hh"
#include "engine/devices/logical_device.hh"
#include "engine/error/vulkan_errors.hh"
#include "engine/single_commands/buffer_memcpy.hh"
#include "engine/texture/texture_manager.hh"
#include "engine/uniform_buffer_object/uniform_buffer_object.hh"
#include "engine/vertex/vertex.hh"

namespace engine
{
    MemoryManager::MemoryManager(const logical_device_t& device, size_t image_count)
        : device_(device)
        , image_count_(image_count)
        , is_pipeline_obsolete_(false)
    {
        setup(image_count);

        vertex_count_ = 0;
        index_count_ = 0;
        ubo_size_ = 0;
    }

    MemoryManager::~MemoryManager()
    {
        cleanup();
    }

    void MemoryManager::setup(size_t image_count)
    {
        setup_texture_manager();

        setup_vertex_buffer();
        setup_index_buffer();
        setup_draw_command_buffer();

        setup_uniform_buffers(image_count);
        setup_descriptor_pool(image_count_);
    }

    void MemoryManager::cleanup()
    {
        cleanup_descriptor_pool();
        cleanup_uniform_buffers();

        cleanup_draw_command_buffer();
        cleanup_vertex_buffer();
        cleanup_index_buffer();

        cleanup_texture_manager();
    }

    void MemoryManager::rebuild_descriptor_pool()
    {
        descriptor_pool().rebuild(*this);
    }

    void MemoryManager::populate_vertex_buffer(const CommandPool& command_pool, const vertices_t& vertices,
                                               const indices_t& indices, const commands_t& commands)
    {
        // Resize to fit new dimensions (only if needed for each buffer)
        resize_vertex_buffers(vertices.size(), indices.size(), commands.size());

        // To be able to use memory areas not available from host
        // we first copy the vertices from host to device, then from device
        // to device. This allow us to be much faster when processing the
        // vertices.

        populate_vertices(command_pool, vertices);
        populate_indices(command_pool, indices);
        populate_commands(command_pool, commands);
    }

    void MemoryManager::populate_uniform_data(uint32_t current_image, const ubo_t& ubo)
    {
#ifndef NDEBUG
        if (current_image >= uniform_buffers_.size())
            throw std::runtime_error("Populating uniform data buffer of an image index bigger that buffer count");
#endif
        map_ubo_to_memory(ubo, uniform_buffers_[current_image]->memory());
    }

    void MemoryManager::setup_texture_manager()
    {
        texture_manager_ = std::make_unique<texture_manager_t>(device_);
    }

    void MemoryManager::cleanup_texture_manager()
    {
        texture_manager_.reset();
    }

    void MemoryManager::setup_vertex_buffer()
    {
        constexpr auto USAGE = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
        constexpr auto PROPERTIES = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

        vertex_count_ = 0;

        vertex_buffer_ = std::make_unique<memory_buffer_t>(device_);
        vertex_buffer_->initialize(1, USAGE, PROPERTIES);
    }

    void MemoryManager::cleanup_vertex_buffer()
    {
        vertex_buffer_.reset();
    }

    void MemoryManager::setup_index_buffer()
    {
        constexpr auto USAGE = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
        constexpr auto PROPERTIES = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

        index_count_ = 0;

        index_buffer_ = std::make_unique<memory_buffer_t>(device_);
        index_buffer_->initialize(1, USAGE, PROPERTIES);
    }

    void MemoryManager::cleanup_index_buffer()
    {
        index_buffer_.reset();
    }

    void MemoryManager::setup_draw_command_buffer()
    {
        constexpr auto USAGE = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT;
        constexpr auto PROPERTIES = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

        draw_command_count_ = 0;

        draw_command_buffer_ = std::make_unique<memory_buffer_t>(device_);
        draw_command_buffer_->initialize(1, USAGE, PROPERTIES);
    }

    void MemoryManager::cleanup_draw_command_buffer()
    {
        draw_command_buffer_.reset();
    }

    void MemoryManager::setup_uniform_buffers(size_t image_count)
    {
        constexpr size_t size = sizeof(UniformBufferObject);
        ubo_size_ = size;

        constexpr auto USAGE = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
        constexpr auto PROPERTIES = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

        uniform_buffers_.reserve(image_count);

        for (size_t i = 0; i < image_count; i++)
        {
            uniform_buffers_.emplace_back(std::make_unique<memory_buffer_t>(device_));
            uniform_buffers_.back()->initialize(size, USAGE, PROPERTIES);
        }
    }

    void MemoryManager::cleanup_uniform_buffers()
    {
        uniform_buffers_.clear();
    }

    void MemoryManager::setup_descriptor_pool(size_t image_count)
    {
        descriptor_pool_ = std::make_unique<descriptor_pool_t>(device_, image_count, *this);
    }

    void MemoryManager::cleanup_descriptor_pool()
    {
        descriptor_pool_.reset();
    }

    MemoryManager::memory_buffer_t MemoryManager::create_staging_buffer(size_t size)
    {
        constexpr auto USAGE = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        constexpr auto PROPERTIES = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

        auto result = memory_buffer_t{device_};
        result.initialize(size, USAGE, PROPERTIES);

        return result;
    }

    void MemoryManager::populate_vertices(const CommandPool& command_pool, const vertices_t& vertices)
    {
        size_t size = vertex_buffer_memory_size(vertices.size());

        memory_buffer_t staging_buffer = create_staging_buffer(size);

        map_vertices_to_memory(vertices, staging_buffer.memory());
        DeviceBufferMemcpy{command_pool}(staging_buffer.buffer(), vertex_buffer_->buffer(), size).execute();
    }

    void MemoryManager::populate_indices(const CommandPool& command_pool, const indices_t& indices)
    {
        size_t size = index_buffer_memory_size(indices.size());

        memory_buffer_t staging_buffer = create_staging_buffer(size);

        map_indices_to_memory(indices, staging_buffer.memory());
        DeviceBufferMemcpy{command_pool}(staging_buffer.buffer(), index_buffer_->buffer(), size).execute();
    }

    void MemoryManager::populate_commands(const CommandPool& command_pool, const commands_t& commands)
    {
        size_t size = draw_command_buffer_memory_size(commands.size());

        memory_buffer_t staging_buffer = create_staging_buffer(size);

        map_commands_to_memory(commands, staging_buffer.memory());
        DeviceBufferMemcpy{command_pool}(staging_buffer.buffer(), draw_command_buffer_->buffer(), size).execute();
    }

    void MemoryManager::map_vertices_to_memory(const vertices_t& vertices, const memory_t& memory)
    {
        size_t size = vertex_buffer_memory_size(vertices.size());

        void* data;
        const auto error = vkMapMemory(device_.get(), memory, 0, size, 0, &data);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Unable to start mapping vertex buffer memory\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }

        memcpy(data, vertices.data(), size);
        vkUnmapMemory(device_.get(), memory);
    }

    void MemoryManager::map_indices_to_memory(const indices_t& indices, const memory_t& memory)
    {
        size_t size = index_buffer_memory_size(indices.size());

        void* data;
        const auto error = vkMapMemory(device_.get(), memory, 0, size, 0, &data);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Unable to start mapping vertex indices buffer memory\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }

        memcpy(data, indices.data(), size);
        vkUnmapMemory(device_.get(), memory);
    }

    void MemoryManager::map_commands_to_memory(const commands_t& commands, const memory_t& memory)
    {
        size_t size = draw_command_buffer_memory_size(commands.size());

        void* data;
        const auto error = vkMapMemory(device_.get(), memory, 0, size, 0, &data);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Unable to start mapping draw commands buffer memory\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }

        memcpy(data, commands.data(), size);
        vkUnmapMemory(device_.get(), memory);
    }

    void MemoryManager::map_ubo_to_memory(const ubo_t& ubo, const memory_t& memory)
    {
        size_t size = uniform_buffer_memory_size(1);

        void* data;
        const auto error = vkMapMemory(device_.get(), memory, 0, size, 0, &data);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Unable to start mapping uniform buffer object memory\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }

        memcpy(data, &ubo, size);
        vkUnmapMemory(device_.get(), memory);
    }

    void MemoryManager::resize_vertex_buffers(size_t vertice_number, size_t indice_number, size_t command_count)
    {
        bool has_changed = false;

        if (vertice_number != vertex_count())
        {
            vertex_buffer_->resize(vertex_buffer_memory_size(vertice_number));
            vertex_count_ = vertice_number;
            has_changed = true;
        }

        if (indice_number != index_count())
        {
            index_buffer_->resize(index_buffer_memory_size(indice_number));
            index_count_ = indice_number;
            has_changed = true;
        }

        if (command_count != draw_command_count())
        {
            draw_command_buffer_->resize(draw_command_buffer_memory_size(command_count));
            draw_command_count_ = command_count;
            has_changed = true;
        }

        is_pipeline_obsolete_ = is_pipeline_obsolete_ || has_changed;
    }

    VkDeviceSize MemoryManager::vertex_buffer_memory_size(size_t count) const
    {
        return sizeof(vertex_t) * count;
    }

    VkDeviceSize MemoryManager::index_buffer_memory_size(size_t count) const
    {
        return sizeof(index_t) * count;
    }

    VkDeviceSize MemoryManager::draw_command_buffer_memory_size(size_t count) const
    {
        return sizeof(command_t) * count;
    }

    VkDeviceSize MemoryManager::uniform_buffer_memory_size(size_t count) const
    {
        return sizeof(ubo_t) * count;
    }

    MemoryManager::buffer_t& MemoryManager::vertices()
    {
        return vertex_buffer_->buffer();
    }

    const MemoryManager::buffer_t& MemoryManager::vertices() const
    {
        return vertex_buffer_->buffer();
    }

    MemoryManager::buffer_t& MemoryManager::indices()
    {
        return index_buffer_->buffer();
    }

    const MemoryManager::buffer_t& MemoryManager::indices() const
    {
        return index_buffer_->buffer();
    }

    MemoryManager::buffer_t& MemoryManager::uniform_buffer_object(size_t index)
    {
        return uniform_buffers_[index]->buffer();
    }

    const MemoryManager::buffer_t& MemoryManager::uniform_buffer_object(size_t index) const
    {
        return uniform_buffers_[index]->buffer();
    }

    MemoryManager::buffer_t& MemoryManager::draw_commands()
    {
        return draw_command_buffer_->buffer();
    }

    const MemoryManager::buffer_t& MemoryManager::draw_commands() const
    {
        return draw_command_buffer_->buffer();
    }

    size_t MemoryManager::vertex_count() const
    {
        return vertex_count_;
    }

    size_t MemoryManager::index_count() const
    {
        return index_count_;
    }

    size_t MemoryManager::draw_command_count() const
    {
        return draw_command_count_;
    }

    size_t MemoryManager::uniform_buffer_count() const
    {
        return uniform_buffers_.size();
    }

    size_t MemoryManager::uniform_buffer_size() const
    {
        return ubo_size_;
    }

    MemoryManager::descriptor_pool_t& MemoryManager::descriptor_pool()
    {
#ifndef NDEBUG
        if (!descriptor_pool_)
            throw std::runtime_error("Querying descriptor pool before initializing it");
#endif

        return *descriptor_pool_;
    }

    const MemoryManager::descriptor_pool_t& MemoryManager::descriptor_pool() const
    {
#ifndef NDEBUG
        if (!descriptor_pool_)
            throw std::runtime_error("Querying descriptor pool before initializing it");
#endif

        return *descriptor_pool_;
    }

    bool MemoryManager::is_pipeline_obsolete() const
    {
        return is_pipeline_obsolete_;
    }

    void MemoryManager::reset_is_pipeline_obsolete()
    {
        is_pipeline_obsolete_ = false;
    }

    MemoryManager::texture_manager_t& MemoryManager::texture_manager()
    {
#ifndef NDEBUG
        if (!texture_manager_)
            throw std::runtime_error("Querying Texture manager before initializing it");
#endif

        return *texture_manager_;
    }

    const MemoryManager::texture_manager_t& MemoryManager::texture_manager() const
    {
#ifndef NDEBUG
        if (!texture_manager_)
            throw std::runtime_error("Querying Texture manager before initializing it");
#endif
        return *texture_manager_;
    }

} // namespace engine
