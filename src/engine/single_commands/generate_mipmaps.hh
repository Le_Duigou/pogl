#pragma once
#include "transition_single_command.hh"

namespace engine
{
    class Image;
    class GenerateMipMaps : public TransitionSingleCommand
    {
    public:
        GenerateMipMaps& operator()(image_t& image);

    private:
        void generate_single_mipmap(image_t& image, uint32_t level);
        void transition_source(image_t& image, uint32_t level, layout_t new_layout);
        void copy_mip_level(image_t& image, uint32_t level);
    };
} // namespace engine
