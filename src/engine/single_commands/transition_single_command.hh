#pragma once

#include <cstddef>
#include <cstdint>
#include <vulkan/vulkan_core.h>

#include "engine/command_pool/single_use_command_buffer.hh"

namespace engine
{
    class Image;
    class TransitionSingleCommand : public SingleUseCommandBuffer
    {
    public:
        using image_t = Image;
        using layout_t = VkImageLayout;

    protected:
        static void set_transformation_dependent_parameters(layout_t old_layout, layout_t new_layout,
                                                            VkPipelineStageFlags& source_stage,
                                                            VkPipelineStageFlags& destination_stage,
                                                            VkImageMemoryBarrier& barrier);
    };
} // namespace engine
