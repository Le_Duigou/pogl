#include "buffer_memcpy.hh"

namespace engine
{
    DeviceBufferMemcpy& DeviceBufferMemcpy::operator()(const buffer_t& src, const buffer_t& dst, size_t size)
    {
        const auto copy_region = VkBufferCopy{
            .srcOffset = 0,
            .dstOffset = 0,
            .size = size,
        };

        vkCmdCopyBuffer(command_buffer_, src, dst, 1, &copy_region);

        return *this;
    }

} // namespace engine
