#pragma once

#include <vulkan/vulkan_core.h>

#include "engine/command_pool/single_use_command_buffer.hh"

namespace engine
{
    class DeviceImageMemcpy : public SingleUseCommandBuffer
    {
    public:
        using buffer_t = VkBuffer;

        DeviceImageMemcpy& operator()(VkBuffer& src, VkImage& dst, VkExtent3D extent);
    };
} // namespace engine
