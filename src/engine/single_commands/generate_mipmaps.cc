#include "generate_mipmaps.hh"

#include <cmath>
#include <cstddef>
#include <cstdint>
#include <stdexcept>
#include <vulkan/vulkan_core.h>

#include "engine/buffer_memory/buffer_memory.hh"
#include "engine/command_pool/command_pool.hh"
#include "engine/image/image.hh"

namespace engine
{
    GenerateMipMaps& GenerateMipMaps::operator()(image_t& image)
    {
        for (uint32_t i = 0; i < image.mipmap_level_ - 1; i++)
        {
            generate_single_mipmap(image, i);
        }
        transition_source(image, image.mipmap_level_ - 1, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

        return *this;
    }

    void GenerateMipMaps::generate_single_mipmap(image_t& image, uint32_t level)
    {
        transition_source(image, level, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
        transition_source(image, level + 1, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
        copy_mip_level(image, level);
        transition_source(image, level, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    }

    void GenerateMipMaps::transition_source(image_t& image, uint32_t level, layout_t new_layout)
    {
        const uint32_t src_level = level;
        const layout_t old_layout = image.layout(level);

        const auto subresource_range = VkImageSubresourceRange{
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = src_level,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        };

        auto barrier = VkImageMemoryBarrier{
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .pNext = nullptr,
            .srcAccessMask = 0,
            .dstAccessMask = 0,
            .oldLayout = old_layout,
            .newLayout = new_layout,
            .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .image = image.buffer().buffer(),
            .subresourceRange = subresource_range,
        };

        VkPipelineStageFlags source_stage;
        VkPipelineStageFlags destination_stage;
        set_transformation_dependent_parameters(old_layout, new_layout, source_stage, destination_stage, barrier);

        vkCmdPipelineBarrier(command_buffer_, source_stage, destination_stage, 0, 0, nullptr, 0, nullptr, 1, &barrier);

        image.layouts_[src_level] = new_layout;
    }

    void GenerateMipMaps::copy_mip_level(image_t& image, uint32_t level)
    {
        const int32_t src_width = std::max<int32_t>(image.width_ / std::pow(2, level), 1);
        const int32_t src_height = std::max<int32_t>(image.height_ / std::pow(2, level), 1);

        const int32_t dst_width = src_width > 1 ? src_width / 2 : 1;
        const int32_t dst_height = src_height > 1 ? src_height / 2 : 1;

        const auto src_subresource_layer = VkImageSubresourceLayers{
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = level,
            .baseArrayLayer = 0,
            .layerCount = 1,
        };

        const auto dst_subresource_layer = VkImageSubresourceLayers{
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = level + 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        };

        const auto blit = VkImageBlit{
            .srcSubresource = src_subresource_layer,
            .srcOffsets = {{0, 0, 0}, {src_width, src_height, 1}},
            .dstSubresource = dst_subresource_layer,
            .dstOffsets = {{0, 0, 0}, {dst_width, dst_height, 1}},
        };

        vkCmdBlitImage(command_buffer_, //
                       image.buffer().buffer(), VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, //
                       image.buffer().buffer(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, //
                       1, &blit, VK_FILTER_LINEAR);
    }

} // namespace engine
