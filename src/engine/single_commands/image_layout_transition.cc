#include "image_layout_transition.hh"

#include <stdexcept>

#include "engine/buffer_memory/buffer_memory.hh"
#include "engine/image/image.hh"

namespace engine
{
    ImageLayoutTransition& ImageLayoutTransition::operator()(image_t& image, layout_t new_layout, size_t level)
    {
        const layout_t old_layout = image.layout(level);

        const auto subresource_range = VkImageSubresourceRange{
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = image.mipmap_level_,
            .baseArrayLayer = 0,
            .layerCount = 1,
        };

        auto barrier = VkImageMemoryBarrier{
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .pNext = nullptr,
            .srcAccessMask = 0,
            .dstAccessMask = 0,
            .oldLayout = old_layout,
            .newLayout = new_layout,
            .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .image = image.buffer().buffer(),
            .subresourceRange = subresource_range,
        };

        VkPipelineStageFlags source_stage;
        VkPipelineStageFlags destination_stage;
        set_transformation_dependent_parameters(old_layout, new_layout, source_stage, destination_stage, barrier);

        vkCmdPipelineBarrier(command_buffer_, source_stage, destination_stage, 0, 0, nullptr, 0, nullptr, 1, &barrier);

        image.layouts_[level] = new_layout;

        return *this;
    }

} // namespace engine
