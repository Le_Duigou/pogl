#pragma once

#include "engine/command_pool/single_use_command_buffer.hh"

namespace engine
{
    class DeviceBufferMemcpy : public SingleUseCommandBuffer
    {
    public:
        using buffer_t = VkBuffer;

        DeviceBufferMemcpy& operator()(const buffer_t& src, const buffer_t& dst, size_t size);
    };
} // namespace engine
