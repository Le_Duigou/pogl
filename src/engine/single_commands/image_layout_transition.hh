#pragma once

#include "transition_single_command.hh"

namespace engine
{
    class Image;
    class ImageLayoutTransition : public TransitionSingleCommand
    {
    public:
        ImageLayoutTransition& operator()(image_t& image, layout_t new_layout, size_t level = 0);
    };
} // namespace engine
