#include "descriptor_pool.hh"

#include <cstdint>
#include <iostream>
#include <stdexcept>

#include "engine/error/vulkan_errors.hh"
#include "engine/memory_manager/memory_manager.hh"
#include "engine/texture/texture_manager.hh"

namespace engine
{
    DescriptorPool::DescriptorPool(const logical_device_t& device, size_t image_count, const memory_t& memory)
        : device_(device)
    {
        setup(image_count, memory);
    }

    DescriptorPool::~DescriptorPool()
    {
        cleanup();
    }

    void DescriptorPool::setup(size_t image_count, const memory_t& memory)
    {
        setup_layout();
        setup_pool(image_count);
        setup_sets(image_count);

        rebuild(memory);
    }

    void DescriptorPool::cleanup()
    {
        cleanup_set();
        cleanup_pool();
        cleanup_layout();
    }

    void DescriptorPool::rebuild(const memory_t& memory)
    {
#ifndef NDEBUG
        if (sets_.size() == 0)
            throw std::runtime_error("Rebuilding uninitialized descriptor pool");
#endif

        for (size_t i = 0; i < sets_.size(); i++)
        {
            setup_single_set(memory.uniform_buffer_object(i), memory, sets_[i]);
        }
    }

    void DescriptorPool::setup_layout()
    {
        const auto bindings = std::array<VkDescriptorSetLayoutBinding, 5>{
            VkDescriptorSetLayoutBinding{
                // ubo bindings
                .binding = 0,
                .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                .descriptorCount = 1,
                .stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
                .pImmutableSamplers = nullptr,
            },
            VkDescriptorSetLayoutBinding{
                // diffuse sampler
                .binding = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .descriptorCount = 1,
                .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
                .pImmutableSamplers = nullptr,
            },
            VkDescriptorSetLayoutBinding{
                // orm sampler
                .binding = 2,
                .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .descriptorCount = 1,
                .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
                .pImmutableSamplers = nullptr,
            },
            VkDescriptorSetLayoutBinding{
                // normal sampler
                .binding = 3,
                .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .descriptorCount = 1,
                .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
                .pImmutableSamplers = nullptr,
            },
            VkDescriptorSetLayoutBinding{
                // environment sampler
                .binding = 4,
                .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .descriptorCount = 1,
                .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
                .pImmutableSamplers = nullptr,
            },
        };

        const auto create_info = VkDescriptorSetLayoutCreateInfo{
            .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .bindingCount = bindings.size(),
            .pBindings = bindings.data(),
        };

        const auto error = vkCreateDescriptorSetLayout(device_.get(), &create_info, nullptr, &layout_);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not create descriptor set layout.\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void DescriptorPool::setup_pool(size_t image_count)
    {
        const uint32_t count = image_count;
        const auto pool_sizes = std::array<VkDescriptorPoolSize, 2>{
            VkDescriptorPoolSize{
                .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                .descriptorCount = count,
            },
            VkDescriptorPoolSize{
                .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .descriptorCount = count * 4,
            },
        };

        const auto pool_create_info = VkDescriptorPoolCreateInfo{
            .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .maxSets = count,
            .poolSizeCount = pool_sizes.size(),
            .pPoolSizes = pool_sizes.data(),
        };

        const auto error = vkCreateDescriptorPool(device_.get(), &pool_create_info, nullptr, &pool_);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not create descriptor pool.\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void DescriptorPool::setup_sets(size_t image_count)
    {
        std::vector<VkDescriptorSetLayout> layouts(image_count, layout_);
        VkDescriptorSetAllocateInfo descriptor_allocation_info{
            .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
            .pNext = nullptr,
            .descriptorPool = pool_,
            .descriptorSetCount = static_cast<uint32_t>(layouts.size()),
            .pSetLayouts = layouts.data(),
        };

        sets_.resize(image_count);
        const auto error = vkAllocateDescriptorSets(device_.get(), &descriptor_allocation_info, sets_.data());

        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not create descriptor sets.\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void DescriptorPool::setup_single_set(const VkBuffer& uniform_buffer, const memory_t& memory,
                                          descriptor_set_t& target)
    {
        const auto buffer_info = VkDescriptorBufferInfo{
            .buffer = uniform_buffer,
            .offset = 0,
            .range = VK_WHOLE_SIZE,
        };
        const auto descriptors = memory.texture_manager().descriptors_of_set("main");

        auto descriptor_write = std::array<VkWriteDescriptorSet, 5>{

            // UBO
            VkWriteDescriptorSet{
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .pNext = nullptr,
                .dstSet = target,
                .dstBinding = 0,
                .dstArrayElement = 0,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                .pImageInfo = nullptr,
                .pBufferInfo = &buffer_info,
                .pTexelBufferView = nullptr,
            },

            // Diffuse Texture
            VkWriteDescriptorSet{
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .pNext = nullptr,
                .dstSet = target,
                .dstBinding = 1,
                .dstArrayElement = 0,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .pImageInfo = &descriptors.diffuse,
                .pBufferInfo = nullptr,
                .pTexelBufferView = nullptr,
            },

            // OcclusionRoughnessMetallic Texture
            VkWriteDescriptorSet{
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .pNext = nullptr,
                .dstSet = target,
                .dstBinding = 2,
                .dstArrayElement = 0,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .pImageInfo = &descriptors.orm,
                .pBufferInfo = nullptr,
                .pTexelBufferView = nullptr,
            },

            // Normal Texture
            VkWriteDescriptorSet{
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .pNext = nullptr,
                .dstSet = target,
                .dstBinding = 3,
                .dstArrayElement = 0,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .pImageInfo = &descriptors.normal,
                .pBufferInfo = nullptr,
                .pTexelBufferView = nullptr,
            },

            // Environment Texture
            VkWriteDescriptorSet{
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .pNext = nullptr,
                .dstSet = target,
                .dstBinding = 4,
                .dstArrayElement = 0,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .pImageInfo = &descriptors.environemnt,
                .pBufferInfo = nullptr,
                .pTexelBufferView = nullptr,
            },
        };

        vkUpdateDescriptorSets(device_.get(), descriptor_write.size(), descriptor_write.data(), 0, nullptr);
    }

    void DescriptorPool::cleanup_layout()
    {
        vkDestroyDescriptorSetLayout(device_.get(), layout_, nullptr);
    }

    void DescriptorPool::cleanup_pool()
    {
        vkDestroyDescriptorPool(device_.get(), pool_, nullptr);
    }

    void DescriptorPool::cleanup_set()
    {}

    DescriptorPool::inner_t& DescriptorPool::get()
    {
        return pool_;
    }

    const DescriptorPool::inner_t& DescriptorPool::get() const
    {
        return pool_;
    }

    const DescriptorPool::layout_t& DescriptorPool::layout() const
    {
        return layout_;
    }

    const DescriptorPool::descriptor_sets_t& DescriptorPool::sets() const
    {
        return sets_;
    }
} // namespace engine
