#pragma once

#include <vulkan/vulkan_core.h>

#include "engine/devices/logical_device.hh"

namespace engine
{
    class LogicalDevice;
    class MemoryManager;

    class DescriptorPool
    {
    public:
        using logical_device_t = LogicalDevice;
        using inner_t = VkDescriptorPool;
        using descriptor_set_t = VkDescriptorSet;
        using descriptor_sets_t = std::vector<descriptor_set_t>;
        using memory_t = MemoryManager;
        using layout_t = VkDescriptorSetLayout;

        DescriptorPool(const logical_device_t& device, size_t image_count, const memory_t& memory);
        ~DescriptorPool();

        void rebuild(const memory_t& memory);

        DescriptorPool::inner_t& get();
        const DescriptorPool::inner_t& get() const;

        const descriptor_sets_t& sets() const;

        const layout_t& layout() const;

    private:
        void setup(size_t image_count, const memory_t& memory);
        void cleanup();

        void setup_layout();
        void setup_pool(size_t image_count);
        void setup_sets(size_t image_count);
        void setup_single_set(const VkBuffer& uniform_buffer, const memory_t& memory, descriptor_set_t& target);
        void cleanup_pool();
        void cleanup_set();
        void cleanup_layout();

    private:
        const logical_device_t& device_;
        inner_t pool_;
        descriptor_sets_t sets_;
        layout_t layout_;
    };
} // namespace engine
