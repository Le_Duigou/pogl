#include "single_use_command_buffer.hh"

#include "command_pool.hh"

namespace engine
{
    SingleUseCommandBuffer::SingleUseCommandBuffer(const command_pool_t& command_pool)
        : command_pool_(command_pool)
        , executed_(false)
    {
        const auto allocation_info = VkCommandBufferAllocateInfo{
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            .pNext = nullptr,
            .commandPool = command_pool_.get(),
            .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
            .commandBufferCount = 1,
        };

        vkAllocateCommandBuffers(command_pool_.device().get(), &allocation_info, &command_buffer_);

        const auto begin_info = VkCommandBufferBeginInfo{
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            .pNext = nullptr,
            .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
            .pInheritanceInfo = nullptr,
        };

        vkBeginCommandBuffer(command_buffer_, &begin_info);
    }

    void SingleUseCommandBuffer::execute()
    {
        if (executed_)
            return;

        vkEndCommandBuffer(command_buffer_);

        VkSubmitInfo submit_info{
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .pNext = nullptr,
            .waitSemaphoreCount = 0,
            .pWaitSemaphores = nullptr,
            .pWaitDstStageMask = nullptr,
            .commandBufferCount = 1,
            .pCommandBuffers = &command_buffer_,
            .signalSemaphoreCount = 0,
            .pSignalSemaphores = nullptr,
        };

        vkQueueSubmit(command_pool_.device().graphic_queue(), 1, &submit_info, VK_NULL_HANDLE);
        vkQueueWaitIdle(command_pool_.device().graphic_queue());

        vkFreeCommandBuffers(command_pool_.device().get(), command_pool_.get(), 1, &command_buffer_);

        executed_ = true;
    }
    SingleUseCommandBuffer::~SingleUseCommandBuffer()
    {
        execute();
    }
} // namespace engine
