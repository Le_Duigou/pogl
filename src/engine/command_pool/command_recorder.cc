#include "command_recorder.hh"

#include <cstddef>
#include <cstdint>
#include <iostream>
#include <vulkan/vulkan_core.h>

#include "engine/error/vulkan_errors.hh"
#include "engine/memory_manager/memory_manager.hh"
#include "engine/pipelines/pipeline.hh"
#include "engine/pipelines/pipeline_set.hh"
#include "engine/render_pass/render_pass.hh"

namespace engine
{
    CommandRecorder::CommandRecorder(const pipeline_t& pipeline, const extent_t& extent, const memory_t& renderer)
        : pipeline_(pipeline)
        , extent_(extent)
        , memory_(renderer)
    {}

    void CommandRecorder::operator()(const command_buffer_t& buffer, const frame_buffer_t& frame_buffer,
                                     const VkDescriptorSet& descriptor_set) const
    {
        start_recording(buffer);

        record_render_pass(buffer, frame_buffer, descriptor_set);

        end_recording(buffer);
    }

    void CommandRecorder::start_recording(const command_buffer_t& buffer) const
    {
        const auto info = VkCommandBufferBeginInfo{
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            .pNext = nullptr,
            .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
            .pInheritanceInfo = 0,
        };

        const auto error = vkBeginCommandBuffer(buffer, &info);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not start recording command buffer.\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void CommandRecorder::end_recording(const command_buffer_t& buffer) const
    {
        const auto error = vkEndCommandBuffer(buffer);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not end recording command buffer.\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void CommandRecorder::record_render_pass(const command_buffer_t& buffer, const frame_buffer_t& frame_buffer,
                                             const VkDescriptorSet& descriptor_set) const
    {
        const auto clear_value = std::vector<VkClearValue>{
            VkClearValue{
                .color = {.float32 = {0.f, 0.f, 0.f, 1.f}},
            },
            VkClearValue{
                .depthStencil = {.depth = 1.f, .stencil = 0},
            },
        };
        const auto render_pass_info = get_render_pass_begin_info(frame_buffer, clear_value);

        vkCmdBeginRenderPass(buffer, &render_pass_info, VK_SUBPASS_CONTENTS_INLINE);

        record_pipeline_binding(buffer);
        record_vertex_buffer_binding(buffer);
        record_descriptor_binding(buffer, descriptor_set);
        record_draw_command(buffer);

        vkCmdEndRenderPass(buffer);
    }

    void CommandRecorder::record_pipeline_binding(const command_buffer_t& buffer) const
    {
        vkCmdBindPipeline(buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline_.graphic_pipeline().pipeline());
    }

    void CommandRecorder::record_vertex_buffer_binding(const command_buffer_t& buffer) const
    {
        VkBuffer vertex_buffers[] = {memory_.vertices()};
        VkBuffer index_buffers = memory_.indices();
        VkDeviceSize offsets[] = {0};

        vkCmdBindVertexBuffers(buffer, 0, 1, vertex_buffers, offsets);
        vkCmdBindIndexBuffer(buffer, index_buffers, 0, VK_INDEX_TYPE_UINT32);
    }

    void CommandRecorder::record_descriptor_binding(const command_buffer_t& buffer,
                                                    const VkDescriptorSet& descriptor_set) const
    {
        vkCmdBindDescriptorSets(buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline_.graphic_pipeline().layout(), 0, 1,
                                &descriptor_set, 0, nullptr);
    }

    void CommandRecorder::record_draw_command(const command_buffer_t& buffer) const
    {
        const auto features = pipeline_.device().physical_device().get_features();

        const uint32_t draw_count = memory_.draw_command_count();
        constexpr VkDeviceSize draw_command_offset = 0;
        constexpr uint32_t stride = sizeof(VkDrawIndexedIndirectCommand);

        if (features.multiDrawIndirect)
        {
            vkCmdDrawIndexedIndirect(buffer, memory_.draw_commands(), draw_command_offset, draw_count, stride);
        }
        else
        {
            // Fallback if multidraw is not supported
            for (uint32_t i = 0; i < draw_count; i++)
            {
                const VkDeviceSize current_offset = draw_command_offset + i * stride;
                vkCmdDrawIndexedIndirect(buffer, memory_.draw_commands(), current_offset, 1, stride);
            }
        }
    }

    VkRenderPassBeginInfo
    CommandRecorder::get_render_pass_begin_info(const frame_buffer_t& frame_buffer,
                                                const std::vector<VkClearValue>& clear_values) const
    {
        const auto render_area = VkRect2D{
            .offset = {0, 0},
            .extent = extent_,
        };

        return VkRenderPassBeginInfo{
            .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
            .pNext = nullptr,
            .renderPass = pipeline_.render_pass().get(),
            .framebuffer = frame_buffer,
            .renderArea = render_area,
            .clearValueCount = static_cast<uint32_t>(clear_values.size()),
            .pClearValues = clear_values.data(),
        };
    }
} // namespace engine
