#pragma once

#include <vulkan/vulkan_core.h>

namespace engine
{
    class CommandPool;

    class SingleUseCommandBuffer
    {
    public:
        using command_pool_t = CommandPool;
        using command_buffer_t = VkCommandBuffer;

        SingleUseCommandBuffer(const command_pool_t& command_pool);
        void execute();
        ~SingleUseCommandBuffer();

    protected:
        const command_pool_t& command_pool_;
        command_buffer_t command_buffer_;

    private:
        bool executed_;
    };
} // namespace engine
