#pragma once

#include <cstddef>
#include <vector>
#include <vulkan/vulkan_core.h>

namespace engine
{
    class RenderPass;
    class PipelineSet;
    class MemoryManager;

    class CommandRecorder
    {
    public:
        using command_buffer_t = VkCommandBuffer;
        using render_pass_t = RenderPass;
        using frame_buffer_t = VkFramebuffer;
        using pipeline_t = PipelineSet;
        using memory_t = MemoryManager;
        using extent_t = VkExtent2D;

        CommandRecorder(const pipeline_t& pipeline, const extent_t& extent, const memory_t& memory);

        void operator()(const command_buffer_t& buffer, const frame_buffer_t& frame_buffer,
                        const VkDescriptorSet& descriptor_set) const;

    private:
        void start_recording(const command_buffer_t& buffer) const;
        void end_recording(const command_buffer_t& buffer) const;

        void record_render_pass(const command_buffer_t& buffer, const frame_buffer_t& frame_buffer,
                                const VkDescriptorSet& descriptor_set) const;

        void record_pipeline_binding(const command_buffer_t& buffer) const;
        void record_vertex_buffer_binding(const command_buffer_t& buffer) const;
        void record_descriptor_binding(const command_buffer_t& buffer, const VkDescriptorSet& descriptor_set) const;
        void record_draw_command(const command_buffer_t& buffer) const;

        VkRenderPassBeginInfo get_render_pass_begin_info(const frame_buffer_t& frame_buffer,
                                                         const std::vector<VkClearValue>& clear_value) const;

    private:
        const pipeline_t& pipeline_;
        const extent_t& extent_;
        const memory_t& memory_;
    };
} // namespace engine
