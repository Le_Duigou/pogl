#include "command_pool.hh"

#include <cstddef>
#include <iostream>
#include <stdexcept>

#include "engine/command_pool/command_recorder.hh"
#include "engine/descriptor_pool/descriptor_pool.hh"
#include "engine/error/vulkan_errors.hh"
#include "engine/frame_buffer/frame_buffer.hh"

namespace engine
{
    CommandPool::CommandPool(physical_device_t& physical_device, logical_device_t& logical_device, size_t image_count)
        : physical_device_(physical_device)
        , logical_device_(logical_device)
    {
        setup(image_count);
    }

    CommandPool::~CommandPool()
    {
        cleanup();
    }

    void CommandPool::setup(size_t image_count)
    {
        setup_pool();
        setup_buffers(image_count);
    }

    void CommandPool::cleanup()
    {
        cleanup_pool();
    }

    void CommandPool::record_buffers(const frame_buffer_list_t& frame_buffer_list, const descriptors_t& descriptors,
                                     const CommandRecorder& recorder)
    {
        for (size_t i = 0; i < buffers_.size(); i++)
        {
            const auto frame_buffer = frame_buffer_list.get()[i];
            const auto descriptor_set = descriptors.sets()[i];

            recorder(buffers_[i], frame_buffer, descriptor_set);
        }
    }

    void CommandPool::setup_pool()
    {
        const auto queue_family_indices = physical_device_.queue_family_indices();

        const auto pool_create_info = VkCommandPoolCreateInfo{
            .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
            .pNext = nullptr,
            .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
            .queueFamilyIndex = queue_family_indices.graphics_family.value(),
        };

        const auto error = vkCreateCommandPool(logical_device_.get(), &pool_create_info, nullptr, &pool_);

        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not create command pool.\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void CommandPool::setup_buffers(size_t image_count)
    {
        buffers_.resize(image_count);

        const auto buffer_allocation_info = VkCommandBufferAllocateInfo{
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            .pNext = nullptr,
            .commandPool = pool_,
            .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
            .commandBufferCount = static_cast<uint32_t>(buffers_.size()),
        };

        const auto error = vkAllocateCommandBuffers(logical_device_.get(), &buffer_allocation_info, buffers_.data());

        if (error != VK_SUCCESS)
        {
            std::cerr << "Could not create command buffers.\n";
            throw std::runtime_error(vulkan_error_to_string(error));
        }
    }

    void CommandPool::cleanup_pool()
    {
        vkDestroyCommandPool(logical_device_.get(), pool_, nullptr);
    }

    const CommandPool::buffer_vector_t& CommandPool::buffers() const
    {
        return buffers_;
    }

    const CommandPool::inner_t& CommandPool::get() const
    {
        return pool_;
    }

    const CommandPool::logical_device_t& CommandPool::device() const
    {
        return logical_device_;
    }

} // namespace engine
