#pragma once

#include <cstddef>
#include <vector>
#include <vulkan/vulkan_core.h>

#include "engine/devices/logical_device.hh"
#include "engine/devices/physical_device.hh"
#include "engine/devices/queue_family.hh"

namespace engine
{
    class CommandRecorder;
    class FrameBufferList;
    class DescriptorPool;
    class Renderer;

    class CommandPool
    {
    public:
        using inner_t = VkCommandPool;
        using logical_device_t = LogicalDevice;
        using physical_device_t = PhysicalDevice;
        using command_buffer_t = VkCommandBuffer;
        using buffer_vector_t = std::vector<command_buffer_t>;
        using frame_buffer_list_t = FrameBufferList;
        using descriptors_t = DescriptorPool;

        CommandPool(physical_device_t& physical_device, logical_device_t& logical_device, size_t image_count);
        ~CommandPool();

        void record_buffers(const frame_buffer_list_t& frame_buffer_list, const descriptors_t& descriptors,
                            const CommandRecorder& recorder);

        const inner_t& get() const;
        const buffer_vector_t& buffers() const;
        const logical_device_t& device() const;

    private:
        void setup(size_t image_count);
        void cleanup();

        void setup_pool();
        void setup_buffers(size_t image_count);

        void cleanup_pool();

    private:
        physical_device_t& physical_device_;
        logical_device_t& logical_device_;
        inner_t pool_;
        buffer_vector_t buffers_;
    };
} // namespace engine
