#pragma once

#include <memory>
#include <string>

#include "engine/buffer_memory/fwd.hh"
#include "engine/object/object.hh"

namespace engine
{
    class Image;
    class TextureImageView;
    class LogicalDevice;

    class DefaultTexture : public Object
    {
    public:
        using device_t = LogicalDevice;
        using buffer_t = ImageBuffer;
        using image_view_t = TextureImageView;

        DefaultTexture(const device_t& device);
        ~DefaultTexture();
        const image_view_t& view() const;

    private:
        void setup();
        void cleanup();

        const device_t& device_;

        std::unique_ptr<buffer_t> buffer_;
        std::unique_ptr<image_view_t> view_;
    };
} // namespace engine
