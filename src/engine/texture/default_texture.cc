#include "default_texture.hh"

#include <vulkan/vulkan_core.h>

#include "engine/buffer_memory/buffer_memory.hh"
#include "engine/image_view/texture_image_view.hh"

namespace engine
{
    DefaultTexture::DefaultTexture(const device_t& device)
        : device_(device)
    {
        setup();
    }

    DefaultTexture::~DefaultTexture()
    {
        cleanup();
    }

    const DefaultTexture::image_view_t& DefaultTexture::view() const
    {
        return *view_;
    }

    void DefaultTexture::setup()
    {
        constexpr auto usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
        constexpr auto properties = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
        constexpr auto format = VK_FORMAT_R8G8B8A8_SRGB;

        buffer_ = std::make_unique<buffer_t>(device_);
        buffer_->initialize({8, 8, 1}, {usage}, properties);

        view_ = std::make_unique<image_view_t>(device_, buffer_->buffer(), format, 1);
    }

    void DefaultTexture::cleanup()
    {
        view_.reset();
        buffer_.reset();
    }

} // namespace engine
