#include "texture.hh"

#include "engine/buffer_memory/buffer_memory.hh"
#include "engine/command_pool/command_pool.hh"
#include "engine/image/image.hh"
#include "engine/image_view/texture_image_view.hh"
#include "engine/single_commands/generate_mipmaps.hh"

namespace engine
{
    Texture::Texture(const command_pool_t& command_pool, const std::string& path, format_t format)
        : command_pool_(command_pool)
    {
        setup(path, format);
    }

    Texture::~Texture()
    {
        cleanup();
    }

    void Texture::setup(const std::string& path, format_t format)
    {
        image_ = std::make_unique<image_t>(command_pool_.device(), command_pool_, path, true, format);
        view_ = std::make_unique<image_view_t>(command_pool_.device(), image().buffer().buffer(),
                                               VK_FORMAT_R8G8B8A8_SRGB, image_->mipmap_level());

        generate_mipmaps();
    }

    void Texture::cleanup()
    {
        view_.reset();
        image_.reset();
    }

    void Texture::generate_mipmaps()
    {
        GenerateMipMaps{{command_pool_}}(*image_).execute();
    }

    const Texture::image_t& Texture::image() const
    {
        return *image_;
    }

    const Texture::image_view_t& Texture::view() const
    {
        return *view_;
    }

} // namespace engine
