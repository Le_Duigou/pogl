#include "texture_manager.hh"

#include <memory>
#include <optional>
#include <stdexcept>

#include "default_texture.hh"
#include "engine/command_pool/command_pool.hh"
#include "engine/image/image.hh"
#include "engine/image_sampler/image_sampler.hh"
#include "engine/image_view/texture_image_view.hh"
#include "texture.hh"

namespace engine
{
    TextureManager::TextureManager(const device_t& device)
        : device_(device)
        , is_dirty_(false)
    {
        setup();
    }

    TextureManager::~TextureManager()
    {
        cleanup();
    }

    void TextureManager::setup()
    {
        setup_default_texture();
        setup_samplers();
    }

    void TextureManager::cleanup()
    {
        cleanup_samplers();
        cleanup_default_texture();
    }

    void TextureManager::setup_default_texture()
    {
        default_texture_ = std::make_unique<default_texture_t>(device_);
    }

    void TextureManager::cleanup_default_texture()
    {
        default_texture_.reset();
    }

    void TextureManager::setup_samplers()
    {
        const auto& set = texture_sets_.find("main");
        const auto found = set != texture_sets_.end();

        diffuse_sampler_ =
            std::make_unique<texture_sampler_t>(device_, found ? set->second.diffuse->image().mipmap_level() : 1);
        orm_sampler_ =
            std::make_unique<texture_sampler_t>(device_, found ? set->second.orm->image().mipmap_level() : 1);
        normal_sampler_ =
            std::make_unique<texture_sampler_t>(device_, found ? set->second.normal->image().mipmap_level() : 1);
        environment_sampler_ =
            std::make_unique<texture_sampler_t>(device_, found ? set->second.environemnt->image().mipmap_level() : 1);
    }

    void TextureManager::cleanup_samplers()
    {
        diffuse_sampler_.reset();
        orm_sampler_.reset();
        normal_sampler_.reset();
        environment_sampler_.reset();
    }

    void TextureManager::set_texture_set(const std::string& set_name, const command_pool_t& command_pool,
                                         const std::string& diffuse_path,
                                         const std::string& occlusion_roughness_metallic_path,
                                         const std::string& normal_path, const std::string& environemnt_path)
    {
        texture_sets_.emplace(
            set_name,
            TextureSet{
                .diffuse = std::make_unique<Texture>(command_pool, diffuse_path, VK_FORMAT_R8G8B8A8_SRGB),
                .orm =
                    std::make_unique<Texture>(command_pool, occlusion_roughness_metallic_path, VK_FORMAT_R8G8B8A8_SRGB),
                .normal = std::make_unique<Texture>(command_pool, normal_path, VK_FORMAT_R8G8B8A8_SRGB),
                .environemnt = std::make_unique<Texture>(command_pool, environemnt_path, VK_FORMAT_R8G8B8A8_SRGB),
            });
        setup_samplers();
        set_dirty(true);
    }

    TextureDescriptorSet TextureManager::descriptors_of_set(const std::string& set_name) const
    {
        const auto find_result = texture_sets_.find(set_name);

        if (find_result == texture_sets_.end())
            return TextureDescriptorSet{
                .diffuse = default_descriptor(*diffuse_sampler_),
                .orm = default_descriptor(*orm_sampler_),
                .normal = default_descriptor(*normal_sampler_),
                .environemnt = default_descriptor(*environment_sampler_),
            };
        else
            return TextureDescriptorSet{
                .diffuse = descriptor_of(*diffuse_sampler_, *find_result->second.diffuse),
                .orm = descriptor_of(*orm_sampler_, *find_result->second.orm),
                .normal = descriptor_of(*normal_sampler_, *find_result->second.normal),
                .environemnt = descriptor_of(*environment_sampler_, *find_result->second.environemnt),
            };
    }

    VkDescriptorImageInfo TextureManager::default_descriptor(const texture_sampler_t& sampler) const
    {
        return VkDescriptorImageInfo{
            .sampler = sampler.get(),
            .imageView = default_texture_->view().get(),
            .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        };
    }

    VkDescriptorImageInfo TextureManager::descriptor_of(const texture_sampler_t& sampler, const Texture& texture) const
    {
        return VkDescriptorImageInfo{
            .sampler = sampler.get(),
            .imageView = texture.view().get(),
            .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        };
    }

    bool TextureManager::is_dirty() const
    {
        return is_dirty_;
    }

    void TextureManager::set_dirty(bool value)
    {
        is_dirty_ = value;
    }

} // namespace engine
