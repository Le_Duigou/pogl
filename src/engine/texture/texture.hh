#pragma once

#include <memory>
#include <string>
#include <vulkan/vulkan_core.h>

#include "engine/object/object.hh"

namespace engine
{
    class Image;
    class TextureImageView;
    class CommandPool;

    class Texture : public Object
    {
    public:
        using command_pool_t = CommandPool;
        using image_t = Image;
        using image_view_t = TextureImageView;
        using format_t = VkFormat;

        Texture(const command_pool_t& command_pool, const std::string& path, format_t format = VK_FORMAT_R8G8B8A8_SRGB);
        ~Texture();

        const image_t& image() const;
        const image_view_t& view() const;

    private:
        void setup(const std::string& path, format_t format);
        void cleanup();

        void generate_mipmaps();

        const command_pool_t& command_pool_;

        std::unique_ptr<image_t> image_;
        std::unique_ptr<image_view_t> view_;
    };
} // namespace engine
