#pragma once

#include <cstddef>
#include <memory>
#include <optional>
#include <unordered_map>
#include <vector>
#include <vulkan/vulkan_core.h>

namespace engine
{
    class CommandPool;
    class ImageSampler;
    class LogicalDevice;
    class DefaultTexture;
    class Texture;

    struct TextureSet
    {
        std::unique_ptr<Texture> diffuse;
        std::unique_ptr<Texture> orm;
        std::unique_ptr<Texture> normal;
        std::unique_ptr<Texture> environemnt;
    };

    struct TextureDescriptorSet
    {
        VkDescriptorImageInfo diffuse;
        VkDescriptorImageInfo orm;
        VkDescriptorImageInfo normal;
        VkDescriptorImageInfo environemnt;
    };

    class TextureManager
    {
    public:
        using device_t = LogicalDevice;
        using command_pool_t = CommandPool;
        using default_texture_t = DefaultTexture;
        using texture_sampler_t = ImageSampler;

        TextureManager(const device_t& device);
        ~TextureManager();

        bool is_dirty() const;
        void set_dirty(bool value);

        void set_texture_set(const std::string& set_name, const command_pool_t& command_pool,
                             const std::string& diffuse_path, const std::string& occlusion_roughness_metallic_path,
                             const std::string& normal_path, const std::string& environemnt_path);

        TextureDescriptorSet descriptors_of_set(const std::string& set_name) const;

    private:
        VkDescriptorImageInfo default_descriptor(const texture_sampler_t& sampler) const;
        VkDescriptorImageInfo descriptor_of(const texture_sampler_t& sampler, const Texture& texture) const;

    private:
        void setup();
        void cleanup();

        void setup_samplers();
        void cleanup_samplers();

        void setup_default_texture();
        void cleanup_default_texture();

        const device_t& device_;

        std::unique_ptr<default_texture_t> default_texture_;

        std::unique_ptr<texture_sampler_t> diffuse_sampler_;
        std::unique_ptr<texture_sampler_t> orm_sampler_;
        std::unique_ptr<texture_sampler_t> normal_sampler_;
        std::unique_ptr<texture_sampler_t> environment_sampler_;

        std::unordered_multimap<std::string, TextureSet> texture_sets_;

        size_t current_texture_index_;

        bool is_dirty_;
    };
} // namespace engine
