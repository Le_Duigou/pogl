#include "object.hh"

namespace engine
{
    bool Object::is_dirty() const
    {
        return is_dirty_;
    }

    void Object::set_dirty(bool value)
    {
        is_dirty_ = value;
    }
} // namespace engine
