#pragma once
#include "engine/units/units.hh"

namespace engine
{
    class Object
    {
    public:
        Object() = default;

        bool is_dirty() const;
        void set_dirty(bool value);

    private:
        bool is_dirty_ = false;
    };

} // namespace engine
