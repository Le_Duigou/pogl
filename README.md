# VULKAN-TEST

Author: Titouan Le Duigou <titouan.le-duigou@epita.fr>

This project is a simple model viewer in Vulkan. It can display multiple scenes.

Most notable features are:
* Viewport resizing and aspect ratio correction
* Vertex and index buffer handing in device local memory
* Depth buffering
* Texture loading in device local memory
* PBR shading with ambient occlusion/roughness/metallic map
* Normal mapping
* Indirect indexing draw calls to display multiple models in a single drawcall
* Environment reflection
* Input handling
* Obj scene loading
* Mipmap texture handling
* Simple gamma correction and tonemapping (embedded in fragment shader)

## Building the project
The following libraries are required to build:
* boost (libboost-all-dev on apt)
* GLFW (libglfw3-dev on apt)
* GLM (libglm-dev on apt)
* stb (libstb-dev on stb)
* glslangValidator

To build this project, use ``cmake``:
```sh
mkdir build && cd build
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_BUILD_TYPE=Release ..
make
```
You can use "Debug" as build type, but it will decrease greatly performances.
In "Release" mode, the following compilation flags are used:
```
-Wall -Wextra -std=c++20 -pedantic -O3 -march=native
```

## Starting the program
The program can be invoked using the command
```sh
./vulkan-test <SCENE_NAME>
```

``<SCENE_NAME>`` should be a valid scene name. To display the list of valid scene names, type ``./vulkan-test list``.

Some additional options might be used. Try ``./vulkan-test --help`` for more information.

## Using without a vulkan GPU driver
If you cannot use a vulkan GPU driver, it is possible to use lavapipe as an alternative. The program will run on the
CPU, showing then worse performances, but it should be enough for the demonstration.

Note that lavapipe implementation does not support anisotropic texture filtering. It should be disabled with the
``--no-anisotropic`` argument.

If you have several vulkan drivers installed, you can choose the one you want with an environmental variable, like this:
```sh
VK_ICD_FILENAMES="/usr/share/vulkan/icd.d/lvp_icd.x86_64.json" ./vulkan-test --no-anisotropic balls
```

## Scenes
Several scenes are available:
* "ball" : A simple rounded cube with a material. Rotate with keyboard arrows, and switch the current material with
    space.
* "monkey" : A simple render with Blender's Suzanne. Rotate with keyboard arrows.
* "chess" : A chessboard. Move around with keyboard arrows.
* "mario" : Mario. Rotate with keyboard arrows.
* "tree" : An extremely heavy file containing a big tree. Take a long time to load. Rotate with keyboard arrows.

If the resources are not included in the project, they are available here:
* https://drive.google.com/file/d/1-q5RXBb7DqMXI_9_4NnQoY4UlPNX1k5x/view?usp=sharing

## Explanation of the engine
Most of the engine code is stored in the ``src/engine/*`` files. The program has been coded fully in vulkan, using
glfw3 for window and input handling, and glm for advanced vector and matrix mathematics.

The following section will detail the inners of the program.


### Window, devices and vulkan instance
The entry point of the engine is the Program class. This class contains all the components of the engine. The lowest
ones are the Window class (A wrapper around glf3 window that expose enough to create a surface from it, detect resizing
and handle inputs), the EngineInstance class (A wrapper around the Vulkan instance, that also enable required extensions
and validation layers) and PhysicalDevice/LogicalDevice.

Vulkan require us to both create the physical device, representing a device (most commonly a GPU), and a logical device
from it (an abstract representation used in most tasks). It is important for compatibility to detect available features
on the device, and check if the required ones are supported. Indeed, some operation can be impossible to be  performed
on some GPU, and we should be able to detect it to either send a message to the user, or use fallbacks if possible.
In two cases, we use fallbacks to support the biggest amount of device possible: With the anisotropic texture filtering,
and with the indirect index draw commands.

Once these low representations are created, we can build some abstraction layers over them.


### Window surface, swap chain and depth buffer
The window surface is created from the window. It is the final destination of our rendered frames.

The next step is to build a swapchain. Since Vulkan does not have the concept of a default framebuffer we could draw on
we will have to create ourselves the buffer we should render on. And to avoid staggering images, we should have multiple
ones, to draw on a different buffer we are currently presenting. This is the goal of the swapchain.

On top of this, we will also create a buffer to hold the depth data. This buffer is used in the rendering process, and
is not presented directly to the user. This is why we don't have to create one for each swapchain image.

Because the size of these buffers cannot be changed dynamically, the swapchain and the depth buffer (and all the
components depending on them) should be recreate each time the user resize the screen.


### Pipeline and command buffer
The way we ask Vulkan to perform an action is a bit particular. Anything that happen on the devices cannot be called
directly. Instead, we should create a buffer to hold commands to perform, and then submit it for Vulkan to apply them.
This process of recording command buffers is of course required for drawing on the screen, but also for more simple
operation, like memory modifications on device such as buffer copy or image mipmap creation.

To hold the command buffers, we should use a command pool.

To draw geometry on screen (and thus to be able to use vertex, geometry, tesselation and fragment shaders), we should
create another construct, called a "pipeline".

The pipeline is by definition immutable. Each step of it can, and **has** to be configured. Some steps, involving
shaders, can be customized (obviously, these are the ones that interest us the most). The sequence of operation a
pipeline describes is similar to the one we can find in openGL. Some parts of it are optional, it is the case of
geometry or tesselation shaders. We won't use them in this project.

In the pipeline, we have to bind the shaders. This means that if we want to use different kind of shaders, we will have
to use different pipelines, and render with each of them one after the other in the command buffer. In the case of this
project, we will have only one pipeline. It has been planed of implementing several ones, but by lack of time, it has
been discarded.

The shaders are different than the ones we can find in DirectX or OpenGL. Indeed, Vulkan can only use shader in ``.spv``
format, a bytecode that has been created for it. This allow Vulkan to solve several issues of old shader formats, like
the difference of behavior of some tricky implementation dependent code that happens with some glsl. It also allow some
performance improvement, and some checks during the pipeline binding (for example, checking that the number of texture
sampler is correct).

Because none of us is particularly good at writing bytecode, the shaders are written in ``glsl``, and then compiled into
``spv``. I automated the process in cmake, and this is why glslangValidator is required for the compilation.


### Renderer and memory manager
Because manipulating low level structures is tiring and frustrating, some abstraction have been created over the object
we want to manipulate. Most of the functionality we need to use outside the engine are included in the class called
Renderer. This class contains the logic for updating each frame, the different meshes, the light and the camera. It
allows modification from outside via an update callback function, and determine when some elements bound to the
command buffers should be updated.

Aside this class can be found the memory manager. This class has for role to handle every elements related to the
manipulated device memory: vertex buffer, index buffer, draw command buffer, images, uniform buffer, etc.
Since a lot of these buffers are handled similarly, an helper class, BufferMemory, is used to handle the buffer and its
memory (two different construct in Vulkan).

Most of the elements in the device memory are placed in local memory. Indeed, Vulkan allow us to choose where to place
each buffer, and local memory is the fastest location for us to place buffers like vertices or textures.
However, this memory space is not directly accessible from the host. For this reason, we will have to use a staging
buffer to explicitly perform the copy. We first copy the buffer from the host to the staging buffer, that is placed in
a memory that is accessible from the host, then we use the command buffer to copy from the staging buffer to the
local memory.

Because this operation can be slower if the buffers change often, we avoid doing this for data that could change often,
like the ubo.


### Limitations of the current command buffer
The current way of creating the command buffer has several limitations that I figured out too late in the development to
dare modify it.

First, we have only one pipeline. We would like to be able to use several, choose one for each mesh, and so on, and the
current implementation doesn't allow it.

The second is that we use only one draw call to draw the whole scene. This is fast, because instead of asking vulkan to
draw each model one by one, we supply it with a buffer containing data about multiple models stored in the vertex.
This, used with an index buffer allow us for minimal number of draw call.

However, every model in the multiple call share the same ubo and the same samplers. For this reason, all the models have
to share the same textures, and the local space to world space matrix is the same for all of them, making it impossible
to transform a single mesh without modifying its vertices directly (and thus rebuilding the vertex buffer).

Finally, for the moment, I handle only one light. It is not complex to add more lights in the uniform buffer, but
this was not the priority during this already extremely time consuming project, and this has been left as a possible
future improvement.


### Shading
Today, nobody uses the diffuse/specular model anymore. It is not physically accurate, it is clumsy and it is hard to
have an acceptable artistic control over it. Today, we prefer using PBR shading models, and the most used one is
a model built on the roughness/metallic parameters. In this project, I decided to implement such a shading model based.

Most of the computation is made in the ``pbr.frag`` file. 

Most of pbr models are based on the theory of microfacets: it aims at representing a small segment of a surface as a
distribution of little invisible bumps that can be averaged using the property of the roughness. To sum it up, the less
a surface is rough, the smoother it is, the more it reflects light in a single direction. The more rough it is, the more
diffuse reflections get.

The shader has then to implement a BRDF (Bidirectional Reflective Diffusion Function) that will approximate light
behavior using the roughness and metallic properties of the surface. To do it in real time, we use some approximations,
such as the GGX distribution or the Schlick-Beckmann approximation.

Globally, we will have to approximate three components:
* The **normal distribution** (``float distribution``), representing the amount of microfacet that are facing in the
    direction of the view vector at this fragment
* The **geometry function** (``float geometry``), representing the amount of self shadowing the microfacets are
    performing on each others.
* The **fresnel equation** (``vec3 fresnel_coeff``), representing the amount of reflected light in the current direction

Using these three components, we can approximate correctly the light behavior with an accuracy that is very acceptable.

All the calculations are made in world space, which is a choice that could be questioned. Indeed, tangent space
calculations are often preferred in this kind of cases, as they allow more calculation on the vertex shader and less
on the fragment. However, due to trouble calculating accurately tangent and bitangent vectors, the choice of going on
world space computation has been made.

At the end, the shader takes three textures:
* The base color
* A packed texture representing baked ambient occlusion, roughness and metallic
* The normal map

A fourth texture represent the environment. It has been used to compute the reflection color.
However, to taking into account the variation of microfacet normal that is created by roughness, we use higher level
of detail (mipmaps) for fragments that have an high roughness. The result is a blurrier environment reflection without
the cost of a convolution blur.


## Post mortem
The original goal of this project was to create a particle system in Vulkan. Not a stupid particle system, that computed
everything on the CPU to only render the triangles on the GPU, but a smart particle that computed the position of the
particles on the GPU using their position, velocity and acceleration, that used instantiation to avoid the cost of too
many draw calls, and that was flexible enough to compute other kinds of attributes on CPU if needed. I started to think
in a way to create a system of plugin for emitters and particles using templates to avoid runtime cost.

But I made a mistake underestimating Vulkan. I thought it would be as simple as OpenGL, with a little more verbose
syntax, but better thought features. This was my mistake.

It took me a week to build the system displaying a single triangle, and after almost three weeks of working daily on it,
sometimes the full day with very little breaks, I came to the conclusion that I needed one more week to actually
implement the features I wanted, a week that I didn't have. This is why I decided to focus on the already well built
PBR system I had already created, and throw away my dreams of smart particle system.

Vulkan is not only verbose, it is extremely low level. It ask you to do everything, and understand what you do. The
results can be very impressive, and it is time consuming. The engine I made might not be impressive, but it has been
such a pain to build from nothing.

But I think the experience (that I do not want to reiterate) was a strong learning experience. Building the process from
its most basic components, with the mistakes I made and ideas I got, is something that helped make this project
something more than a pointless wast of time and resources.

## References
* https://neil3d.github.io/reading/assets/slides/s2012_pbs_disney_brdf_notes_v3.pdf
* https://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf
